# Soiree Application - Nicholas Carugati

Written in: Spring boot JPA with Tomcat, Hibernate
Front end written in: AngularJS 1.6.9
Front end styling used: AngularJS Material


Few things to keep in mind:

## Quirks
Some of the quirks that are in Soiree that I couldn't fix are as follows
 - XML Root Element not found error occurrs whenever you're rating finding event rights or looking for certain reified associations (Firefox only)
 - Some "images/links" from Google Chrome will come up as 404 and not recognized the fact that the hyperlink references are resolved AngularJS variables. So using chrome will give a bit of 404 errors. This does not mean the application is broken per se.
 - Due to the nature of Free Tier AWS RDS. Much of the interactions are going to be slow. The advice i'd give here is to give about 8-10 seconds for an operation to perform (Such as liking a post/commenting) before concluding theres a problem detected.
- EventBrite has a small problem interacting with requests made by a java client so there are at times where the explore feed will throw an error message (Not by the server). I tried contacting EventBrite support about it and they have not replied back to me on the matter yet.
- EventBrite's API is limited to 2000 API calls for an hour so if there is one stress test which involves spamming API calls keep that threshold in mind.

## How to Build
- To clean and rebuild maven dependancies perform 'mvn clean install' on the working directory
- To run spring boot on standalone use the following command in the working directory: 'java -jar target/dependency/webapp-runner.jar --port 8080 target/*.war'
- There are no build/run operations required for front end as most of the dependancies for front end are downloaded and initialized through CDNs 
- If you plan on truncating the entire Soiree schema I would not reccommend truncating tables 'event_role' and 'categories' these are enum tables which get the contents for event interests/categories and identify event staffing permissions. If the data for these are missing please execute roles.sql and categories.sql located in the sql folder of the working directory to repopulate the records.
- If you are starting with an empty schema entirely please execute soiree.sql located in the sql folder of the working directory 


## References

[Spring Boot Authentication](https://spring.io/guides/gs/securing-web/)

[Spring Boot Authentication (Secondary)](http://www.baeldung.com/securing-a-restful-web-service-with-spring-security)

[Hibernate Search with Apache Lucene](http://hibernate.org/search/documentation/getting-started/)

[File Uploading with Spring Boot](http://www.mkyong.com/spring-boot/spring-boot-file-upload-example/)

[ResponseEntity Source](http://www.baeldung.com/spring-mvc-controller-custom-http-status-code)

[particles.js (For the cool particle effect on the homepage)](https://vincentgarreau.com/particles.js/#default)

[AngularJS Material](https://material.angularjs.org/latest/)
