DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` varchar(512) NOT NULL,
  `type` varchar(32) NOT NULL DEFAULT 'USER',
  `username` varchar(256) NOT NULL,
  `password` varchar(512) NOT NULL,
  `email` varchar(256) NOT NULL,
  `member` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_user_generalization_idx` (`member`),
  CONSTRAINT `user_user_generalization` FOREIGN KEY (`member`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(512) NOT NULL,
  `first_name` varchar(64) DEFAULT NULL,
  `last_name` varchar(64) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `bio` longtext,
  `phone` varchar(32) DEFAULT NULL,
  `website` varchar(128) DEFAULT NULL,
  `alias` varchar(64) DEFAULT NULL,
  `specialization` varchar(64) DEFAULT NULL,
  `venue` varchar(64) DEFAULT NULL,
  `category` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`(255)),
  KEY `settings_users_generalization` (`user_id`),
  CONSTRAINT `settings_users_generalization` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(512) NOT NULL,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `name` varchar(128) NOT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `location` varchar(128) DEFAULT NULL,
  `admission` double DEFAULT NULL,
  `description` longtext,
  `image` varchar(128) DEFAULT NULL,
  `priority` int(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_event_generalization_idx` (`owner_id`(255)),
  KEY `user_event_generalization` (`owner_id`),
  CONSTRAINT `user_event_generalization` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(512) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext,
  `attachment` longtext,
  `locked` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_posts_generalization_idx` (`owner_id`(255)),
  KEY `user_posts_generalization` (`owner_id`),
  CONSTRAINT `user_posts_generalization` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner_id` varchar(512) NOT NULL,
  `post_id` int(11) NOT NULL,
  `content` longtext,
  `timestamp` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_comments_generaliztion_idx` (`owner_id`(255)),
  KEY `post_comments_generalization_idx` (`post_id`),
  KEY `user_comments_generaliztion` (`owner_id`),
  CONSTRAINT `post_comments_generalization` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_comments_generaliztion` FOREIGN KEY (`owner_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `interest`;
CREATE TABLE `interest` (
  `id` int(64) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(512) NOT NULL,
  `interest_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `interests_user_generalization_idx` (`user_id`(255)),
  KEY `interests_category_generalization_idx` (`interest_id`),
  KEY `interests_user_generalization` (`user_id`),
  CONSTRAINT `interests_category_generalization` FOREIGN KEY (`interest_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `interests_user_generalization` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `rating`;
CREATE TABLE `rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(512) NOT NULL,
  `post_id` int(11) NOT NULL,
  `rating` tinyint(3) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_posts_rating_generalization_idx` (`user_id`(255)),
  KEY `posts_posts_rating_generalization_idx` (`post_id`),
  KEY `user_rating_generalization` (`user_id`),
  CONSTRAINT `posts_rating_generalization` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_rating_generalization` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_posts`;
CREATE TABLE `profile_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(512) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_profile_posts_generalization_idx` (`user_id`(255)),
  KEY `post_profile_posts_generalization_idx` (`post_id`),
  KEY `user_profile_posts_generalization` (`user_id`),
  CONSTRAINT `post_profile_posts_generalization` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_profile_posts_generalization` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversation_id` int(11) NOT NULL,
  `sender_id` varchar(512) NOT NULL,
  `recipient_id` varchar(512) NOT NULL,
  `message` longtext NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_read` tinyint(2) NOT NULL DEFAULT '0',
  `sender_archived` tinyint(2) NOT NULL DEFAULT '0',
  `recipient_archived` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `sender_user_generalization_idx` (`sender_id`(255)),
  KEY `recipient_user_generalization_idx` (`recipient_id`(255)),
  KEY `message_conversation_generalization_idx` (`conversation_id`),
  KEY `recipient_user_generalization` (`recipient_id`),
  KEY `sender_user_generalization` (`sender_id`),
  CONSTRAINT `message_conversation_generalization` FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `recipient_user_generalization` FOREIGN KEY (`recipient_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sender_user_generalization` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `followers`;
CREATE TABLE `followers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `follower_id` varchar(512) NOT NULL,
  `followee_id` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `follower_user_generalization_idx` (`follower_id`(255)),
  KEY `followee_user_generalization_idx` (`followee_id`(255)),
  KEY `followee_user_generalization` (`followee_id`),
  KEY `follower_user_generalization` (`follower_id`),
  CONSTRAINT `followee_user_generalization` FOREIGN KEY (`followee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `follower_user_generalization` FOREIGN KEY (`follower_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `event_role`;
CREATE TABLE `event_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

LOCK TABLES `event_role` WRITE;
INSERT INTO `event_role` VALUES (1,'PLANNER'),(2,'PROMOTER'),(3,'MODERATOR');
UNLOCK TABLES;

DROP TABLE IF EXISTS `event_posts`;
CREATE TABLE `event_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `event_event_post_generlization_idx` (`event_id`),
  KEY `post_event_post_generalization_idx` (`post_id`),
  CONSTRAINT `event_event_post_generlization` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_event_post_generalization` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `event_interest`;
CREATE TABLE `event_interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(512) NOT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_event_interest_generalization_idx` (`user_id`(255)),
  KEY `event_event_interest_generalization_idx` (`event_id`),
  KEY `user_event_interest_generalization` (`user_id`),
  CONSTRAINT `event_event_interest_generalization` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_event_interest_generalization` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `event_associations`;
CREATE TABLE `event_associations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(512) NOT NULL,
  `event_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_event_roles_generalization_idx` (`user_id`(255)),
  KEY `event_role_event_assoication_generalization_idx` (`role_id`),
  KEY `event_event_associations_generalization_idx` (`event_id`),
  KEY `user_event_associations_generalization` (`user_id`),
  CONSTRAINT `event_event_associations_generalization` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_event_associations_generalization` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_roles_event_associations_generalization` FOREIGN KEY (`role_id`) REFERENCES `event_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `conversations`;
CREATE TABLE `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participant_1` varchar(512) NOT NULL,
  `participant_2` varchar(512) NOT NULL,
  `part_1_archived` tinyint(2) NOT NULL DEFAULT '0',
  `part_2_archived` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `conversation_user_generalization_2_idx` (`participant_2`(255)),
  KEY `conversation_user_generalization_idx` (`participant_1`(255)),
  KEY `conversation_user_generalization` (`participant_1`),
  KEY `conversation_user_generalization_2` (`participant_2`),
  CONSTRAINT `conversation_user_generalization` FOREIGN KEY (`participant_1`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `conversation_user_generalization_2` FOREIGN KEY (`participant_2`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
INSERT INTO `categories` VALUES (101,'Business & Professional'),(102,'Science & Technology'),(103,'Music'),(104,'Film & Media'),(105,'Performing & Visual Arts'),(106,'Fashion & Beauty'),(107,'Health & Wellness'),(108,'Sports & Fitness'),(109,'Travel & Outdoor'),(110,'Food & Drink'),(111,'Charity & Causes'),(112,'Government & Politics'),(113,'Community & Culture'),(114,'Religion & Sprituality'),(115,'Family & Education'),(116,'Seasonal & Holiday'),(117,'Home & Lifestyle'),(118,'Auto, Boat & Air'),(119,'Hobbies & Special Interests'),(120,'School Activities');
UNLOCK TABLES;

DROP TABLE IF EXISTS `attendees`;
CREATE TABLE `attendees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `attendee_id` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `event_attendees_generalization_idx` (`event_id`),
  KEY `user_attendees_generalization_idx` (`attendee_id`(255)),
  KEY `user_attendees_generalization` (`attendee_id`),
  CONSTRAINT `event_attendees_generalization` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_attendees_generalization` FOREIGN KEY (`attendee_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
