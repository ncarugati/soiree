DROP TABLE IF EXISTS `event_role`;
CREATE TABLE `event_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

LOCK TABLES `event_role` WRITE;
INSERT INTO `event_role` VALUES (1,'PLANNER'),(2,'PROMOTER'),(3,'MODERATOR');
UNLOCK TABLES;