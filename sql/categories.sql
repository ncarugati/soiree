DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `categories` WRITE;
INSERT INTO `categories` VALUES (101,'Business & Professional'),(102,'Science & Technology'),(103,'Music'),(104,'Film & Media'),(105,'Performing & Visual Arts'),(106,'Fashion & Beauty'),(107,'Health & Wellness'),(108,'Sports & Fitness'),(109,'Travel & Outdoor'),(110,'Food & Drink'),(111,'Charity & Causes'),(112,'Government & Politics'),(113,'Community & Culture'),(114,'Religion & Sprituality'),(115,'Family & Education'),(116,'Seasonal & Holiday'),(117,'Home & Lifestyle'),(118,'Auto, Boat & Air'),(119,'Hobbies & Special Interests'),(120,'School Activities');
UNLOCK TABLES;