package edu.northeastern.cs5200;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import edu.northeastern.cs5200.project.auth.AuthenticationFailure;
import edu.northeastern.cs5200.project.auth.AuthenticationSuccess;
import edu.northeastern.cs5200.project.auth.EntryPointUnauthorizedHandler;
import edu.northeastern.cs5200.project.auth.UserDetailServiceImpl;

/**
 * The security configuration for the application 
 * @see https://spring.io/guides/gs/securing-web/
 * @author Nicholas Carugati
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	/**
	 * The routes that only anonymous users can access
	 */
	public static final String[] PUBLIC_POST_ROUTES = { "/api/user", "/login", "/logout"};
	
	/**
	 * The routes that all users can access 
	 */
	public static final String[] PUBLIC_GET_ROUTES = { "/**", "/login", "/register", "/profile/**", "/api/loggedin", "/events/**", 
													"/api/user/**", "/api/event/**", "/search", "/api/search/**"};
	
	/**
	 * The delete routes only admins can access
	 */
	public static final String[] ADMIN_DELETE_ROUTES = { "/api/message/admin/**" };
	
	/**
	 * The put routes only admins can access
	 */
	public static final String[] ADMIN_PUT_ROUTES = { "/api/message/admin/**" };
	
	/**
	 * The post routes only admins can access
	 */
	public static final String[] ADMIN_POST_ROUTES = { "/api/admin/user" };
	
	/**
	 * The get routes only admins can access
	 */
	public static final String[] ADMIN_GET_ROUTES = { "/admin", "/api/event", "/api/message/all", "/api/post", "/api/admin/user" };
	
	/**
	 * The front end asset routes that should be overlooked by security
	 */
	public static final String[] PUBLIC_ASSET_ROUTES = { "/config.js", "/app.js", "/controllers/**", "/directives/**", "/services/**", 
														"/views/**", "/images/**", "/css/**", "/uploads/**", "/js/**", "/favicon.ico"};

	/**
	 * Handles if a user has failed to authenticate a route
	 */
	@Autowired
	AuthenticationFailure authFailure;

	/**
	 * Handles if a user has succeeded to authenticate a route
	 */
	@Autowired
	AuthenticationSuccess authSuccess;

	/**
	 * Handles if a user access an unauthorized route
	 */
	@Autowired
	EntryPointUnauthorizedHandler unauthHandler;

	/**
	 * Retrieves the current user session details
	 */
	@Autowired
	UserDetailServiceImpl userDetails;

	/**
	 * The password encrypter which encrypts users passwords with bcrypt
	 * @param builder the manager builder for the session
	 * @throws Exception if the service fails to obtain the user details
	 */
	@Autowired
	public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
		builder.userDetailsService(userDetails).passwordEncoder(new BCryptPasswordEncoder());
	}
	
	/**
	 * The configuration method which ignores all public assets for the front end
	 * @throws Exception if the routes fail to read.
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(PUBLIC_ASSET_ROUTES);
	}

	/**
	 * The configuration policies for the application
	 * @param http the http security builder which evaluates the policy
	 * @throws Exception if the http security builder fails to read the policy
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.formLogin()
				.loginPage("/login")
				.successHandler(authSuccess)
				.failureHandler(authFailure)
				.permitAll()
		.and()
				.logout()
				.logoutSuccessUrl("/login")
		.and()
			.exceptionHandling()
			.authenticationEntryPoint(unauthHandler)
		.and()
			.authorizeRequests()
			.antMatchers(HttpMethod.POST, PUBLIC_POST_ROUTES).permitAll()
			.antMatchers(HttpMethod.GET, ADMIN_GET_ROUTES).hasRole("ADMIN")
			.antMatchers(HttpMethod.DELETE, ADMIN_DELETE_ROUTES).hasRole("ADMIN")
			.antMatchers(HttpMethod.PUT, ADMIN_PUT_ROUTES).hasRole("ADMIN")
			.antMatchers(HttpMethod.POST, ADMIN_POST_ROUTES).hasRole("ADMIN")
			.antMatchers(HttpMethod.GET, PUBLIC_GET_ROUTES).permitAll()
			.anyRequest().authenticated();

	}
}
