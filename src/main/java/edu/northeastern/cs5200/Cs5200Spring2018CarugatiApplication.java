package edu.northeastern.cs5200;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * The main Spring Boot Application launcher
 * @author Nicholas Carugati
 *
 */
@SpringBootApplication
public class Cs5200Spring2018CarugatiApplication extends SpringBootServletInitializer {

	/**
	 * Searches for configurations for this spring boot build
	 * @param application the application to search for configurations
	 * @return the configuration sources for this application
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Cs5200Spring2018CarugatiApplication.class);
	}

	/**
	 * Runs the application
	 * @param args additional program arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(Cs5200Spring2018CarugatiApplication.class, args);
	}
}
