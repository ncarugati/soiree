package edu.northeastern.cs5200;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * The viewer configurer which manages and makes 
 * the view resolver compatible to angularJS in HTML 5 mode
 * @author Nicholas Carugati
 *
 */
@Configuration
@EnableWebMvc
public class WebConfiguration extends WebMvcConfigurerAdapter {

	/**
	 * Retrieves the properties of the server
	 * @return the server configuration
	 */
	@Bean
	public ServerProperties getServerProperties() {
		return new ServerConfiguration();
	}

	/**
	 * Retrieves the view resolver
	 * @return the view resolver
	 */
	@Bean
	public ViewResolver getViewResolver() {
		return new InternalResourceViewResolver();
	}

	/**
	 * Provides resource exceptions for files which will be creates
	 * @param resgitry the current registry instance
	 */
	@Override
	public void addResourceHandlers(final ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/uploads/**").addResourceLocations("file:uploads/");
	}

	/**
	 * Configures the default view resolver settings
	 * @param configurer the default servlet configurer instance
	 */
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
}
