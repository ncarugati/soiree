package edu.northeastern.cs5200;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.lucene.search.Query;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.springframework.stereotype.Repository;

import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.User;

/**
 * Handles and initializes all search operations and indexing for Apache Lucene
 * @see https://www.mkyong.com/spring-boot/spring-boot-hibernate-search-example/
 * @author Michael Good
 * @author Nicholas Carugati
 *
 */
@Repository
public class SearchHandler {
	
	/**
	 * The edit distance for search results
	 */
	public static final int EDIT_DISTANCE = 1;
	
	/**
	 * The prefix length for search results
	 */
	public static final int PREFIX_LENGTH = 1;

	/**
	 * The entity manager to recall and persist indexes entities
	 */
	@PersistenceContext
	private EntityManager entityManager;
	
	/**
	 * Forms the composite search results
	 * @param keyword the keyword to evaluate the search against
	 * @return the list of all the search entities
	 */
	@Transactional
	@SuppressWarnings("unchecked")
	public List<List<Object>> compositeSearch(String keyword) {

		javax.persistence.Query userQuery = establishQueryForClass(User.class, keyword);
		javax.persistence.Query eventQuery = establishQueryForClass(Event.class, keyword);
		
		List<Object> userList = userQuery.getResultList();
		List<Object> eventList = eventQuery.getResultList();
		List<List<Object>> compositeList = new ArrayList<List<Object>>();
		compositeList.add(userList);
		compositeList.add(eventList);

		return compositeList;
	}
	
	/**
	 * Initializes the data fields for Lucene's indexing
	 * @param cls the class to find indexed variables for
	 * @param keyword the keyword to evaluate the indexing against
	 * @return the full text query from the evaluated entities
	 */
	private javax.persistence.Query establishQueryForClass(Class<?> cls, String keyword) {
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		QueryBuilder qb = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(cls).get();
		Query luceneQuery = qb.keyword().fuzzy()
				.withEditDistanceUpTo(EDIT_DISTANCE)
				.withPrefixLength(PREFIX_LENGTH)
				.onFields(getVariableFields(cls)).matching(keyword).createQuery();
		return fullTextEntityManager.createFullTextQuery(luceneQuery, cls);
	}
	
	/**
	 * Recursively finds the variable fields depending on the class.
	 * @param cls the class to search annotations for
	 * @return the fields for the indexer to look up against
	 */
	private String[] getVariableFields(Class<?> cls) {
		Set<String> fields = new HashSet<String>();
		for (Field field : cls.getDeclaredFields()) {
			Class<org.hibernate.search.annotations.Field> searchField = 
					org.hibernate.search.annotations.Field.class;
			if (field.getAnnotation(searchField) != null) {
				fields.add(field.getName());
			} else if(field.getAnnotation(IndexedEmbedded.class) != null) {
				for(String member : getVariableFields(field.getType())) {
					StringBuilder sb = new StringBuilder(field.getName() + ".");
					sb.append(member);
					fields.add(sb.toString());
				}
			}
		}
		return fields.toArray(new String[fields.size()]);
	}

}
