package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.EventAssociation;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the event association data model on the database
 * @author Nicholas Carugati
 *
 */
public interface EventAssociationRepository extends JpaRepository<EventAssociation, Integer> {
	
	/**
	 * Determines if a user has staff rights to an event
	 * @param user the user to find
	 * @param event the event to find
	 * @return The instance if it exists
	 */
	EventAssociation findByUserAndEvent(User user, Event event);
	
}
