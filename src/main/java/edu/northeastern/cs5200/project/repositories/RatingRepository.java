package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Post;
import edu.northeastern.cs5200.project.models.Rating;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the rating data model on the database
 * @author Nicholas Carugati
 *
 */
public interface RatingRepository extends JpaRepository<Rating, Integer> {

	/**
	 * Retrieves the rating a user created for a post
	 * @param user the user to search for
	 * @param post the post to search for
	 * @return the rating instance for a user
	 */
	Rating findByUserAndPost(User user, Post post);
}
