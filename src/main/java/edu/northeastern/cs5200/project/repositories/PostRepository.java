package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Post;

/**
 * Represents the set of operations which interact with the post data model on the database
 * @author Nicholas Carugati
 *
 */
public interface PostRepository extends JpaRepository<Post, Integer> {

}
