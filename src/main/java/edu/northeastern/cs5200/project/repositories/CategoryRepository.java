package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Category;

/**
 * Represents the set of operations which interact with the category data model on the database
 * @author Nicholas Carugati
 *
 */
public interface CategoryRepository extends JpaRepository<Category, Integer> { 
	
}
