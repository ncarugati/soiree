package edu.northeastern.cs5200.project.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.EventInterest;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the EventInterest data model on the database
 * @author Nicholas Carugati
 *
 */
public interface EventInterestRepository extends JpaRepository<EventInterest, Integer> {
	
	/**
	 * Finds the list of interests by an event
	 * @param evt the event to find
	 * @return the list of users interested in the event
	 */
	Set<EventInterest> findByEvent(Event evt);
	
	/**
	 * Finds the list of events a user is interested in
	 * @param user the user to find the interested events for
	 * @return the list of the events a user is interested in
	 */
	Set<EventInterest> findByUser(User user);
	
	/**
	 * Finds an event and user interest pair
	 * @param evt the event to search for
	 * @param user the user to search for
	 * @return The event interest instance
	 */
	EventInterest findByEventAndUser(Event evt, User user);

}
