package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.EventRole;

/**
 * Represents the set of operations which interact with the event role data model on the database
 * @author Nicholas Carugati
 *
 */
public interface EventRoleRepository extends JpaRepository<EventRole, Integer> {

}
