package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.EventPost;

/**
 * Represents the set of operations which interact with the event posts data model on the database
 * @author Nicholas Carugati
 *
 */
public interface EventPostRepository extends JpaRepository<EventPost, Integer> {

}
