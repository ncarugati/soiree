package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import edu.northeastern.cs5200.project.models.Settings;

/**
 * Represents the set of operations which interact with the settings data model on the database
 * @author Nicholas Carugati
 *
 */
public interface SettingsRepository extends JpaRepository<Settings, Integer> {
	
}
