package edu.northeastern.cs5200.project.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Interest;

/**
 * Represents the set of operations which interact with the interest data model on the database
 * @author Nicholas Carugati
 *
 */
public interface InterestRepository extends JpaRepository<Interest, Integer> {

}
