package edu.northeastern.cs5200.project.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Follower;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the follower data model on the database
 * @author Nicholas Carugati
 *
 */
public interface FollowerRepository extends JpaRepository<Follower, Integer> { 
	
	/**
	 * Finds the follower-followee pair
	 * @param follower the follower to find
	 * @param followee the followee to find
	 * @return The following instance
	 */
	public Follower findByFollowerAndFollowee(User follower, User followee);
	
	/**
	 * Retrieves a user's followers
	 * @param follower the user to search followers for
	 * @return the list of followers for a user
	 */
	public List<Follower> findByFollower(User follower);
	
	/**
	 * Retrieves the users a single user is following
	 * @param followee the user to search for followees for
	 * @return the list of followees for a user
	 */
	public List<Follower> findByFollowee(User followee);

}