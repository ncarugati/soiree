package edu.northeastern.cs5200.project.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Attendee;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the attendee data model on the database
 * @author Nicholas Carugati
 *
 */
public interface AttendeeRepository extends JpaRepository<Attendee, Integer> {
	
	/**
	 * Finds the list of attendees by event
	 * @param ev the event to search for
	 * @return the list of people attending the event
	 */
	Set<Attendee> findByEvent(Event ev);
	
	/**
	 * Searches for event and attendee to determine if a user is going to an event
	 * @param ev the event to find
	 * @param user the user to find
	 * @return the user-event pair instance
	 */
	Attendee findByEventAndAttendee(Event ev, User user);

}
