package edu.northeastern.cs5200.project.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the event data model on the database
 * @author Nicholas Carugati
 *
 */
public interface EventRepository extends JpaRepository<Event, Integer> {
	
	/**
	 * Find the events that a user owns in recent order
	 * @param user the user to search events for
	 * @return the list of events owned by a user
	 */
	Set<Event> findByOwnerOrderByTimestampDesc(User user);
	
	/**
	 * Searches for the events of all a user's followers
	 * @param user the user to search its follower's events for
	 * @return the list of events made by a user's followers
	 */
	@Query("SELECT e FROM events e JOIN e.owner.followers o WHERE o.followee = ?1 ORDER BY e.timestamp DESC")
	Set<Event> getFollowerEvents(User user);
	
	/**
	 * Gets list of all events except for its own events
	 * @param user the user to search all events for
	 * @return the list of all events from priority to recents
	 */
	@Query("SELECT e FROM events e WHERE e.owner != ?1 ORDER BY e.priority DESC, e.timestamp DESC")
	Set<Event> getEventFeed(User user);

}
