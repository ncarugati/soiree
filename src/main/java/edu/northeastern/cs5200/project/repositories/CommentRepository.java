package edu.northeastern.cs5200.project.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.northeastern.cs5200.project.models.Comment;
import edu.northeastern.cs5200.project.models.Post;

/**
 * Represents the set of operations which interact with the comment data model on the database
 * @author Nicholas Carugati
 *
 */
public interface CommentRepository extends JpaRepository<Comment, Integer> {
	
	/**
	 * Finds the list of comments for an attached post
	 * @param p the post to find from the event or profile
	 * @return the list of comment associated with a post
	 */
	Set<Comment> findByPostOrderByTimestampDesc(Post p);

}
