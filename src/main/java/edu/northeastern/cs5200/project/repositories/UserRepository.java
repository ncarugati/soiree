package edu.northeastern.cs5200.project.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with user data model on the database
 * @author Nicholas Carugati
 *
 */
public interface UserRepository extends JpaRepository<User, String> {
	
	/**
	 * Retrieves a user by its username
	 * @param username the name of the user
	 * @return the user instance linked to the username
	 */
	User findByUsername(String username);
	
	/**
	 * Finds all the users except for the one specified
	 * @param userId the id of the user to exclude
	 * @return the list of all users except for the one specified
	 */
	@Query("SELECT u FROM users u WHERE u.id != ?1")
	List<User> findAllExcept(String userId);
	
	/**
	 * Finds all the associated members for a user
	 * @param user the user to find members for
	 * @return the list of members for a user
	 */
	Set<User> findByMember(User user);
	
	/**
	 * Retrieves all the list of available users to add for membership
	 * @param userId the userId to exclude
	 * @return the list of users except for the unavailable and the own user searching for members
	 */
	@Query("SELECT u FROM users u WHERE (u.member IS NULL AND u.type = 'USER' AND u.id != ?1)")
	Set<User> findAvailableUsers(String userId);
	
}