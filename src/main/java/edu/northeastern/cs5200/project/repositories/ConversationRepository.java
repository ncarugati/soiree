package edu.northeastern.cs5200.project.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import edu.northeastern.cs5200.project.models.Conversation;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the conversation data model on the database
 * @author Nicholas Carugati
 *
 */
public interface ConversationRepository extends JpaRepository<Conversation, Integer> {
	
	/**
	 * Searches for a conversation based on its participants
	 * @param primary the primary participant
	 * @param secondary the secondary participant
	 * @return the conversation instance associated with the pair
	 */
	@Query("SELECT c FROM conversations c WHERE (c.participantOne = ?1 AND c.participantTwo = ?2) OR "
		 + "(c.participantOne = ?2 AND c.participantTwo = ?1)")
	public Conversation findConversationByParticipants(User primary, User secondary);
	
	/**
	 * Finds all the conversations for a specific user
	 * @param user the user to search for
	 * @return the list of conversations for the user's inbox
	 */
	@Query("SELECT c FROM conversations c WHERE c.participantOne = ?1 OR c.participantTwo = ?1")
	public Set<Conversation> findConversationsForUser(User user);
	
}
