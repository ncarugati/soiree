package edu.northeastern.cs5200.project.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import edu.northeastern.cs5200.project.models.Conversation;
import edu.northeastern.cs5200.project.models.Message;
import edu.northeastern.cs5200.project.models.User;

/**
 * Represents the set of operations which interact with the message data model on the database
 * @author Nicholas Carugati
 *
 */
public interface MessageRepository extends JpaRepository<Message, Integer> {
	
	/**
	 * Retrieves the messages associated with a conversation
	 * @param sender the sender participant of the conversation
	 * @param recipient the recipient participant of the conversation
	 * @return the list of messages for this conversation
	 */
	@Query("SELECT m FROM messages m WHERE (m.sender = ?1 AND m.recipient = ?2) OR "
			+ "(m.sender = ?2 AND m.recipient = ?1) ORDER BY m.timestamp DESC")
	List<Message> findConversation(User sender, User recipient);
	
	
	/**
	 * Gets all the recent conversation messages for a user contingent that they haven't archived the recent message
	 * This complex query could not be executed in JPQL
	 * @param userId The userId to get the conversations for
	 * @return the conversation data set
	 */
	@Query(value = "SELECT * FROM soiree.messages mess " + 
			"JOIN (SELECT conversation_id, MAX(timestamp) recent FROM (SELECT conversation_id, timestamp FROM soiree.messages " + 
			"JOIN soiree.conversations conv ON conv.id = conversation_id " + 
			"WHERE " + 
			"CASE " + 
			"WHEN sender_id = ?1 THEN sender_archived != 1 " + 
			"ELSE recipient_archived != 1 END) initial GROUP BY conversation_id) res " + 
			"ON res.conversation_id = mess.conversation_id AND res.recent = mess.timestamp WHERE " + 
			"(sender_id = ?1 OR recipient_id = ?1) " + 
			"ORDER BY mess.timestamp DESC", nativeQuery=true)
	List<Message> findConversationsForUser(String userId);
	
	/**
	 * Reads all the messages when a user opens up a conversation
	 * @param user the user to read all messages for
	 * @param conv the conversation to set all read messages for
	 */
	@Transactional
	@Modifying
	@Query("UPDATE messages m SET m.read = 1 WHERE (m.recipient = ?1 AND m.conversation = ?2)")
	void readAllMessages(User user, Conversation conv);
	
	
}
