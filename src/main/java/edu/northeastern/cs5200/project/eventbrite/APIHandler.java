package edu.northeastern.cs5200.project.eventbrite;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Represents the operations to interact and parse data from the EventBrite API
 * @author Nicholas Carugati
 *
 */
public class APIHandler {

	/**
	 * The client secret for the API
	 */
	public static final String CLIENT_SECRET = "3CUCVMF2JNFT577AVWAVOPWWQMDDCRMRJD3HZRD446IB4B64G3";
	
	/**
	 * The OAUTH token for the EventBrite API
	 */
	public static final String OAUTH_TOKEN = "IJH3767MFPDQM6W5BSEU";
	
	/**
	 * The Anonymous OAUTH Token for the EventBrite API
	 */
	public static final String ANONYMOUS_OAUTH_TOKEN = "HYAEGRAHQPVYGGQWN35G";

	/**
	 * Retrieves the search results data for the explore feed
	 * @param handler the query to input into the API
	 * @return the query search payload in JSON
	 */
	public ObjectNode getExploreEvents(EventbriteQuery handler) {	
		try {
			StringBuilder uri = new StringBuilder("https://www.eventbriteapi.com/v3/events/search/");
			
			String queryPayload = handler.manifestURIPayload();
			uri.append(queryPayload).append("&token=").append(OAUTH_TOKEN);
			URL url = new URL(uri.toString());
			System.out.println("URL: " + uri.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			conn.setRequestProperty("Authorization", "Bearer " + OAUTH_TOKEN);
			if(conn.getResponseCode() == 400 || conn.getResponseCode() == 500) {
				return getErrorPayload(conn.getErrorStream());
			}
			InputStream is = conn.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder result = new StringBuilder();
			String output = null;
			while ((output = br.readLine()) != null) {
				result.append(output);
			}
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory factory = mapper.getFactory();
			JsonParser jp = factory.createParser(result.toString());
			JsonNode compositeObject = mapper.readTree(jp);
			JsonNode events = compositeObject.path("events");
			
			ObjectNode compositeNode = mapper.createObjectNode();
			ArrayNode arrNode = mapper.createArrayNode();
			compositeNode.set("events", arrNode);
			if(events.isArray()) {
				for(JsonNode jn : events) {
					ObjectNode node = mapper.createObjectNode();
					node.set("name", jn.path("name").path("text"));
					node.set("description", jn.path("description").path("text"));
					node.set("start", jn.path("start").path("utc"));
					node.set("end", jn.path("end").path("utc"));
					node.set("link", jn.path("url"));
					node.set("img", jn.path("logo").path("original").path("url"));
					arrNode.add(node);
				}
				compositeNode.set("events", arrNode);
			}
			return compositeNode;
		} catch (IOException io) {
			io.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Parses the JSON from EventBrite if an error occurs
	 * @param content the error content
	 * @return The JSON payload to be sent to the client
	 */
	private ObjectNode getErrorPayload(InputStream content) {
		try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(content));
			StringBuilder result = new StringBuilder();
			String output = null;
			while ((output = br.readLine()) != null) {
				result.append(output);
			}
			ObjectMapper mapper = new ObjectMapper();
			JsonFactory factory = mapper.getFactory();
			JsonParser jp = factory.createParser(result.toString());
			JsonNode compositeObject = mapper.readTree(jp);
			
			ObjectNode compositeNode = mapper.createObjectNode();
			compositeNode.set("error_description", compositeObject.path("error_description"));
			compositeNode.set("error", compositeObject.path("error"));
			return compositeNode;
		} catch(IOException io) {
			io.printStackTrace();
			return null;
		}

	}

}
