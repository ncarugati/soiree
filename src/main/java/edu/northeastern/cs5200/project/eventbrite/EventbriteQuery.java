package edu.northeastern.cs5200.project.eventbrite;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import edu.northeastern.cs5200.project.models.Interest;

/**
 * Represents a EventBrite-specific query
 * @author Nicholas Carugati
 *
 */
public class EventbriteQuery {

	/**
	 * Retrieves the predicate if the current query is searching by the user's interest
	 * @return the specified predicate
	 */
	public boolean isSearchingInterests() {
		return searchingInterests;
	}

	/**
	 * Sets the predicate if the user is searching by their interests
	 * @param searchingInterests the new boolean to assign
	 */
	public void setSearchingInterests(boolean searchingInterests) {
		this.searchingInterests = searchingInterests;
	}

	/**
	 * Retrieves the location name of this query
	 * @return the location keyword of the query
	 */
	public String getLocationName() {
		return locationName;
	}

	/**
	 * Assigns a new location name for the query
	 * @param locationName the location name of the query
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * Retrieves the interests that were inserted into the query
	 * @return the interests of the query
	 */
	public Interest[] getInterests() {
		return interests;
	}

	/**
	 * Modifies the interest of the query
	 * @param interests the interest of the query
	 */
	public void setInterests(Interest[] interests) {
		this.interests = interests;
	}

	/**
	 * Retrieves the generic keyword query
	 * @return the keyword query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Assigns a new keyword to the query
	 * @param query the new query keyword to assign
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Instantiates a new query for the EventBrite API
	 * @param interests the user's interests
	 * @param locationName the location to search for
	 * @param query the keyword to search for
	 * @param searchingInterests if the user will search based on interests
	 */
	public EventbriteQuery(Interest[] interests, String locationName, String query, boolean searchingInterests) {
		this.interests = interests;
		this.searchingInterests = searchingInterests;
		this.locationName = locationName;
		this.query = query;
	}
	
	/**
	 * Dummy constructor for JSON serializing
	 */
	public EventbriteQuery() {
		
	}
	
	/**
	 * Creates the URI query for the EventBrite API
	 * @return the uri which will be inserted into the URL request
	 */
	public String manifestURIPayload() {
		StringBuilder payload = new StringBuilder("?");
		if(searchingInterests) {
			if(interests.length > 0) {
				payload.append("categories=");
				for(int i = 0; i < interests.length; i++) {
					if(i == interests.length - 1) {
						payload.append(interests[i].getCategory().getId());
					} else {
						payload.append(interests[i].getCategory().getId()).append(",");
					}
				}
			}
		}
		if(locationName != null && !locationName.isEmpty()) {
			if(!payload.toString().equals("?")) {
				payload.append("&location.address=").append(locationName);
			} else {
				payload.append("location.address=").append(locationName);
			}
		}
		if(query != null && !query.isEmpty()) {
			String encodedQuery = query;
			try {
				encodedQuery = URLEncoder.encode(query, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if(!payload.toString().equals("?")) {
				payload.append("&q=").append(encodedQuery);
			} else {
				payload.append("q=").append(encodedQuery);
			}
		}
		if(!payload.toString().equals("?")) {
			return payload.append("&sort_by=date").toString();
		}
		return payload.append("sort_by=date").toString();
	}

	/**
	 * The location name within the query
	 */
	private String locationName;
	
	/**
	 * The interests within the query
	 */
	private Interest[] interests;
	
	/**
	 * The keyword within the query
	 */
	private String query;
	
	/**
	 * Whether if the query will search based upon interests
	 */
	private boolean searchingInterests = false;

}
