package edu.northeastern.cs5200.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Post;
import edu.northeastern.cs5200.project.models.Rating;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.PostRepository;
import edu.northeastern.cs5200.project.repositories.RatingRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent event role data
 * @author Nicholas Carugati
 *
 */
@RestController
public class RatingController {
	
	/**
	 * The repository which handles all raw MySQL operations for ratings
	 */
	@Autowired
	RatingRepository ratingRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for events
	 */
	@Autowired
	PostRepository postRepository;
	
	/**
	 * Rates a post
	 * @param postId the id of the post to rate
	 * @param rate the rate data
	 * @return the response for the client
	 */
	@PostMapping("/api/post/{pid}/rate")
	public ResponseEntity<Rating> ratePost(@PathVariable("pid") int postId, @RequestBody Rating rate) {
		Rating rating = rate;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Rating>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Post post = postRepository.findOne(postId);
		rating.setPost(post);
		rating.setUser(user);
		rating.setRating(rate.getRating());
		ratingRepository.save(rating);
		return new ResponseEntity<Rating>(rating, HttpStatus.OK);
	}
	
	/**
	 * Retrieves a rating for the session user (if available)
	 * @param postId the id of the post to search for 
	 * @return the value of the rating the session user gave
	 */
	@GetMapping("/api/post/{pid}/rate")
	public ResponseEntity<Rating> findRatingForUser(@PathVariable("pid") int postId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Rating>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Post post = postRepository.findOne(postId);
		Rating rating = ratingRepository.findByUserAndPost(user, post);
		return new ResponseEntity<Rating>(rating, HttpStatus.OK);
	}
	
	/**
	 * Alters an existing rating
	 * @param rateInfo the rate data
	 * @param postId the id of the post to alter the session user's rating
	 * @return the response for the client
	 */
	@PutMapping("/api/post/{pid}/rate")
	public ResponseEntity<Rating> modifyRating(@RequestBody Rating rateInfo, @PathVariable("pid") int postId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Rating>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Post post = postRepository.findOne(postId);
		Rating rating = ratingRepository.findByUserAndPost(user, post);
		rating.setRating(rateInfo.getRating());
		ratingRepository.save(rating);
		return new ResponseEntity<Rating>(rating, HttpStatus.OK);
	}
	
	/**
	 * Removes a rating for the session user
	 * @param postId the id of the post to remove the rating for
	 * @return the response for the client
	 */
	@DeleteMapping("/api/post/{pid}/rate")
	public ResponseEntity<Rating> removeRating(@PathVariable("pid") int postId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Rating>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Post post = postRepository.findOne(postId);
		Rating rating = ratingRepository.findByUserAndPost(user, post);
		ratingRepository.delete(rating.getId());
		return new ResponseEntity<Rating>(rating, HttpStatus.OK);
	}


}
