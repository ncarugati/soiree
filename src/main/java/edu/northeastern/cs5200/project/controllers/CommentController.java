package edu.northeastern.cs5200.project.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Comment;
import edu.northeastern.cs5200.project.models.Post;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.CommentRepository;
import edu.northeastern.cs5200.project.repositories.PostRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

@RestController
public class CommentController {

	/**
	 * The repository which handles all raw MySQL operations for comments
	 */
	@Autowired
	CommentRepository commentRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for posts
	 */
	@Autowired
	PostRepository postRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * Creates a new comment for a post
	 * @param postId the id of the post to create a client for
	 * @param comment the comment's contents
	 * @return the response for the client
	 */
	@PostMapping("/api/post/{pid}/comment")
	public ResponseEntity<Comment> createComment(@PathVariable("pid") int postId, @RequestBody Comment comment) {
		Comment com = comment;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Comment>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Post post = postRepository.findOne(postId);
		if(!post.isLocked()) {
			com.setOwner(user);
			com.setPost(post);
			commentRepository.save(com);
			return new ResponseEntity<Comment>(com, HttpStatus.OK);
		} 
		return new ResponseEntity<Comment>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves the list of comments for a post
	 * @param postId the id of the post to retrieve comments for
	 * @return the list of comments for a post
	 */
	@GetMapping("/api/post/{pid}/comment")
	public ResponseEntity<Set<Comment>> getCommentsForPost(@PathVariable("pid") int postId) {
		Post post = postRepository.findOne(postId);
		return new ResponseEntity<Set<Comment>>(post.getComments(), HttpStatus.OK);
	}
	
	/**
	 * Updates a single comment
	 * @param commentInfo the new comment data
	 * @param commentId the id of the comment to modify
	 * @return the response for the client
	 */
	@PutMapping("/api/comment/{cid}")
	public ResponseEntity<Comment> updateComment(@RequestBody Comment commentInfo, @PathVariable("cid") int commentId) {
		Comment com = commentRepository.findOne(commentId);
		com.setContent(commentInfo.getContent());
		commentRepository.save(com);
		return new ResponseEntity<Comment>(com, HttpStatus.OK);
	}
	
	/**
	 * Removes a comment from a post
	 * @param commentId the id of the comment to remove
	 * @return the response for the client
	 */
	@DeleteMapping("/api/comment/{cid}")
	public ResponseEntity<Comment> deleteComment(@PathVariable("cid") int commentId) {
		commentRepository.delete(commentId);
		return new ResponseEntity<Comment>(HttpStatus.OK);
	}
	
}
