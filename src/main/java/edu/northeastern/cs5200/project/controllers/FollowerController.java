package edu.northeastern.cs5200.project.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Follower;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.FollowerRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent follower data
 * @author Nicholas Carugati
 *
 */
@RestController
public class FollowerController {
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for attendee instances
	 */
	@Autowired
	FollowerRepository followerRepository;

	/**
	 * Follows a user
	 * @param followingId the id of the user to follow
	 * @return the response for the client
	 */
	@PostMapping("/api/user/follow/{uid}")
	public ResponseEntity<User> follow(@PathVariable("uid") String followingId) {
		Follower follower = new Follower();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<User>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		User following = userRepository.findOne(followingId);
		Follower pairCheck = followerRepository.findByFollowerAndFollowee(following, user);
		if(pairCheck != null) {
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		follower.setFollower(following);
		follower.setFollowee(user);
		followerRepository.save(follower);
		return new ResponseEntity<User>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves follower data for a user
	 * @param userId the userId to get the list of followers for
	 * @return the list of followers for a specific user
	 */
	@GetMapping("/api/user/follow/{uid}/follower")
	public ResponseEntity<List<Follower>> getFollowerData(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		List<Follower> followers = followerRepository.findByFollower(user);
		return new ResponseEntity<List<Follower>>(followers, HttpStatus.OK);
	}
	
	/**
	 * Retrieves following data for a specific user
	 * @param userId the userId to get the list of users that the user is following
	 * @return the list of users that the user is following
	 */
	@GetMapping("/api/user/follow/{uid}/following")
	public ResponseEntity<List<Follower>> getFolloweeData(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		List<Follower> followee = followerRepository.findByFollowee(user);
		return new ResponseEntity<List<Follower>>(followee, HttpStatus.OK);
	}
	
	/**
	 * Determines if the session user is following a specified user
	 * @param userId the userId to search for
	 * @return the boolean if the user is being followed by the session user
	 */
	@GetMapping("/api/user/follow/{uid}/isfollow")
	public ResponseEntity<Boolean> isFollowing(@PathVariable("uid") String userId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}
		String sessionUserId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User sessionUser = userRepository.findOne(sessionUserId);
		User user = userRepository.findOne(userId);
		Follower follow = followerRepository.findByFollowerAndFollowee(user, sessionUser);
		if(follow == null) {
			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	/**
	 * Retrieves a composite data count of followers/following for a user
	 * @param userId the userId to get the data for
	 * @return the brief array of followers in an integer array
	 */
	@GetMapping("/api/user/follow/{uid}/count")
	public ResponseEntity<Integer[]> getFollowerDataCountForUser(@PathVariable("uid") String userId) {
		Integer[] followData = new Integer[2];
		User user = userRepository.findOne(userId);
		List<Follower> followings = followerRepository.findByFollowee(user);
		List<Follower> followers = followerRepository.findByFollower(user);
		followData[0] = followings.size();
		followData[1] = followers.size();
		return new ResponseEntity<Integer[]>(followData, HttpStatus.OK);
	}
	
	/**
	 * Retrieves all follower/following data pairs for a user
	 * @param userId the userId to get the data for
	 * @return the dual list of follower/following pairs for a user
	 */
	@GetMapping("/api/user/follow/{uid}")
	public ResponseEntity<List<List<Follower>>> getFollowerDataForUser(@PathVariable("uid") String userId) {
		List<List<Follower>> followData = new ArrayList<List<Follower>>();
		User user = userRepository.findOne(userId);
		List<Follower> followings = followerRepository.findByFollowee(user);
		List<Follower> followers = followerRepository.findByFollower(user);
		followData.add(followings);
		followData.add(followers);
		return new ResponseEntity<List<List<Follower>>>(followData, HttpStatus.OK);
	}
	
	/**
	 * Unfollows a user
	 * @param followingId the id of the user to unfollow
	 * @return the response to the client
	 */
	@DeleteMapping("/api/user/follow/{uid}")
	public ResponseEntity<Follower> unfollow(@PathVariable("uid") String followingId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Follower>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);		
		User following = userRepository.findOne(followingId);
		Follower follower = followerRepository.findByFollowerAndFollowee(following, user);
		if(follower == null) {
			return new ResponseEntity<Follower>(HttpStatus.BAD_REQUEST);
		}
		followerRepository.delete(follower);
		return new ResponseEntity<Follower>(HttpStatus.OK);
	}
	
}
