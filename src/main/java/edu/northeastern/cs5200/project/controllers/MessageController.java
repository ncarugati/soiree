package edu.northeastern.cs5200.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Conversation;
import edu.northeastern.cs5200.project.models.Message;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.ConversationRepository;
import edu.northeastern.cs5200.project.repositories.MessageRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent message data
 * @author Nicholas Carugati
 *
 */
@RestController
public class MessageController {
	
	/**
	 * The repository which handles all raw MySQL operations for messages
	 */
	@Autowired
	MessageRepository messageRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for conversations
	 */
	@Autowired
	ConversationRepository conversationRepository;
	
	/**
	 * Creates a new personal message
	 * @param recipientId the recipient who is going to receive the message
	 * @param content the content of the message
	 * @return the response for the user
	 */
	@PostMapping("/api/message/{uid}")
	public ResponseEntity<Message> sendPersonalMessage(@PathVariable("uid") String recipientId, @RequestBody Message content) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Message>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User sender = userRepository.findOne(userId);
		User recipient = userRepository.findOne(recipientId);
		Conversation conv = conversationRepository.findConversationByParticipants(sender, recipient);
		Message message = new Message();
		message.setSender(sender);
		message.setRecipient(recipient);
		message.setMessage(content.getMessage());
		if(conv != null) {
			message.setConversation(conv);
		} else {
			conv = createNewConversation(sender, recipient);
			message.setConversation(conv);
		}
		messageRepository.save(message);
		return new ResponseEntity<Message>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves all of the conversations for a user's inbox
	 * @return the list of conversations for the seession user
	 */
	@GetMapping("/api/message")
	public ResponseEntity<List<Message>> getConversations() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<List<Message>>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		List<Message> conversations = messageRepository.findConversationsForUser(userId);
		return new ResponseEntity<List<Message>>(conversations, HttpStatus.OK);
	}
	
	/**
	 * Archives a message for a user
	 * @param messageId the id of the message to archive
	 * @return the response for the client
	 */
	@PutMapping("/api/message/{mid}/archive")
	public ResponseEntity<Message> archiveMessage(@PathVariable("mid") int messageId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Message>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		Message message = messageRepository.findOne(messageId);
		String senderId = message.getSender().getId();
		if(senderId.equals(userId)) {
			message.setSenderArchived(!message.isSenderArchived());
		} else {
			message.setRecipientArchived(!message.isRecipientArchived());
		}
		messageRepository.save(message);
		return new ResponseEntity<Message>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves all of the messages for an administrator
	 * @return the list of all the messages ever sent by a user
	 */
	@GetMapping("/api/message/all")
	public ResponseEntity<List<Message>> getAllMessages() {
		return new ResponseEntity<List<Message>>(messageRepository.findAll(), HttpStatus.OK);
	}
	
	/**
	 * Edits an existing message
	 * @param messageId the id of the message to edit
	 * @param message the message data
	 * @return the resposne for the client
	 */
	@PutMapping("/api/message/admin/{mid}")
	public ResponseEntity<Message> editMessage(@PathVariable("mid") int messageId, @RequestBody Message message) {
		Message mess = messageRepository.findOne(messageId);
		mess.setMessage(message.getMessage());
		messageRepository.save(mess);
		return new ResponseEntity<Message>(HttpStatus.OK);
	}
	
	/**
	 * Deletes a message
	 * @param messageId the id of the message to delete
	 * @return the response for the client
	 */
	@DeleteMapping(value = "/api/message/admin/{mid}")
	public ResponseEntity<Message> deleteMessage(@PathVariable("mid") int messageId) {
		messageRepository.delete(messageId);
		return new ResponseEntity<Message>(HttpStatus.OK);
	}
	
	/**
	 * Utility routine which creates a new conversation
	 * @param primary the primary person who will be talking to the secondary
	 * @param secondary the secondary participant who will be interacting with the primary
	 * @return the newly applied conversation
	 */
	public Conversation createNewConversation(User primary, User secondary) {
		Conversation conv = new Conversation();
		conv.setParticipantOne(primary);
		conv.setParticipantTwo(secondary);
		return conversationRepository.save(conv);
	}

}
