package edu.northeastern.cs5200.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.EventAssociationRepository;
import edu.northeastern.cs5200.project.repositories.EventRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The controller which handles web page rendering
 * @author Nicholas Carugati
 *
 */
@Controller
public class RenderController {
	
	/**
	 * The repository which handles all raw MySQL operations for events
	 */
	@Autowired
	private EventRepository eventRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	private UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for event association pairs
	 */
	@Autowired
	private EventAssociationRepository eventAssociationRepository;
	
	/**
	 * Renders the page based on the configured routes
	 * @return the single page application view
	 */
	@GetMapping(value = {"/", "/login", "/register" })
	public String loadPublicViews() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth.getPrincipal() instanceof String)) {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			return "redirect:/main/" + userId;
		}
		return "/index.html";
	}
	
	/**
	 * Loads the admin page
	 * Spring security manages the route security for this one
	 * @return the main index page
	 */
	@GetMapping("/admin")
	public String loadAdminPage() {
		return "/index.html";
	}
	
	/**
	 * View which handles the profile and in-depth information about a user
	 * @param uid The userId of the profile
	 * @return the webpage route depending on permissions
	 */
	@GetMapping(value = {"/profile/{uid}", "/profile/{uid}/**"})
	public String loadProfileView(@PathVariable("uid") String uid) {
		try {
			User user = userRepository.findOne(uid);
			if(user == null) {
				return "redirect:/404"; 
			}
		} catch(Exception e) {
			return "redirect:/404"; 
		}
		return "/index.html";
	}
	
	/**
	 * Loads the pages related to events
	 * @param eid The event id of the user
	 * @return the webpage route depending on permissions
	 */
	@GetMapping(value = {"/events/{eid}", "/events/{eid}/**"})
	public String loadEventView(@PathVariable("eid") int eid) {
		try {
			Event event = eventRepository.findOne(eid);
			if(event == null) {
				return "redirect:/404"; 
			}
		} catch(Exception e) {
			e.printStackTrace();
			return "redirect:/error"; 
		}
		return "/index.html";
	}
	
	/**
	 * Loads settings page for an event
	 * @param eid the event id of the user
	 * @return the webpage route depending on permissions
	 */
	@GetMapping("/events/{eid}/settings")
	public String loadEventsettingsView(@PathVariable("eid") int eid) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth.getPrincipal() instanceof String)) {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			try {
				Event event = eventRepository.findOne(eid);
				User user = userRepository.findOne(userId);
				boolean isOwner = event.getOwner().getId().equals(userId);
				if(event == null || (eventAssociationRepository.findByUserAndEvent(user, event) == null && !isOwner)) {
					return "redirect:/404"; 
				}
				return "/index.html";
			} catch(Exception e) {
				e.printStackTrace();
				return "redirect:/error"; 
			}
		}
		return "redirect:/404";
	}
	
	/**
	 * Loads the public search page
	 * @return the public search page
	 */
	@GetMapping("/search")
	public String loadVariablePublicViews() {
		return "/index.html";
	}
	
	/**
	 * Loads the feed page for a logged in user
	 * @param uid the id of the user
	 * @return the page to load for the user
	 */
	@GetMapping("/main/{id}")
	public String loadFeedView(@PathVariable("id") String uid) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth.getPrincipal() instanceof String)) {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			if(uid.equals(userId)) {
				return "/index.html";
			}
			return "redirect:" + userId;
		}
		return "redirect:/login";
	}
	
	/**
	 * Loads the additional pages that only the session user has access to
	 * @param uid the userId of the user
	 * @return the web page route depending on permissions
	 */
	@GetMapping("/main/{id}/**")
	public String renderSettings(@PathVariable("id") String uid) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth.getPrincipal() instanceof String)) {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			if(uid.equals(userId)) {
				return "/index.html";
			}
			return "redirect:/main/" + userId;
		}
		return "redirect:/login";
	}

	
}