package edu.northeastern.cs5200.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.models.EventRole;
import edu.northeastern.cs5200.project.repositories.EventRoleRepository;

/**
 * The REST controller which retrieves pertinent event role data
 * @author Nicholas Carugati
 *
 */
@RestController
public class EventRoleController {

	/**
	 * The repository which handles all raw MySQL operations
	 */
	@Autowired
	EventRoleRepository eventRoleRepository;

	/**
	 * Retrieves the category list from the database
	 * @return the list of categories
	 */
	@GetMapping("/api/event/roles")
	public ResponseEntity<List<EventRole>> getCategoryList() {
		return new ResponseEntity<List<EventRole>>(eventRoleRepository.findAll(), HttpStatus.OK);
	}

}
