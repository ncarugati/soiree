package edu.northeastern.cs5200.project.controllers;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * The REST operations which manage file uploading
 * @see http://www.mkyong.com/spring-boot/spring-boot-file-upload-example/
 * @author Mkyong
 * @author Nicholas Carugati
 *
 */
@RestController
public class FileUploadController {
	
	/**
	 * Uploads a file to the server
	 * @param file the file contents
	 * @return the relative path located on the server
	 */
	@SuppressWarnings("unused")
	@PostMapping("/api/upload")
	@ResponseBody
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			if (file.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} 
			byte[] buffer = new byte[4096];
			InputStream stream = file.getInputStream();
			String path = "uploads/" + file.getOriginalFilename();
			OutputStream out = new FileOutputStream(path);
			int position;
			while ((position = stream.read(buffer)) != -1) {
				out.write(buffer);
			}
			out.close();
			stream.close();
			return new ResponseEntity<>(path.toString(), HttpStatus.OK);
		} catch (IOException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
