package edu.northeastern.cs5200.project.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.northeastern.cs5200.SearchHandler;
import edu.northeastern.cs5200.project.eventbrite.APIHandler;
import edu.northeastern.cs5200.project.eventbrite.EventbriteQuery;

/**
 * The REST controller which handles the search module
 * @author Nicholas Carugati
 *
 */
@RestController
public class SearchController {
	
	/**
	 * The search service which processes the query and handles Apache lucene's indexing
	 */
	@Autowired
	private SearchHandler searchHandler;

	/**
	 * Performs a search for a user
	 * @param query the query to run the search on
	 * @return the composite search results for users, events, and EventBrite data
	 * @throws IOException if the request body is processed incorrectly.
	 */
	@GetMapping("/api/search")
	public ResponseEntity<List<List<Object>>> performSearch(@RequestParam("query") String query) throws IOException {
		List<List<Object>> compositeResults = new ArrayList<List<Object>>();
		if(query.isEmpty()) {
			return new ResponseEntity<List<List<Object>>>(compositeResults, HttpStatus.OK);
		}
		compositeResults = searchHandler.compositeSearch(query);
		APIHandler handler = new APIHandler();
		EventbriteQuery briteQuery = new EventbriteQuery();
		briteQuery.setSearchingInterests(false);
		briteQuery.setQuery(query);
		ObjectNode node = handler.getExploreEvents(briteQuery);
		List<Object> eventBriteComposite = new ArrayList<Object>();
		eventBriteComposite.add(node);
		compositeResults.add(eventBriteComposite);
		return new ResponseEntity<List<List<Object>>>(compositeResults, HttpStatus.OK);
	}

}
