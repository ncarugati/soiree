package edu.northeastern.cs5200.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Conversation;
import edu.northeastern.cs5200.project.models.Message;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.ConversationRepository;
import edu.northeastern.cs5200.project.repositories.MessageRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent conversation data
 * @author Nicholas Carugati
 *
 */
@RestController
public class ConversationController {

	/**
	 * The repository which handles all raw MySQL operations for messages
	 */
	@Autowired
	MessageRepository messageRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for conversations
	 */
	@Autowired
	ConversationRepository conversationRepository;

	/**
	 * Reads all of the messages in a conversation
	 * @param conversationId the id of the conversation to read messages for
	 * @return the response for the client
	 */
	@PutMapping("/api/message/conversation/{cid}/read")
	public ResponseEntity<Conversation> readAllMessages(@PathVariable("cid") int conversationId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Conversation>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Conversation conv = conversationRepository.findOne(conversationId);
		messageRepository.readAllMessages(user, conv);
		return new ResponseEntity<Conversation>(HttpStatus.OK);
	}

	/**
	 * Retrieves all of the messages associated with a conversation
	 * @param conversationId the id of the conversation to get messages for
	 * @return the list of messages for a conversation
	 */
	@GetMapping("/api/message/conversation/{cid}")
	public ResponseEntity<List<Message>> getMessagesByConversationId(@PathVariable("cid") int conversationId) {
		Conversation convo = conversationRepository.findOne(conversationId);
		List<Message> conversationContents = convo.getMessages();
		return new ResponseEntity<List<Message>>(conversationContents, HttpStatus.OK);
	}

	/**
	 * Retrieves the conversation by its id
	 * @param conversationId the id of the conversation to get
	 * @return the conversation for the client
	 */
	@GetMapping("/api/conversation/{cid}")
	public ResponseEntity<Conversation> getConversationById(@PathVariable("cid") int conversationId) {
		Conversation convo = conversationRepository.findOne(conversationId);
		return new ResponseEntity<Conversation>(convo, HttpStatus.OK);
	}
	
	/**
	 * Archives a conversation for a client
	 * @param conversationId the id of the conversation to archive
	 * @return the response for the client
	 */
	@PutMapping("/api/conversation/{cid}")
	public ResponseEntity<Conversation> archiveConversation(@PathVariable("cid") int conversationId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Conversation>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		Conversation convo = conversationRepository.findOne(conversationId);
		if(convo.getParticipantOne().getId().equals(userId)) {
			convo.setPartOneArchived(!convo.isPartOneArchived());
		} else {
			convo.setPartTwoArchived(!convo.isPartTwoArchived());
		}
		conversationRepository.save(convo);
		return new ResponseEntity<Conversation>(convo, HttpStatus.OK);
	}

}
