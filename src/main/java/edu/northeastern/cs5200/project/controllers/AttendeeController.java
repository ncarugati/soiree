package edu.northeastern.cs5200.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Attendee;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.AttendeeRepository;
import edu.northeastern.cs5200.project.repositories.EventRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent attendee data
 * @author Nicholas Carugati
 *
 */
@RestController
public class AttendeeController {
	
	/**
	 * The repository which handles all raw MySQL operations
	 */
	@Autowired
	EventRepository eventRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for attendee instances
	 */
	@Autowired
	AttendeeRepository attendeeRepository;

	/**
	 * An action which has a user attend an event
	 * @param eventId the eventId for the session user to attend
	 * @return the response for the client
	 */
	@PostMapping("/api/event/{eid}/attend")
	public ResponseEntity<Attendee> attendEvent(@PathVariable("eid") int eventId) {
		Attendee a = new Attendee();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Attendee>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Event event = eventRepository.findOne(eventId);
		a.setAttendee(user);
		a.setEvent(event);
		attendeeRepository.save(a);
		return new ResponseEntity<Attendee>(HttpStatus.OK);
	}
	
	/**
	 * Removes attendance status for a user
	 * @param eventId the event to remove attendance status for
	 * @return the response for the client
	 */
	@DeleteMapping("/api/event/{eid}/attend")
	public ResponseEntity<Attendee> rescindAttendance(@PathVariable("eid") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Attendee>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Event event = eventRepository.findOne(eventId);
		Attendee a = attendeeRepository.findByEventAndAttendee(event, user);
		attendeeRepository.delete(a);
		return new ResponseEntity<Attendee>(HttpStatus.OK);
	}
	
	/**
	 * An endpoint which determines if the session user is attending an event
	 * @param eventId the event in which to check
	 * @return the response for the client
	 */
	@GetMapping("/api/event/{eid}/attend")
	public ResponseEntity<Boolean> isUserAttending(@PathVariable("eid") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Boolean>(false, HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Event event = eventRepository.findOne(eventId);
		Attendee att = attendeeRepository.findByEventAndAttendee(event, user);
		if(att == null) {
			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

}
