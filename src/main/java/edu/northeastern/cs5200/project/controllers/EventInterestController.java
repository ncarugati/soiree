package edu.northeastern.cs5200.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.EventInterest;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.EventInterestRepository;
import edu.northeastern.cs5200.project.repositories.EventRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent event interest data
 * @author Nicholas Carugati
 *
 */
@RestController
public class EventInterestController {
	
	/**
	 * The repository which handles all raw MySQL operations
	 */
	@Autowired
	EventRepository eventRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for attendee instances
	 */
	@Autowired
	EventInterestRepository eventInterestRepository;

	/**
	 * Attends the session user to an event
	 * @param eventId the id of the event to attend to
	 * @return the response for the client
	 */
	@PostMapping(value = "/api/event/{eid}/interest")
	public ResponseEntity<EventInterest> attendEvent(@PathVariable("eid") int eventId) {
		EventInterest ei = new EventInterest();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<EventInterest>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Event event = eventRepository.findOne(eventId);
		ei.setUser(user);
		ei.setEvent(event);
		eventInterestRepository.save(ei);
		return new ResponseEntity<EventInterest>(HttpStatus.OK);
	}
	
	/**
	 * Declines the attendance for a user
	 * @param eventId the id of the event to decline attendance for
	 * @return the response for the client
	 */
	@DeleteMapping("/api/event/{eid}/interest")
	public ResponseEntity<EventInterest> rescindAttendance(@PathVariable("eid") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<EventInterest>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Event event = eventRepository.findOne(eventId);
		EventInterest ei = eventInterestRepository.findByEventAndUser(event, user);
		eventInterestRepository.delete(ei);
		return new ResponseEntity<EventInterest>(HttpStatus.OK);
	}
	
	/**
	 * Determines if the session user is attending an event
	 * @param eventId the id of the event to search for
	 * @return the boolean response for the client
	 */
	@GetMapping("/api/event/{eid}/interest")
	public ResponseEntity<Boolean> isUserAttending(@PathVariable("eid") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Boolean>(false, HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Event event = eventRepository.findOne(eventId);
		EventInterest ei = eventInterestRepository.findByEventAndUser(event, user);
		if(ei == null) {
			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}

}
