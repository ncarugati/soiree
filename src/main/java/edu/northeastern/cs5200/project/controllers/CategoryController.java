package edu.northeastern.cs5200.project.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.models.Category;
import edu.northeastern.cs5200.project.repositories.CategoryRepository;

/**
 * The REST controller which retrieves pertinent category data
 * @author Nicholas Carugati
 *
 */
@RestController
public class CategoryController {
	
	/**
	 * Manages database operations for the category data model
	 */
	@Autowired
	CategoryRepository categoryRepository;

	/**
	 * Retrieves the list of all categories for a user
	 * @return the list of all categories in the database.
	 */
	@GetMapping("/api/event/categories")
	public ResponseEntity<List<Category>> getCategoryList() {
		return new ResponseEntity<List<Category>>(categoryRepository.findAll(), HttpStatus.OK);
	}

}
