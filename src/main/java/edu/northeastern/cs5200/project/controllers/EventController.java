package edu.northeastern.cs5200.project.controllers;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.eventbrite.APIHandler;
import edu.northeastern.cs5200.project.eventbrite.EventbriteQuery;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.EventPost;
import edu.northeastern.cs5200.project.models.Interest;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.EventRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent event data
 * @author Nicholas Carugati
 *
 */
@RestController
public class EventController {

	/**
	 * The repository which handles all raw MySQL operations
	 */
	@Autowired
	EventRepository eventRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;

	/**
	 * Creates a new event
	 * @param event the new event data
	 * @return the response for the client
	 */
	@PostMapping("/api/event")
	public ResponseEntity<Event> createEvent(@RequestBody Event event) {
		Event ev = event;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Event>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		if(user.getType().equals("ADMIN")) {
			ev.setOwner(event.getOwner());
		} else {
			ev.setOwner(user);
		}
		eventRepository.save(ev);
		return new ResponseEntity<Event>(ev, HttpStatus.OK);
	}
	
	/**
	 * Finds all of the events by a user
	 * @param userId the user to search for
	 * @return the list of events owned by the user
	 */
	@GetMapping(value = "/api/event/user/{uid}")
	public ResponseEntity<Set<Event>> getEventsByUser(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		Set<Event> events = eventRepository.findByOwnerOrderByTimestampDesc(user);
		return new ResponseEntity<Set<Event>>(events, HttpStatus.OK);
	}
	
	/**
	 * Retrieves an event by its id
	 * @param eventId the event id to search for
	 * @return the event and its data.
	 */
	@GetMapping("/api/event/{eid}")
	public ResponseEntity<Event> findEventById(@PathVariable("eid") int eventId) {
		Event evt = eventRepository.findOne(eventId);
		return new ResponseEntity<Event>(evt, HttpStatus.OK);
	}
	
	/**
	 * Retrieves brief stats regarding the event
	 * @param eventId the id of the event
	 * @return the stats of the event
	 */
	@GetMapping("/api/event/{eid}/stats")
	public ResponseEntity<List<Integer>> getEventStats(@PathVariable("eid") int eventId) {
		List<Integer> values = new ArrayList<Integer>();
		Event evt = eventRepository.findOne(eventId);
		values.add(evt.getAttending().size());
		values.add(evt.getInterestedUsers().size());
		return new ResponseEntity<List<Integer>>(values, HttpStatus.OK);
	}
	
	/**
	 * Updates an existing event
	 * @param eventInfo the event data flattened
	 * @param eventId the id to adjust
	 * @return the response for the client
	 */
	@PutMapping("/api/event/{eid}")
	public ResponseEntity<Event> updateEvent(@RequestBody ObjectNode eventInfo, @PathVariable("eid") int eventId) {
		Event evt = eventRepository.findOne(eventId);
		if(!eventInfo.path("name").asText().equals("null")) {
			evt.setName(eventInfo.path("name").asText());
		}
		if(!eventInfo.path("name").asText().equals("null")) {
			evt.setName(eventInfo.path("name").asText());
		}
		
		Calendar cal = null;
		if(!eventInfo.path("timeStart").asText().equals("null")) {
			cal = DatatypeConverter.parseDateTime(eventInfo.path("timeStart").asText());
			evt.setTimeStart(cal.getTime());
		}
		if(!eventInfo.path("timeEnd").asText().equals("null")) {
			cal = DatatypeConverter.parseDateTime(eventInfo.path("timeEnd").asText());
			evt.setTimeEnd(cal.getTime());
		}
		if(!eventInfo.path("location").asText().equals("null")) {
			evt.setLocation(eventInfo.path("location").asText());
		}
		if(!eventInfo.path("admission").asText().equals("null")) {
			evt.setAdmission(eventInfo.path("admission").asDouble());
		}
		if(!eventInfo.path("description").asText().equals("null")) {
			evt.setDescription(eventInfo.path("description").asText());
		}
		String imgPath = eventInfo.path("image").asText();
		if(!imgPath.equals("null")) {
			evt.setImage(imgPath);
		}
		eventRepository.save(evt);
		return new ResponseEntity<Event>(evt, HttpStatus.OK);
	}
	
	/**
	 * Removes an event
	 * @param eventId the id of the event to remove
	 * @return the response provided by the client
	 */
	@DeleteMapping("/api/event/{eid}")
	public ResponseEntity<Event> deleteEvent(@PathVariable("eid") int eventId) {
		eventRepository.delete(eventId);
		return new ResponseEntity<Event>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves all of the posts for an event
	 * @param eventId the id of the event to search for
	 * @return the list of postings for a particular event
	 */
	@GetMapping("/api/event/{eid}/posts")
	public ResponseEntity<List<EventPost>> getEventPosts(@PathVariable("eid") int eventId) {
		Event ev = eventRepository.findOne(eventId);
		Set<EventPost> events = ev.getPosts();
		List<EventPost> postList = new ArrayList<EventPost>();
		postList.addAll(events);
		postList.sort(new Comparator<EventPost>() {
			@Override
			public int compare(EventPost o1, EventPost o2) {
				Date d1 = o1.getPost().getTimestamp();
				Date d2 = o2.getPost().getTimestamp();
				return d2.compareTo(d1);
			}
		});
		return new ResponseEntity<List<EventPost>>(postList, HttpStatus.OK);
	}
	
	/**
	 * Retrieves the EventBrite explore data
	 * @param query the query data for the feed
	 * @return the search results for the client
	 */
	@PostMapping("/api/event/explore")
	public ResponseEntity<Object> getExploreListing(@RequestBody EventbriteQuery query) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		query.setInterests(user.getInterests().toArray(new Interest[user.getInterests().size()]));
		ObjectNode exploreTuple;
		exploreTuple = new APIHandler().getExploreEvents(query);
		return new ResponseEntity<Object>(exploreTuple, HttpStatus.OK);
	}
	
	/**
	 * Retrieves the list of events for a user's followers
	 * @return the list of events for the events that the session user's followees own
	 */
	@GetMapping("/api/event/following")
	public ResponseEntity<Set<Event>> getFollowerEventListing() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Set<Event>>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		Set<Event> followingEvents = eventRepository.getFollowerEvents(user);
		return new ResponseEntity<Set<Event>>(followingEvents, HttpStatus.OK);
	}
	
	/**
	 * Retrieves the general event feed for the whole application
	 * @return the list of events on the database except for the ones the session user owns
	 */
	@GetMapping("/api/event/feed")
	public ResponseEntity<Set<Event>> getEventFeed() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Set<Event>>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		User user = userRepository.findOne(userId);
		return new ResponseEntity<Set<Event>>(eventRepository.getEventFeed(user), HttpStatus.OK);
	}
	
	/**
	 * Retrieves all of the events for a user
	 * @return the list of all the events include the ones which a session user owns.
	 */
	@GetMapping("/api/event")
	public ResponseEntity<List<Event>> getAllEvents() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<List<Event>>(HttpStatus.FORBIDDEN);
		}
		return new ResponseEntity<List<Event>>(eventRepository.findAll(), HttpStatus.OK);
	}
	
	
	
	
}
