package edu.northeastern.cs5200.project.controllers;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.EventAssociation;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.EventAssociationRepository;
import edu.northeastern.cs5200.project.repositories.EventRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent event association data
 * @author Nicholas Carugati
 *
 */
@RestController
public class EventAssociationController {
	
	/**
	 * The repository which handles all raw MySQL operations
	 */
	@Autowired
	EventRepository eventRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for event association pairs
	 */
	@Autowired
	EventAssociationRepository eventAssociationRepository;

	/**
	 * Creates event rights for a user
	 * @param eventId the event id to create rights for
	 * @param event the rights data
	 * @return the response for the client
	 */
	@PostMapping("/api/event/{eid}/rights")
	public ResponseEntity<EventAssociation> createRights(@PathVariable("eid") int eventId, @RequestBody EventAssociation event) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<EventAssociation>(HttpStatus.FORBIDDEN);
		} else {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			if(!isEventOwner(userId, eventId)) {
				return new ResponseEntity<EventAssociation>(HttpStatus.UNAUTHORIZED);
			}
		}
		Event e = eventRepository.findOne(eventId);
		event.setEvent(e);
		eventAssociationRepository.save(event);
		return new ResponseEntity<EventAssociation>(event, HttpStatus.OK);
	}
	
	/**
	 * Gets the list of rights for an event
	 * @param eventId the event id to get the list of rights for
	 * @return the event-user rights pairs
	 */
	@GetMapping("/api/event/{eid}/rights")
	public ResponseEntity<Set<EventAssociation>> getAllRightsForEvent(@PathVariable("eid") int eventId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Set<EventAssociation>>(HttpStatus.FORBIDDEN);
		} else {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			if(!isEventOwner(userId, eventId)) {
				return new ResponseEntity<Set<EventAssociation>>(HttpStatus.UNAUTHORIZED);
			}
		}
		Event e = eventRepository.findOne(eventId);
		return new ResponseEntity<Set<EventAssociation>>(e.getEventRights(), HttpStatus.OK);
	}
	
	/**
	 * Retrieves the rights for a single user
	 * @param eventId the event to search for
	 * @param userId the user to search for
	 * @return The response for the client
	 */
	@GetMapping("/api/event/{eid}/rights/{uid}")
	public ResponseEntity<EventAssociation> findRightsForUser(@PathVariable("eid") int eventId, 
															@PathVariable("uid") String userId) {
		Event event = eventRepository.findOne(eventId);
		User user = userRepository.findOne(userId);
		EventAssociation entry = eventAssociationRepository.findByUserAndEvent(user, event);
		if(entry == null) {
			return new ResponseEntity<EventAssociation>(HttpStatus.OK);
		}
		return new ResponseEntity<EventAssociation>(entry, HttpStatus.OK);
	}
	
	/**
	 * Modifies the rights for a user that has existing rights to an event
	 * @param eventId the id to search the right for
	 * @param rightsId the new right id
	 * @param event the event association data
	 * @return the response for the client
	 */
	@PutMapping("/api/event/{eid}/rights/{rid}")
	public ResponseEntity<EventAssociation> modifyRightsForUser(@PathVariable("eid") int eventId, 
			@PathVariable("rid") int rightsId, 
			@RequestBody EventAssociation event) {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<EventAssociation>(HttpStatus.FORBIDDEN);
		} else {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			if(!isEventOwner(userId, eventId)) {
				return new ResponseEntity<EventAssociation>(HttpStatus.UNAUTHORIZED);
			}
		}
		EventAssociation ea = eventAssociationRepository.findOne(rightsId);
		ea.setRole(event.getRole());
		eventAssociationRepository.save(ea);
		return new ResponseEntity<EventAssociation>(HttpStatus.OK);
	}
	
	/**
	 * Revokes rights for a user
	 * @param eventId the event to search for
	 * @param rightsId the rights instance id to find
	 * @return the response for the client
	 */
	@DeleteMapping("/api/event/{eid}/rights/{rid}")
	public ResponseEntity<EventAssociation> revokeRights(@PathVariable("eid") int eventId, @PathVariable("rid") int rightsId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<EventAssociation>(HttpStatus.FORBIDDEN);
		} else {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			if(!isEventOwner(userId, eventId)) {
				return new ResponseEntity<EventAssociation>(HttpStatus.UNAUTHORIZED);
			}
		}
		eventAssociationRepository.delete(rightsId);
		return new ResponseEntity<EventAssociation>(HttpStatus.OK);
	}
	
	/**
	 * Determines if the session user is the owner of the event
	 * @param userId the userId to compare to 
	 * @param eventId the event to search for
	 * @return if the user is the owner of the event
	 */
	private boolean isEventOwner(String userId, int eventId) {
		Event e = eventRepository.findOne(eventId);
		return e.getOwner().getId().equals(userId);
	}

}
