package edu.northeastern.cs5200.project.controllers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Category;
import edu.northeastern.cs5200.project.models.Interest;
import edu.northeastern.cs5200.project.models.ProfilePost;
import edu.northeastern.cs5200.project.models.Settings;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which manages all CRUD operations for user operations
 * @author Nicholas Carugati
 *
 */
@RestController
public class UserController {
	
	/**
	 * The mechanism which encrypts and verifies password with bcrypt
	 * @return the encoder instance
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

	/**
	 * The repository which handles all raw MySQL operations
	 */
	@Autowired
	UserRepository userRepository;

	/**
	 * Registers a new user
	 * @param user the user data
	 * @return the response for the client
	 */
	@PostMapping("/api/user")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		User u = new User(user.getType(), user.getUsername(), user.getPassword(), user.getEmail());
		if(userRepository.findByUsername(user.getUsername()) != null) {
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		u.setType("USER");
		u.setPassword(passwordEncoder().encode(user.getPassword()));
		Settings settings = new Settings();
		u.setSettings(settings);
		settings.setUser(u);
		userRepository.save(u);
		return new ResponseEntity<User>(u, HttpStatus.OK);
	}
	
	/**
	 * Creates a user with extensive user data
	 * Must have admin privileges to use this endpoint
	 * @param user the user data
	 * @return the response to the client
	 */
	@PostMapping("/api/admin/user")
	public ResponseEntity<User> createUserAdmin(@RequestBody User user) {
		if(userRepository.findByUsername(user.getUsername()) != null) {
			return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
		}
		user.setPassword(passwordEncoder().encode(user.getPassword()));
		Settings settings = user.getSettings();
		if(user.getType().equals("TALENT")) {
			settings.setVenue(null);
			settings.setCategory(null);
		} else if(user.getType().equals("VENUE")) {
			settings.setAlias(null);
			settings.setSpecialization(null);
		} else {
			user.getMembers().clear();
			settings.setAlias(null);
			settings.setSpecialization(null);
			settings.setVenue(null);
			settings.setCategory(null);
		}
		settings.setUser(user);
		user.setSettings(settings);
		userRepository.save(user);
		return new ResponseEntity<User>(HttpStatus.OK);
	}
	
	/**
	 * Determines if a user is currently logged in
	 * @return the predicate if the user has a session
	 */
	@GetMapping("/api/loggedin")
	public ResponseEntity<Boolean> hasSession() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!(auth.getPrincipal() instanceof String)) {
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		}
		return new ResponseEntity<Boolean>(false, HttpStatus.OK);
	}
	
	/**
	 * Retrieves information of the session user to the client
	 * @return the session user's data
	 */
	@GetMapping("/api/session")
	public ResponseEntity<User> getSession() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<User>(HttpStatus.OK);
		}
		AccountUserDetails aud = ((AccountUserDetails) auth.getPrincipal());
		String userId = aud.getId();
		return new ResponseEntity<User>(userRepository.findOne(userId), HttpStatus.OK);
	}
	
	/**
	 * Retrieves all users in the application
	 * @param sessionUser if the list should include the session user
	 * @return the list of all users depending on the predicate
	 */
	@GetMapping("/api/user")
	public ResponseEntity<List<User>> getAllUsers(@RequestParam(value = "session", required = true) boolean sessionUser) {
		if(!sessionUser) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			AccountUserDetails aud = ((AccountUserDetails) auth.getPrincipal());
			String userId = aud.getId();
			return new ResponseEntity<List<User>>(userRepository.findAllExcept(userId), HttpStatus.OK);
		} 
		return new ResponseEntity<List<User>>(userRepository.findAll(), HttpStatus.OK);
	}
	
	/**
	 * Retrieves the list of users that are members of a specific user
	 * @param userId the user ID to search for
	 * @return the list of users associated with the userId
	 */
	@GetMapping("/api/user/members/{uid}")
	public ResponseEntity<Set<User>> getMemebersOfUsers(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		return new ResponseEntity<Set<User>>(userRepository.findByMember(user), HttpStatus.OK);
	}
	
	/**
	 * Retrieves the list of available users to become members
	 * @return the list of available users
	 */
	@GetMapping("/api/user/members")
	public ResponseEntity<Set<User>> getMemberAviliability() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Set<User>>(HttpStatus.FORBIDDEN);
		}
		String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
		return new ResponseEntity<Set<User>>(userRepository.findAvailableUsers(userId), HttpStatus.OK);
	}

	/**
	 * Retrieves a single user
	 * @param userId the id of the user to search for
	 * @return the user data for the client
	 */
	@GetMapping("/api/user/{uid}")
	public ResponseEntity<User> getUser(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		if(user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}		
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	/**
	 * Modifies an existing user
	 * @param userInfo the user data flattened
	 * @param userId the id of the user to modify
	 * @return the response to the client
	 * @throws ParseException if the JSON deserializer fails to parse the JSON request
	 */
	@PutMapping("/api/user/{uid}")
	public ResponseEntity<User> modifyUser(@RequestBody ObjectNode userInfo, @PathVariable("uid") String userId) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<User>(HttpStatus.FORBIDDEN);
		}
		AccountUserDetails aud = ((AccountUserDetails) auth.getPrincipal());
		User foundUser = userRepository.findOne(userId);
		String username = userInfo.get("username").asText();
		foundUser.setUsername(username);
		JsonNode role = userInfo.get("type");

		String roleName = role.asText().toUpperCase();
		if(!roleName.equalsIgnoreCase("ADMIN")) {
			foundUser.setType(role.asText().toUpperCase());
		} else {
			if(aud.getType().equals("ADMIN")) {
				foundUser.setType(role.asText().toUpperCase());
			}
		}
		if(userInfo.get("email") != null || !userInfo.get("email").asText().isEmpty()) {
			foundUser.setEmail(userInfo.get("email").asText());
		}
		
		JsonNode currentPassword = userInfo.get("currentPassword");
		JsonNode password = userInfo.get("password");
		JsonNode verify = userInfo.get("verify");
		if(currentPassword != null && password != null && verify != null) {
			if(!currentPassword.asText().isEmpty() && !password.asText().isEmpty()) {
				if(password.asText().equals(verify.asText())) {
					if(passwordEncoder().matches(currentPassword.asText(), foundUser.getPassword())) {
						foundUser.setPassword(passwordEncoder().encode(password.asText()));
					} else {
						return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
					}
				} else {
					return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
				}
			}
		}
		
		
		
		//Because Hibernate can't do this with a simple clear-and-populate-all-into-hash-set 
		//Much like interests for some reason we have to do it manually
		LinkedHashMap<String, User> memberUpdate = new LinkedHashMap<String, User>();
		JsonNode membersNode = userInfo.get("members");
		if(membersNode.isArray()) {
			for(JsonNode node : membersNode) {
				User user = userRepository.findOne(node.path("id").asText());
				if(user.getMember() == null) {
					user.setMember(foundUser);
				}
				memberUpdate.put(user.getId(), user);
			}
		}
		userRepository.save(memberUpdate.values());
		List<User> removalList = new ArrayList<User>();
		for(User user : foundUser.getMembers()) {
			if(!memberUpdate.containsKey(user.getId())) {
				user.setMember(null);
				removalList.add(user);
			}
		}
		JsonNode memberNode = userInfo.get("member");
		if(memberNode.asText().equals("null")) {
			foundUser.setMember(null);
		}
		userRepository.save(removalList);

		Settings settings = foundUser.getSettings();
		JsonNode config = userInfo.get("settings");
		
		if(!config.get("firstName").asText().equals("null")) {
			settings.setFirstName(config.get("firstName").asText());
		}
		if(!config.get("lastName").asText().equals("null")) {
			settings.setLastName(config.get("lastName").asText());
		}
		if(!config.get("birthday").asText().equals("null")) {
			Calendar cal = DatatypeConverter.parseDateTime(config.get("birthday").asText());
			settings.setBirthday(cal.getTime());
		}
		if(!config.get("bio").asText().equals("null")) {
			settings.setBio(config.get("bio").asText());
		}
		if(!config.get("avatar").asText().equals("null")) {
			settings.setAvatar(config.get("avatar").asText());
		}
		if(!config.get("phone").asText().equals("null")) {
			settings.setPhone(config.get("phone").asText());
		}
		if(!config.get("website").asText().equals("null")) {
			settings.setWebsite(config.get("website").asText());
		}
		if(foundUser.getType().equals("TALENT")) {
			settings.setAlias(config.get("alias").asText());
			settings.setSpecialization(config.get("specialization").asText());
			settings.setVenue(null);
			settings.setCategory(null);
		} else if(foundUser.getType().equals("VENUE")) {
			settings.setAlias(null);
			settings.setSpecialization(null);
			settings.setVenue(config.get("venue").asText());
			settings.setCategory(config.get("category").asText());
		} else {
			foundUser.getMembers().clear();
			settings.setAlias(null);
			settings.setSpecialization(null);
			settings.setVenue(null);
			settings.setCategory(null);
		}
		foundUser.setSettings(settings);
		
		Set<Interest> interestUpdate = new HashSet<Interest>();
		JsonNode interestNode = userInfo.get("interests");
		if(interestNode.isArray()) {
			for(JsonNode node : interestNode) {
				Interest interest = new Interest(foundUser, new Category(node.get("id").asInt(), node.get("name").asText()));
				interestUpdate.add(interest);
			}
		}
		foundUser.getInterests().clear();
		foundUser.getInterests().addAll(interestUpdate);
		userRepository.save(foundUser);
		return new ResponseEntity<User>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves all statistics for a user
	 * @param userId the userId to obtain statistics for
	 * @return the JSON stats node for the user's profile data
	 */
	@GetMapping("/api/user/{uid}/stats")
	public ResponseEntity<ObjectNode> getUserStatistics(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		ObjectMapper mapper = new ObjectMapper();
		if(user == null) {
			return new ResponseEntity<ObjectNode>(HttpStatus.NOT_FOUND);
		}	
		ObjectNode statsNode = mapper.createObjectNode();
		statsNode.put("followers", user.getFollowers().size());
		statsNode.put("following", user.getFollowing().size());
		statsNode.put("events", user.getPostedEvents().size());
		statsNode.put("interestCount", user.getInterestedEvents().size());
		statsNode.put("attendance", user.getAttending().size());
		return new ResponseEntity<ObjectNode>(statsNode, HttpStatus.OK);
	}
	
	/**
	 * Retrieves the posts for a user's profile
	 * @param userId the userId to retrieve posts for
	 * @return the Users profile posts
	 */
	@GetMapping("/api/user/{uid}/posts")
	public ResponseEntity<List<ProfilePost>> getProfilePosts(@PathVariable("uid") String userId) {
		User user = userRepository.findOne(userId);
		Set<ProfilePost> posts = user.getProfilePosts();
		List<ProfilePost> postList = new ArrayList<ProfilePost>();
		postList.addAll(posts);
		postList.sort(new Comparator<ProfilePost>() {
			@Override
			public int compare(ProfilePost o1, ProfilePost o2) {
				Date d1 = o1.getPost().getTimestamp();
				Date d2 = o2.getPost().getTimestamp();
				return d2.compareTo(d1);
			}
		});
		return new ResponseEntity<List<ProfilePost>>(postList, HttpStatus.OK);
	}
	
	/**
	 * Removes an existing user
	 * @param deleteId the id of the user to delete
	 * @return the response to the client
	 */
	@DeleteMapping("/api/user/{uid}")
	public ResponseEntity<User> deleteUser(@PathVariable("uid") String deleteId) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
		}
		AccountUserDetails aud = ((AccountUserDetails) auth.getPrincipal());
		String userId = aud.getId();
		if(userId == deleteId || aud.getType().equals("ADMIN")) {
			userRepository.delete(deleteId);
			return new ResponseEntity<User>(HttpStatus.OK);
		}
		return new ResponseEntity<User>(HttpStatus.UNAUTHORIZED);
	}
}
