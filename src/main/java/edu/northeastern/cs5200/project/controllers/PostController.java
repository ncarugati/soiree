package edu.northeastern.cs5200.project.controllers;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import edu.northeastern.cs5200.project.auth.AccountUserDetails;
import edu.northeastern.cs5200.project.models.Comment;
import edu.northeastern.cs5200.project.models.Event;
import edu.northeastern.cs5200.project.models.EventPost;
import edu.northeastern.cs5200.project.models.Post;
import edu.northeastern.cs5200.project.models.ProfilePost;
import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.CommentRepository;
import edu.northeastern.cs5200.project.repositories.EventPostRepository;
import edu.northeastern.cs5200.project.repositories.EventRepository;
import edu.northeastern.cs5200.project.repositories.PostRepository;
import edu.northeastern.cs5200.project.repositories.ProfilePostRepository;
import edu.northeastern.cs5200.project.repositories.RatingRepository;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * The REST controller which retrieves pertinent post data
 * @author Nicholas Carugati
 *
 */
@RestController
public class PostController {
	
	/**
	 * The repository which handles all raw MySQL operations for posts
	 */
	@Autowired
	PostRepository postRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for users
	 */
	@Autowired
	UserRepository userRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for events
	 */
	@Autowired
	EventRepository eventRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for comments
	 */
	@Autowired
	CommentRepository commentRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for event posts
	 */
	@Autowired
	EventPostRepository eventPostRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for profile posts
	 */
	@Autowired
	ProfilePostRepository profilePostRepository;
	
	/**
	 * The repository which handles all raw MySQL operations for post ratings
	 */
	@Autowired
	RatingRepository ratingRepository;

	/**
	 * Posts content to a designated profile
	 * @param uId the userId of the profile to post to
	 * @param post the post data
	 * @return the response to the session user
	 */
	@PostMapping("/api/post/profile/{uid}")
	public ResponseEntity<Post> postContentToProfile(@PathVariable("uid") String uId, @RequestBody Post post) {
		Post p = post;
		ProfilePost pp = new ProfilePost();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Post>(HttpStatus.FORBIDDEN);
		}
		if(post.getOwner() == null) {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			User user = userRepository.findOne(userId);
			p.setOwner(user);
		}
		postRepository.save(p);
		
		User recipientUser = userRepository.findOne(uId);
		pp.setUser(recipientUser);
		pp.setPost(p);
		profilePostRepository.save(pp);
		
		return new ResponseEntity<Post>(p, HttpStatus.OK);
	}
	
	/**
	 * Posts content to an event
	 * @param eventId the id of the event
	 * @param post the post data
	 * @return the response for the session user
	 */
	@PostMapping("/api/post/event/{eid}")
	public ResponseEntity<Post> postContentToEvent(@PathVariable("eid") int eventId, @RequestBody Post post) {
		Post p = post;
		EventPost ep = new EventPost();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth.getPrincipal() instanceof String) {
			return new ResponseEntity<Post>(HttpStatus.FORBIDDEN);
		}
		if(post.getOwner() == null) {
			String userId = ((AccountUserDetails) auth.getPrincipal()).getId();
			User user = userRepository.findOne(userId);
			p.setOwner(user);	
		}
		postRepository.save(p);
		
		Event evt = eventRepository.findOne(eventId);
		ep.setEvent(evt);
		ep.setPost(p);
		eventPostRepository.save(ep);
		return new ResponseEntity<Post>(p, HttpStatus.OK);
	}
	
	/**
	 * Locks a post so other users cannot comment
	 * @param postId the id of the post to lock
	 * @return the response for the client
	 */
	@PutMapping("/api/post/{pid}/lock")
	public ResponseEntity<Post> togglePostLock(@PathVariable("pid") int postId) {
		Post p = postRepository.findOne(postId);
		p.setLocked(!p.isLocked());
		postRepository.save(p);
		return new ResponseEntity<Post>(p, HttpStatus.OK);
	}
	
	/**
	 * Retrieves a single post by its id
	 * @param postId the id of the post to search for
	 * @return the post data if available
	 */
	@GetMapping("/api/post/{pid}")
	public ResponseEntity<Post> findPostById(@PathVariable("pid") int postId) {
		Post p = postRepository.findOne(postId);
		return new ResponseEntity<Post>(p, HttpStatus.OK);
	}
	
	/**
	 * Updates an existing post
	 * @param postId the id of the post
	 * @param postInfo the post data
	 * @return the response for the client
	 */
	@PutMapping("/api/post/{pid}")
	public ResponseEntity<Post> updatePost(@PathVariable("pid") int postId, @RequestBody Post postInfo) {
		Post p = postRepository.findOne(postId);
		p.setContent(postInfo.getContent());
		p.setLocked(postInfo.isLocked());
		if(postInfo.getAttachment() == null) {
			p.setAttachment(null);
		}
		postRepository.save(p);
		return new ResponseEntity<Post>(p, HttpStatus.OK);
	}
	
	/**
	 * Removes a post
	 * @param postId the id of the post to delete
	 * @return the response for the client
	 */
	@DeleteMapping("/api/post/{pid}")
	public ResponseEntity<Post> deletePost(@PathVariable("pid") int postId) {
		postRepository.delete(postId);
		return new ResponseEntity<Post>(HttpStatus.OK);
	}
	
	/**
	 * Retrieves the comments for a post
	 * @param postId the id of the post to get comments for
	 * @return the list of comments for a specific post
	 */
	@GetMapping("/api/post/{pid}/comments")
	public ResponseEntity<Set<Comment>> getPostComments(@PathVariable("pid") int postId) {
		Post p = postRepository.findOne(postId);
		Set<Comment> comments = commentRepository.findByPostOrderByTimestampDesc(p);
		return new ResponseEntity<Set<Comment>>(comments, HttpStatus.OK);
	}
	
	/**
	 * Retrieves every single post submitted to the application
	 * @return the list of all posts including the ones a session user owns.
	 */
	@GetMapping("/api/post")
	public ResponseEntity<List<Post>> getAllPosts() {
		return new ResponseEntity<List<Post>>(postRepository.findAll(), HttpStatus.OK);
	}

}
