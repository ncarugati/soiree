package edu.northeastern.cs5200.project.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import edu.northeastern.cs5200.project.models.User;
import edu.northeastern.cs5200.project.repositories.UserRepository;

/**
 * Represents the User Detail Service which process important user information
 * @see https://spring.io/guides/gs/securing-web/
 * @author Nicholas Carugati
 *
 */
@Component
public class UserDetailServiceImpl implements UserDetailsService {

	/**
	 * The user repository which manages all user operations
	 */
	@Autowired
	private UserRepository userRepository;

	/**
	 * Loads the user session by its username
	 * @param name the name of the user
	 * @return The meta-user details for the spring boot security instance
	 * @throws UsernameNotFoundException if the username cannot be found
	 */
	@Override
	public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(name);
		if(user == null) {
			throw new UsernameNotFoundException("Username specified not found");
		}
		return new AccountUserDetails(user);
	}

}
