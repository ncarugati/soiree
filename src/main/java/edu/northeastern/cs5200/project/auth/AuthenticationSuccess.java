package edu.northeastern.cs5200.project.auth;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

/**
 * Represents an authentication success event
 * @see http://www.baeldung.com/securing-a-restful-web-service-with-spring-security
 * @author Nicholas Carugati
 *
 */
@Component
public class AuthenticationSuccess extends SimpleUrlAuthenticationSuccessHandler {

	/**
	 * Invokes on an event where a user is able to authenticate
	 * @param request the request which was sent
	 * @param response the response which will return
	 * @param authentication the authentication details that will be dispersed to the session user
	 * @throws IOException if the server fails to process a request or response
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, 
			Authentication authentication) throws IOException {
		String userId = ((AccountUserDetails) authentication.getPrincipal()).getId();
		response.setStatus(HttpServletResponse.SC_OK);
		
		//This persists the userId so a 404 doesn't occur. 
		response.getOutputStream().print(userId);
		response.flushBuffer();
	}

}
