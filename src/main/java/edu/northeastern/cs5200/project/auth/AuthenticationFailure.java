package edu.northeastern.cs5200.project.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * Represents an authentication failure event
 * @see http://www.baeldung.com/securing-a-restful-web-service-with-spring-security
 * @author Nicholas Carugati
 *
 */
@Component
public class AuthenticationFailure extends SimpleUrlAuthenticationFailureHandler {

	/**
	 * Invokes on an event where a user fails to authenticate
	 * @param request the request which was sent
	 * @param response the response which will return
	 * @param exception the exception which will be thrown on failure
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) {
		response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
	}

}
