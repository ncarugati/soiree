package edu.northeastern.cs5200.project.auth;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * An event which is called if the user goes to a route that is unauthorized
 * @author Nicholas Carugati
 *
 */
@Component
public class EntryPointUnauthorizedHandler implements AuthenticationEntryPoint {

	/**
	 * Invoked when the user navigates to an unauthorized route
	 * @param request the request that is being made
	 * @param response the response that will be sent
	 * @param exception the exception that will be thrown on event
	 * @throws IOException if the server fails to process the request or response
	 */
	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authException) throws IOException {
		response.sendRedirect("/login");
	}

}
