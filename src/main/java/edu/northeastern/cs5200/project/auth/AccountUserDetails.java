package edu.northeastern.cs5200.project.auth;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import edu.northeastern.cs5200.project.models.User;

/**
 * Represents a User associated with the Spring Security session
 * @see https://spring.io/guides/gs/securing-web/
 * @author Nicholas Carugati
 *
 */
public class AccountUserDetails implements UserDetails {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8436977607468302427L;
	
	/**
	 * The user attached to the instance
	 */
	private final User user;
	
	/**
	 * Instantiates a new User model that Spring Security can recognize
	 * @param user
	 */
	public AccountUserDetails(User user) {
		this.user = user;
	}
	
	/**
	 * Populates granted permissions to a user. 
	 * In this case only one permission should be granted
	 * @return the list of authorized permissions
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		ArrayList<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();
		auths.add(new SimpleGrantedAuthority("ROLE_" + user.getType()));
		return auths;
	}
	
	/**
	 * Retrieves the ID of the user details
	 * @return the id from the User entity
	 */
	public String getId() {
		return user.getId();
	}
	
	/**
	 * Retrieves the type or the role of the user
	 * @return the role of the user
	 */
	@JsonIgnore
	public String getType() {
		return user.getType();
	}
	
	/**
	 * Retrieves the password of the session user
	 * @return The password
	 */
	@JsonIgnore
	@Override
	public String getPassword() {
		return user.getPassword();
	}

	/**
	 * Retrieves the username attached to the session
	 * @return the username involved with the session
	 */
	@JsonIgnore
	@Override
	public String getUsername() {
		return user.getUsername();
	}

	/**
	 * Determines if the session is not expired
	 * This feature is not implemented but is required for declaration
	 */
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	
	/**
	 * Determines if the account is locked
	 * This feature is not implemented but is required for declaration
	 */
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	/**
	 * Determines if the user's credentials are not expired
	 * This feature is not implemented but is required for declaration
	 */
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	/**
	 * Determines if the user's session is enabled
	 * This feature is not implemented but is required for declaration
	 */
	@Override
	public boolean isEnabled() {
		return true;
	}


}
