package edu.northeastern.cs5200.project.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a single interest for a user
 * @author Nicholas Carugati
 *
 */
@Entity(name = "interest")
public class Interest {

	/**
	 * Retrieves the unique id of the interest
	 * @return the id of the interest
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the interest
	 * @param id the new id of the user
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the owner of the interest
	 * @return the owner of the interest
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Assigns a new user to the interest
	 * @param user the new user which will own the instance
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retrieves the category of the interest
	 * @return the category of the interest
	 */
	public Category getCategory() {
		return category;
	}

	/**
	 * Assigns a new category to the particular instance
	 * @param category the new category instance
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	
	/**
	 * Creates a new interest instance
	 * @param id the unique id of the instance
	 * @param user the user who owns the instance
	 * @param interestName the name of the interest
	 */
	public Interest(User user, Category category) {
		this.user = user;
		this.category = category;
	}
	
	/**
	 * Dummy constructor
	 */
	public Interest() {
		
	}

	/**
	 * The unique id of the interest
	 */
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	/**
	 * The user instance which owns the interest
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	
	/**
	 * The category of the interest
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="interest_id")
	private Category category;

}
