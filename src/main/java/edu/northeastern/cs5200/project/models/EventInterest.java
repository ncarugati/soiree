package edu.northeastern.cs5200.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a user's interest in the scope of an event
 * @author Nicholas Carugati
 *
 */
@Entity(name = "event_interest")
public class EventInterest {

	/**
	 * Retrieves the ID of the relationship
	 * @return the new id of the relationship
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new ID to the relationship
	 * @param id the new id to assign
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the user that is interested in the event
	 * @return the user that is interested in the event
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Assigns a new user to be interested in the event
	 * @param user the new user to assign
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retrieves the event that is being interested in
	 * @return the event of interest
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Assigns a new event of interest
	 * @param event the new event which will be assigned
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Dummy constructor
	 */
	public EventInterest() {

	}
	/**
	 * The ID of the event interest
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The user who expressed interest in the event
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	
	/**
	 * The event in which the user is interested in
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="event_id")
	private Event event;

}
