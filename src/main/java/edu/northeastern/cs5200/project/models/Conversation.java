package edu.northeastern.cs5200.project.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a conversation in an inbox system
 * @author Nicholas Carugati
 *
 */
@Entity(name = "conversations")
public class Conversation {
	
	/**
	 * Retrieves the unique ID of a conversation
	 * @return the unique id of a conversation
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to a conversation
	 * @param id the id of the conversation
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the primary participant of the conversation
	 * @return the primary participant of the conversation
	 */
	public User getParticipantOne() {
		return participantOne;
	}

	/**
	 * Assigns a new primary participant of the conversation
	 * @param participantOne the new primary participant of the conversation
	 */
	public void setParticipantOne(User participantOne) {
		this.participantOne = participantOne;
	}

	/**
	 * Retrieves the secondary participant of a conversation
	 * @return the secondary participant of a conversation
	 */
	public User getParticipantTwo() {
		return participantTwo;
	}

	/**
	 * Assigns a new secondary participant of a conversation
	 * @param participantTwo the new secondary participant of the conversation
	 */
	public void setParticipantTwo(User participantTwo) {
		this.participantTwo = participantTwo;
	}

	/**
	 * Retrieves the messages associated with the conversation
	 * @return the messages attached to the conversation
	 */
	public List<Message> getMessages() {
		return messages;
	}

	/**
	 * Assigns a new list of messages for a conversation
	 * @param messages the new list of messages for a conversation
	 */
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	/**
	 * Retrieves the archive status of the first participant
	 * @return if the first participant archived the chat
	 */
	public boolean isPartOneArchived() {
		return partOneArchived;
	}

	/**
	 * Assigns the archive status of the first participant
	 * @param partOneArchived the new predicate for the first participants archive state
	 */
	public void setPartOneArchived(boolean partOneArchived) {
		this.partOneArchived = partOneArchived;
	}

	/**
	 * Retrieves the archive status of the second participant
	 * @return if the second participant archived the chat
	 */
	public boolean isPartTwoArchived() {
		return partTwoArchived;
	}

	/**
	 * Assigns the archive status of the second participant
	 * @param partTwoArchived the new predicate for the second participants archive state
	 */
	public void setPartTwoArchived(boolean partTwoArchived) {
		this.partTwoArchived = partTwoArchived;
	}

	/**
	 * Dummy constructor
	 */
	public Conversation() {
		
	}

	/**
	 * The unique ID of a conversation
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The primary participant of the conversation
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="participant_1")
	private User participantOne;
	
	/**
	 * The secondary participant of a conversation
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="participant_2")
	private User participantTwo;
	
	/**
	 * Predicate for if the first participant archived the chat
	 */
	@Column(name = "part_1_archived")
	private boolean partOneArchived;
	
	/**
	 * Predicate for if the second participant archived the chat
	 */
	@Column(name = "part_2_archived")
	private boolean partTwoArchived;
	
	/**
	 * The collection of messages attached to the conversation
	 */
	@JsonIgnore
	@OrderBy("timestamp ASC")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "conversation")
	private List<Message> messages = new ArrayList<>();

}
