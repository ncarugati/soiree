package edu.northeastern.cs5200.project.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.search.annotations.Field;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The instance which represents the setting configurations for a user
 * @author Nicholas Carugati
 *
 */
@Entity(name = "settings")
public class Settings {
	
	/**
	 * Retrieves the ID of the user's settings configuration
	 * @return the id of the user's settings configuration
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the user's settings config
	 * @param id the new unique id to the user's settings
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the users who owns the user configuration
	 * @return the owner of the settings config
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets a new owner to the settings config
	 * @param user the new owner of this settings instance
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retrieves the first name of the user
	 * @return the first name of the user
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name of the user
	 * @param firstName the new first name of the user
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Retrieves the last name of the user
	 * @return the last name of the user
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Assigns a new last name to a user
	 * @param lastName the new last name of a user
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the date of birth of the user
	 * @return the date of birth of the user
	 */
	public Date getBirthday() {
		return birthday;
	}

	/**
	 * Sets the birthday of the user
	 * @param birthday the new date of birth
	 */
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	/**
	 * Retrieves the path of the user's avatar image
	 * @return the path for the avatar image
	 */
	public String getAvatar() {
		return avatar;
	}

	/**
	 * Sets the avatar path for a user
	 * @param avatar the new path for the avatar image
	 */
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/**
	 * Retrieves the bio of the user
	 * @return the bio of the user
	 */
	public String getBio() {
		return bio;
	}

	/**
	 * Sets the bio of the user
	 * @param bio the new bio of the user and its contents
	 */
	public void setBio(String bio) {
		this.bio = bio;
	}

	/**
	 * Retrieves the phone number of this user
	 * @return the phone number of this user
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Assigns a new phone number for this user
	 * @param phone the new phone number to assign to the user
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Retrieves the website of the user
	 * @return the current website the user owns
	 */
	public String getWebsite() {
		return website;
	}

	/**
	 * Assigns a new website for a user
	 * @param website the new website to assign to the user
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * Gets the alias name of this user account (band/group type)
	 * @return the alias name of this user account
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Assigns an alias to the user of this account (band/group type)
	 * @param alias the new alias name of the user
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Gets the specialization of the user (band/group type)
	 * @return the specialization of the user
	 */
	public String getSpecialization() {
		return specialization;
	}

	/**
	 * Assigns a new specialization to this user (band/group type)
	 * @param specialization the new specialization of the user
	 */
	public void setSpecialization(String specialization) {
		this.specialization = specialization;
	}

	/**
	 * Gets the venue name of the account (venue type)
	 * @return the venue name of the user (venue type)
	 */
	public String getVenue() {
		return venue;
	}

	/**
	 * Sets the venue name of the account (venue type)
	 * @param venue the venue of the account
	 */
	public void setVenue(String venue) {
		this.venue = venue;
	}

	/**
	 * Retrieves the category of the account (venue type)
	 * @return the category of the account
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * Assigns a new category of the account (venue type)
	 * @param category the new category of the account
	 */
	public void setCategory(String category) {
		this.category = category;
	}
	
	/**
	 * Dummy constructor
	 */
	public Settings() { }
	
	/**
	 * The unique id of the settings config
	 */
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The user that owns the settings config
	 */
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id", nullable = false)
	private User user;
	
	/**
	 * First name of the user
	 */
	@Field
	@Column(name="first_name")
	private String firstName;
	
	/**
	 * Last name of the user
	 */
	@Field
	@Column(name="last_name")
	private String lastName;
	
	/**
	 * The date of birth of the user
	 */
	@Column(name="birthday")
	private Date birthday;
	
	/**
	 * The URL of the avatar
	 */
	@Column(name="avatar")
	private String avatar;
	
	/**
	 * The bio of the user
	 */
	@Column(name="bio")
	private String bio;
	
	/**
	 * The phone number of the user
	 */
	@Column(name="phone")
	private String phone;
	
	/**
	 * The website URL of the user
	 */
	@Column(name="website")
	private String website;
	
	/**
	 * The alias of the user (If applicable)
	 */
	@Field
	@Column(name="alias")
	private String alias;
	
	/**
	 * The specialization of the user (If applicable)
	 */
	@Field
	@Column(name="specialization")
	private String specialization;
	
	/**
	 * The venue of the user (If applicable)
	 */
	@Field
	@Column(name="venue")
	private String venue;
	
	/**
	 * The category of the venue (If applicable)
	 */
	@Field
	@Column(name="category")
	private String category;

}
