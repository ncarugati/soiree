package edu.northeastern.cs5200.project.models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/**
 * Represents a single user in the application
 * @author Nicholas Carugati
 *
 */
@Indexed
@Entity(name = "users")
public class User {

	/**
	 * Retrieves the ID of a particular person
	 * @return the ID of a particular person
	 */
	public String getId() {
		return id;
	}

	/**
	 * Assigns a new ID to a person
	 * @param id the new id of the person to assign to
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * Retrieves the type of user the person is
	 * @return the type of user
	 */
	public String getType() {
		return type;
	}

	/**
	 * Assigns a type of person for a particular user
	 * @param type the new type to assign to
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Retrieves the username of the person
	 * @return the username of the person
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets a new username to a particular user instance
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Retrieves the password of a person (in hash form)
	 * @return the password of the user
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Assigns a new password to a person
	 * @param password the new password to assign
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Retrieves the email address of a particular user
	 * @return the email address of the user
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets a new email address for a particular user
	 * @param email the new email address to assign
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Retrieves the settings configuration for this user
	 * @return the settings configuration
	 */
	public Settings getSettings() {
		return settings;
	}

	/**
	 * Sets the settings configuration for this user
	 * @param settings the new settings configuration
	 */
	public void setSettings(Settings settings) {
		this.settings = settings;
	}

	/**
	 * Retrieves the user's interests
	 * @return the user's interests
	 */
	public Set<Interest> getInterests() {
		return interests;
	}

	/**
	 * Assigns new interests for the user
	 * @param interests the new tuple of interests
	 */
	public void setInterests(Set<Interest> interests) {
		this.interests = interests;
	}

	/**
	 * Retrieves the set of followers a user has
	 * @return the set of followers a user has
	 */
	public Set<Follower> getFollowers() {
		return followers;
	}

	/**
	 * Assigns a new list of followers for the user
	 * @param followers the new list of followers
	 */
	public void setFollowers(Set<Follower> followers) {
		this.followers = followers;
	}

	/**
	 * Retrieves the list the user is following
	 * @return the list of followees
	 */
	public Set<Follower> getFollowing() {
		return following;
	}

	/**
	 * Sets a new list of followees
	 * @param following the new list of followees
	 */
	public void setFollowing(Set<Follower> following) {
		this.following = following;
	}

	/**
	 * Gets the list of user posted events
	 * @return the list of user posted events
	 */
	public Set<Event> getPostedEvents() {
		return postedEvents;
	}

	/**
	 * Sets the list of user posted events
	 * @param postedEvents the new list of user posted events
	 */
	public void setPostedEvents(Set<Event> postedEvents) {
		this.postedEvents = postedEvents;
	}

	/**
	 * Retrieves the set of all the user's posts
	 * @return the set of all user posts
	 */
	public Set<Post> getPosts() {
		return posts;
	}

	/**
	 * Assigns a new list of posts by the user
	 * @param posts the new list of posts by the user
	 */
	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	/**
	 * Gets the list of events the user is attending
	 * @return the list of attending events
	 */
	public Set<Attendee> getAttending() {
		return attending;
	}

	/**
	 * Sets a new list of events the user is attending
	 * @param attending the new list of attending users
	 */
	public void setAttending(Set<Attendee> attending) {
		this.attending = attending;
	}

	/**
	 * Retrieves the set of all comments by a user
	 * @return the set of comments by a user
	 */
	public Set<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the list of comments submitted by a user
	 * @param comments the list of submitted comments
	 */
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Retrieves all the posts that a user rated
	 * @return the rating of all posts for a user
	 */
	public Set<Rating> getRatings() {
		return ratings;
	}

	/**
	 * Assigns new ratings that a user posted
	 * @param ratings the new collection of ratings
	 */
	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	/**
	 * Retrieves all the events a user is interested in
	 * @return the list of events a user is interested in
	 */
	public Set<EventInterest> getInterestedEvents() {
		return interestedEvents;
	}

	/**
	 * Assigns a new collection of events a user is interested in
	 * @param interestedEvents the new list of events a user is interested in
	 */
	public void setInterestedEvents(Set<EventInterest> interestedEvents) {
		this.interestedEvents = interestedEvents;
	}

	/**
	 * Retrieves the sender instance
	 * This instance is typically the user itself
	 * @return the message instance
	 */
	public List<Message> getSender() {
		return sender;
	}

	/**
	 * Assigns a new sender to a message
	 * @param sender the sender instance to assign
	 */
	public void setSender(List<Message> sender) {
		this.sender = sender;
	}

	/**
	 * Retrieves all the recipients that a user sent
	 * @return the list of users messages were sent to
	 */
	public List<Message> getRecipients() {
		return recipients;
	}

	/**
	 * Assigns a new list of recipients
	 * @param recipients the new collection of recipients
	 */
	public void setRecipients(List<Message> recipients) {
		this.recipients = recipients;
	}

	/**
	 * Retrieves all profile posts for a user
	 * @return the profile posts for a user
	 */
	public Set<ProfilePost> getProfilePosts() {
		return profilePosts;
	}

	/**
	 * Assigns all profile posts for a user
	 * @param profilePosts the new collection of profile posts to display
	 */
	public void setProfilePosts(Set<ProfilePost> profilePosts) {
		this.profilePosts = profilePosts;
	}

	/**
	 * Retrieves the conversations where a user is a primary participant
	 * @return the list of primary participant conversations
	 */
	public Set<Conversation> getPrimaryParticipants() {
		return primaryParticipants;
	}

	/**
	 * Assigns a new list of conversations where the user is a primary participant
	 * @param primaryParticipants the new list of conversations
	 */
	public void setPrimaryParticipants(Set<Conversation> primaryParticipants) {
		this.primaryParticipants = primaryParticipants;
	}

	/**
	 * Retrieves the conversations where a user is a secondary participant
	 * @return the list of secondary participant conversations
	 */
	public Set<Conversation> getSecondaryParticipants() {
		return secondaryParticipants;
	}

	/**
	 * Assigns a new list of conversations where the user is a secondary participant
	 * @param primaryParticipants the new list of conversations
	 */
	public void setSecondaryParticipants(Set<Conversation> secondaryParticipants) {
		this.secondaryParticipants = secondaryParticipants;
	}

	/**
	 * Retrieves all the events which the user has rights
	 * @return the list of events where the user has rights
	 */
	public Set<EventAssociation> getEventRights() {
		return eventRights;
	}

	/**
	 * Assigns a new list of events where the user has rights
	 * @param eventRights the new list of events to override
	 */
	public void setEventRights(Set<EventAssociation> eventRights) {
		this.eventRights = eventRights;
	}
	
	/**
	 * Retrieves the account the user is a member of
	 * @return the account the user is a member of
	 */
	public User getMember() {
		return member;
	}

	/**
	 * Assigns a new user to become a member of
	 * @param member the new member account
	 */
	public void setMember(User member) {
		this.member = member;
	}

	/**
	 * Retrieves all the users that are members of this account
	 * @return the list of users that are members
	 */
	public Set<User> getMembers() {
		return members;
	}

	/**
	 * Assigns a new list of users to become members of this account
	 * @param members the list of unique, new members to assign
	 */
	public void setMembers(Set<User> members) {
		this.members = members;
	}

	/**
	 * Instantiates a new user instance
	 * @param type the type of the user
	 * @param username the user's username
	 * @param password the user's password
	 * @param email the email address of the user
	 */
	public User(String type, String username, String password, String email) {
		this.type = type;
		this.username = username;
		this.password = password;
		this.email = email;
	}


	/**
	 * Dummy constructor
	 */
	public User() { 

	}

	/**
	 * The unique id of the user
	 */
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Column(name="id")
	private String id;

	/**
	 * The type of the user
	 */
	@Column(name="type")
	private String type;

	/**
	 * The user's username
	 */
	@Field
	@Column(name="username")
	private String username;

	/**
	 * The user's password
	 */
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name="password")
	private String password;

	/**
	 * The user's email
	 */
	@Field
	@Column(name="email")
	private String email;
	
	/**
	 * The user instance which represents what the user is a member of
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="member")
	private User member;	

	/**
	 * The user's settings configuration
	 */
	@IndexedEmbedded
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL, mappedBy = "user")
	private Settings settings;

	/**
	 * The user's interests
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
	private Set<Interest> interests = new HashSet<>();
	
	/**
	 * Who are the user's followers
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "follower")
	private Set<Follower> followers = new HashSet<>();
	
	/**
	 * Who is the user following
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "followee")
	private Set<Follower> following = new HashSet<>();
	
	/**
	 * The list of posted events 
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "owner")
	private Set<Event> postedEvents = new HashSet<>();
	
	/**
	 * The list of posts this person has posted
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "owner")
	private Set<Post> posts = new HashSet<>(); 
	
	/**
	 * The set of events the user is attending
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "attendee")
	private Set<Attendee> attending = new HashSet<>();
	
	/**
	 * The comments the user has created
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "owner")
	private Set<Comment> comments = new HashSet<>();
	
	/**
	 * The ratings given by a user
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
	private Set<Rating> ratings = new HashSet<>();
	
	/**
	 * The list of events a user is interested in
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
	private Set<EventInterest> interestedEvents = new HashSet<>();
	
	/**
	 * The list of posts within an event
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade =  CascadeType.ALL, orphanRemoval = true, mappedBy = "sender")
	private List<Message> sender = new ArrayList<>();
	
	/**
	 * The list of recipients for inbox system
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "recipient")
	private List<Message> recipients = new ArrayList<>();
	
	/**
	 * The list of profile posts on the user's profile page
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
	private Set<ProfilePost> profilePosts = new HashSet<>();
	
	/**
	 * List conversations where users are a primary participant
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "participantOne")
	private Set<Conversation> primaryParticipants = new HashSet<>();
	
	/**
	 * Lists covnersations where users are a secondary participant
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "participantTwo")
	private Set<Conversation> secondaryParticipants = new HashSet<>();
	
	/**
	 * The list of events and the associated rights that the user has
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "user")
	private Set<EventAssociation> eventRights = new HashSet<>();
	
	/**
	 * Retrieves the list of other users that are members of this account
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.DETACH, mappedBy = "member")
	private Set<User> members = new HashSet<>();

}
