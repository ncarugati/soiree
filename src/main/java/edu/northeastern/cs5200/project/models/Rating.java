package edu.northeastern.cs5200.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a single rating for a post
 * @author Nicholas Carugati
 *
 */
@Entity(name = "rating")
public class Rating {

	/**
	 * Retrieves the unique id of a rating
	 * @return the id of the rating
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the rating instance
	 * @param id the new id to assign
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the user that made the rating
	 * @return the user instance of the rating
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets a new rater
	 * @param user the new user who will rate the post
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retrieves the post that will be rated
	 * @return the post instance
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * Assigns a new post to be rated
	 * @param post the new post instance
	 */
	public void setPost(Post post) {
		this.post = post;
	}

	/**
	 * Retrieves the rating value of this rating
	 * @return the rating value
	 */
	public int getRating() {
		return rating;
	}

	/**
	 * Assigns a new rating value to this rating instance
	 * @param rating the new rating value
	 */
	public void setRating(int rating) {
		this.rating = rating;
	}

	/**
	 * Dummy constructor
	 */
	public Rating() {
		
	}
	
	/**
	 * The unique id of the rating
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The user that invoked the rating
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	
	/**
	 * The post where the rating will be placed
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="post_id")
	private Post post;
	
	/**
	 * The value of the rating
	 */
	@Column(name = "rating")
	private int rating;

}
