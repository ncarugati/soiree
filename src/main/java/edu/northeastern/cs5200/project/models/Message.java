package edu.northeastern.cs5200.project.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * Represents a single message
 * @author Nicholas Carugati
 *
 */
@Entity(name = "messages")
public class Message {

	/**
	 * Retrieves the unique id of the message
	 * @return the id of the message
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the message
	 * @param id the new id to assign
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the conversation attached to this message
	 * @return the conversation associated with the message
	 */
	public Conversation getConversation() {
		return conversation;
	}

	/**
	 * Assigns a new conversation to attach to the message
	 * @param conversation the new conversation the message should be associated with
	 */
	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

	/**
	 * Gets the sender of the message
	 * @return the sender of the message
	 */
	public User getSender() {
		return sender;
	}

	/**
	 * Assigns a new sender to the message
	 * @param sender the new sender of the message
	 */
	public void setSender(User sender) {
		this.sender = sender;
	}

	/**
	 * Gets the recipient of the message
	 * @return the recipient of the message
	 */
	public User getRecipient() {
		return recipient;
	}

	/**
	 * Assigns a new recipient of the message
	 * @param recipient the new recipient for the message
	 */
	public void setRecipient(User recipient) {
		this.recipient = recipient;
	}

	/**
	 * Retrieves the contents of the message
	 * @return the contents of the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the contents of the message
	 * @param message the new message contents
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Retrieves the date when the message was sent
	 * @return the date when the message was sent
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets a new date for when the message was sent
	 * @param timestamp the new timestamp for the message
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Determines if the recipient read the message
	 * @return if the message was read
	 */
	public boolean isRead() {
		return read;
	}

	/**
	 * Sets the read status for a recipient
	 * @param read the new read predicate
	 */
	public void setRead(boolean read) {
		this.read = read;
	}

	/**
	 * Determines if the user archived the message
	 * @return the sender archive status
	 */
	public boolean isSenderArchived() {
		return senderArchived;
	}

	/**
	 * Sets the archive status on the sender's end
	 * @param senderArchived the new archive status for the sender
	 */
	public void setSenderArchived(boolean senderArchived) {
		this.senderArchived = senderArchived;
	}

	/**
	 * Determines if the recipient archived the message
	 * @return if the recipient archived the message
	 */
	public boolean isRecipientArchived() {
		return recipientArchived;
	}

	/**
	 * Sets the archive status on the recipient's end
	 * @param recipientArchived the new archive status for the sender
	 */
	public void setRecipientArchived(boolean recipientArchived) {
		this.recipientArchived = recipientArchived;
	}

	/**
	 * Dummy constructor
	 */
	public Message() {
		
	}
	
	/**
	 * The unique id of the message
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The conversation that is attached to this message
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="conversation_id")
	private Conversation conversation;
	
	/**
	 * The sender of the message
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="sender_id")
	private User sender;
	
	/**
	 * The recipient of the message
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="recipient_id")
	private User recipient;
	
	/**
	 * The contents of the message
	 */
	@Column(name = "message")
	private String message;
	
	/**
	 * The timestamp of when the message was sent
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	@CreationTimestamp
	@Column(name = "timestamp")
	private Date timestamp;
	
	/**
	 * The read status of a message
	 */
	@Column(name = "is_read")
	private boolean read;
	
	/**
	 * Determines if the sending user archived the message
	 */
	@Column(name = "sender_archived")
	private boolean senderArchived;
	
	/**
	 * Determines if the sending user archived the message
	 */
	@Column(name = "recipient_archived")
	private boolean recipientArchived;

}
