package edu.northeastern.cs5200.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a user role within an event
 * @author Nicholas Carugati
 *
 */
@Entity(name="event_associations")
public class EventAssociation {

	/**
	 * Retrieves the id of the event association
	 * @return the id of the event association
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new unique ID to the event association
	 * @param id the new ID of the event association
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the event that the user has rights to
	 * @return the event that the user is associated with
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Assigns a new event for a user with an existing role
	 * @param event the new event to assign
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Retrieves the user who has a role within an event
	 * @return the user who has a role in the event
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Assigns a new user for the event association
	 * @param user the new user for an associated event
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retrieves the role associated with the user
	 * @return the role that is associated with the user
	 */
	public EventRole getRole() {
		return role;
	}

	/**
	 * Assigns a new role to the user
	 * @param role the new role that will be assigned to the user
	 */
	public void setRole(EventRole role) {
		this.role = role;
	}

	/**
	 * Dummy constructor
	 */
	public EventAssociation() {
		
	}
	
	/**
	 * The ID of the association
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The user that has rights to the event
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="user_id")
	private User user;
	
	/**
	 * The event that the user has rights to
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="event_id")
	private Event event;
	
	/**
	 * The role that the user possesses
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="role_id")
	private EventRole role;

}
