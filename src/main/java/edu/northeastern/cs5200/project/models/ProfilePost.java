package edu.northeastern.cs5200.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a post on a profile page
 * @author Nicholas Carugati
 *
 */
@Entity(name = "profile_posts")
public class ProfilePost {

	/**
	 * Retrieves the unique id of the instance
	 * @return the id of the instance
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the instance
	 * @param id the new ID to assign
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the user information 
	 * @return the user instance where the post will be placed
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Assigns a new user where the profile will be placed
	 * @param user the new user to assign to assign the profile post to
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Retrieves the post that will be placed on the user's profile
	 * @return the post that will be placed on profile
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * Assigns a new post to be placed onto the profile
	 * @param post the new post that will be put on profile
	 */
	public void setPost(Post post) {
		this.post = post;
	}

	/**
	 * Dummy constructor
	 */
	public ProfilePost() {
		
	}
	
	/**
	 * The ID of the instance
	 */
	@Id
	@JsonIgnore
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The user's information where the post will be place
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;
	
	/**
	 * The post that will be placed on the profile
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="post_id")
	private Post post;

}
