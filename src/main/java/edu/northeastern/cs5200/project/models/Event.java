package edu.northeastern.cs5200.project.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
/**
 * Represents a single event
 * @author Nicholas Carugati
 *
 */
@Indexed
@Entity(name = "events")
public class Event {
	
	/**
	 * Retrieves the unique id of a post
	 * @return the id of a single post
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to a single post
	 * @param id the new id of the post
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the owner of the post
	 * @return the owner of the post
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * Assigns a new owner to a post
	 * @param owner the new owner to assign to
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}

	/**
	 * Retrieves when the date was posted
	 * @return the date of the post
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Assigns a new creation date of the post
	 * @param timestamp the new date instance to assign
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Retrieves the name of an event
	 * @return the name of an event
	 */
	public String getName() {
		return name;
	}

	/**
	 * Assigns a new event name
	 * @param name the new event name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the start time of the event
	 * @return the starting time of the event
	 */
	public Date getTimeStart() {
		return timeStart;
	}

	/**
	 * Sets the start time of the event
	 * @param time the event's starting time
	 */
	public void setTimeStart(Date timeStart) {
		this.timeStart = timeStart;
	}
	
	
	/**
	 * Gets the end time of the event
	 * @return the end time of the event
	 */
	public Date getTimeEnd() {
		return timeEnd;
	}

	/**
	 * Sets the end time of the event
	 * @param time the event's ending time
	 */
	public void setTimeEnd(Date timeEnd) {
		this.timeEnd = timeEnd;
	}

	/**
	 * Retrieves the location of the event
	 * @return the location of the event
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Assigns a new location to an event
	 * @param location the new location of the event
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Retrieves the admission rate
	 * @return the admission rate
	 */
	public double getAdmission() {
		return admission;
	}

	/**
	 * Assigns a new admission rate
	 * @param admission the new admission rate
	 */
	public void setAdmission(double admission) {
		this.admission = admission;
	}

	/**
	 * Retrieves the description of the event
	 * @return the description of the event
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Assigns a new description for the event
	 * @param description the description of the event
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Get the list of attendees
	 * @return the list of attendees
	 */
	public Set<Attendee> getAttending() {
		return attending;
	}

	/**
	 * Revises the list of attendees
	 * @param attending the new list of attendees
	 */
	public void setAttending(Set<Attendee> attending) {
		this.attending = attending;
	}

	/**
	 * Retrieves the image of an event
	 * @return the image of an event
	 */
	public String getImage() {
		return image;
	}

	/**
	 * Assigns a new image to an event
	 * @param image the new image url to assign
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * Retrieves the list of posts for an event
	 * @return the list of posts for an event
	 */
	public Set<EventPost> getPosts() {
		return posts;
	}

	/**
	 * Sets the posts for an event
	 * @param posts the new post list for an event
	 */
	public void setPosts(Set<EventPost> posts) {
		this.posts = posts;
	}

	/**
	 * Retrieves the interested users of this event
	 * @return the number of interested users
	 */
	public Set<EventInterest> getInterestedUsers() {
		return interestedUsers;
	}

	/**
	 * Sets a number of interested users
	 * @param interestedUsers the new list of interested users
	 */
	public void setInterestedUsers(Set<EventInterest> interestedUsers) {
		this.interestedUsers = interestedUsers;
	}

	/**
	 * Retrieves the users that have event rights to the event
	 * @return the event rights for this event
	 */
	public Set<EventAssociation> getEventRights() {
		return eventRights;
	}

	/**
	 * Assigns a new list of event rights for an event
	 * @param eventRightsthe new list of event rights.
	 */
	public void setEventRights(Set<EventAssociation> eventRights) {
		this.eventRights = eventRights;
	}

	/**
	 * Retrieves the priority level of an event
	 * @return the priority level of an event
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * Assigns a new priority level to an event the higher the value the more priority.
	 * @param priority the new priority level of the event
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}

	/**
	 * Dummy Constructor
	 */
	public Event() {
		
	}
	
	/**
	 * The unique ID of the post
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The owner of the post
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="owner_id")
	private User owner;
	
	/**
	 * The time stamp for when the date was posted
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	@CreationTimestamp
	@Column(name = "timestamp")
	private Date timestamp;
	
	/**
	 * The name of the event
	 */
	@Field
	@Column(name = "name")
	private String name;
	
	/**
	 * The starting time of the event
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	@Column(name = "time_start")
	private Date timeStart;
	
	/**
	 * The starting time of the event
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSX")
	@Column(name = "time_end")
	private Date timeEnd;
	
	/**
	 * The location of the event
	 */
	@Field
	@Column(name = "location")
	private String location;
	
	/**
	 * The admission rate of the event
	 */
	@Column(name = "admission")
	private double admission;
	
	/**
	 * The description of the event
	 */
	@Field
	@Column(name = "description")
	private String description;
	
	/**
	 * The picture of the event
	 */
	@Column(name = "image")
	private String image;
	
	/**
	 * The priority level of a event
	 */
	@Column(name = "priority")
	private int priority;
	
	/**
	 * The attendee list
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "event")
	private Set<Attendee> attending = new HashSet<>();
	
	/**
	 * The list of posts on the event
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "event")
	private Set<EventPost> posts = new HashSet<>();
	
	/**
	 * The list of interested users
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "event")
	private Set<EventInterest> interestedUsers = new HashSet<>();
	
	/**
	 * The event rights associated with the event
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "event")
	private Set<EventAssociation> eventRights = new HashSet<>();

}
