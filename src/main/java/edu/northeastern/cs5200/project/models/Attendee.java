package edu.northeastern.cs5200.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a single attendee
 * @author Nicholas Carugati
 *
 */
@Entity(name = "attendees")
public class Attendee {
	
	/**
	 * Retrieves the unique id of the instance
	 * @return the id of the attendee
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id of the instance
	 * @param id the id to assign
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the event of the attendee
	 * @return the event of the attendee
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Assigns a new event for the attendee
	 * @param event the new event to assign
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Retrieves the attendee of this event
	 * @return the attendee of this event
	 */
	public User getAttendee() {
		return attendee;
	}

	/**
	 * Assigns a new attendee for the event
	 * @param attendee the new attendee of the event
	 */
	public void setAttendee(User attendee) {
		this.attendee = attendee;
	}

	/**
	 * Dummy constructor
	 */
	public Attendee() {

	}
	
	/**
	 * The unique ID of the instance
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The event of the instance
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="event_id")
	private Event event;
	
	/**
	 * The attendee of the event
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="attendee_id")
	private User attendee;

}
