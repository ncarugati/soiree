package edu.northeastern.cs5200.project.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a single category
 * @author Nicholas
 *
 */
@Entity(name = "categories")
public class Category implements Serializable {
	
	/**
	 * Generate Serial Version UID
	 */
	private static final long serialVersionUID = 87104875371884733L;

	/**
	 * Retrieves the id of the category
	 * @return the id of the category
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id for a category
	 * @param id the new id to assign the category
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the name of the category
	 * @return the name of the category
	 */
	public String getName() {
		return name;
	}

	/**
	 * Assigns a new name to the category
	 * @param name a new name for the category instance
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Creates a category with specifics
	 * @param id the id of the category
	 * @param name the name of the category
	 */
	public Category(int id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Dummy constructor
	 */
	public Category() {}
	
	/**
	 * The unique id of the category
	 */
	@Id
	@Column(name="id")
	private int id;
	
	/**
	 * The name of the category
	 */
	@Column(name="name")
	private String name;
	
	/**
	 * The interests this category is mapped to
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "category")
	private List<Interest> interests = new ArrayList<>();

}
