package edu.northeastern.cs5200.project.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Represents a follower/followee entity
 * @author Nicholas Carugati
 *
 */
@Entity(name = "followers")
public class Follower {

	/**
	 * Retrieves the unique ID of the follower relationship
	 * @return the unique ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new ID of the follower relationship
	 * @param id the id of the relationship
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the follower for this instace
	 * @return the follower
	 */
	public User getFollower() {
		return follower;
	}

	/**
	 * Assigns a follower for this relationship
	 * @param follower the new follower to assign
	 */
	public void setFollower(User follower) {
		this.follower = follower;
	}

	/**
	 * Retrieves the followee of this following relationship
	 * @return the followee
	 */
	public User getFollowee() {
		return followee;
	}

	/**
	 * Assigns a new followee to this relationship
	 * @param followee a new followee
	 */
	public void setFollowee(User followee) {
		this.followee = followee;
	}
	
	/**
	 * Dummy constructor
	 */
	public Follower() {

	}
	
	/**
	 * The unique id of a follower relationship
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	/**
	 * The person who is following
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="follower_id")
	private User follower;
	
	/**
	 * The person who is being followed
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="followee_id")
	private User followee;



}
