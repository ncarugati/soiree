package edu.northeastern.cs5200.project.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a unique event role within the system
 * @author Nicholas Carugati
 *
 */
@Entity(name="event_role")
public class EventRole {

	/**
	 * Retrieves the id of the event role
	 * @return the id of the event role
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the event role
	 * @param id the new id of the event role
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the name of the event role
	 * @return the name of the event role
	 */
	public String getName() {
		return name;
	}

	/**
	 * Assigns a new name for the unique event role
	 * @param name the new name of the event role to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Gets all the listed rights for a specific right
	 * @return the listed events and users assigned that right
	 */
	public Set<EventAssociation> getListedRights() {
		return listedRights;
	}

	/**
	 * Assigns a new list of events-user rights for that associated right
	 * @param listedRights the new list of event-user rights
	 */
	public void setListedRights(Set<EventAssociation> listedRights) {
		this.listedRights = listedRights;
	}

	/**
	 * Dummy Constructor
	 */
	public EventRole() {
		
	}
	
	/**
	 * The unique id of the event role
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The name of the unique role
	 */
	@Column(name = "name")
	private String name;

	/**
	 * The list of rights enforced on events to users
	 */
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "role")
	private Set<EventAssociation> listedRights = new HashSet<>();

}
