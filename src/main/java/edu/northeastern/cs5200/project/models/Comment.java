package edu.northeastern.cs5200.project.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a single comment
 * @author Nicholas Carugati
 *
 */
@Entity(name = "comments")
public class Comment {

	/**
	 * Retrieves the unique ID of this comment
	 * @return the ID of this comment
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new ID to this comment
	 * @param id the new id of this comment
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the owner of this comment
	 * @return the owner of this comment
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * Assigns a new owner to this comment
	 * @param owner the new owner of this comment
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}

	/**
	 * Retrieves the post associated with this comment
	 * @return the post associated with this comment
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * Assigns a new post to this comment
	 * @param post the new post the comment will be attached to
	 */
	public void setPost(Post post) {
		this.post = post;
	}

	/**
	 * Retrieves the content of the comment
	 * @return the content of the comment
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Assigns new content for the comment
	 * @param content the new content string
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * Retrieves when the comment was posted
	 * @return the timestamp of the comment
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Sets a new time for when the comment was posted
	 * @param timestamp the new time to set to
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Dummy constructor
	 */
	public Comment() {
		
	}
	
	/**
	 * The unique ID of the comment
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The owner of the comment
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="owner_id")
	private User owner;
	
	/**
	 * The post that the comment is attached to
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="post_id")
	private Post post;
	
	/**
	 * The content of the comment
	 */
	@Column(name = "content")
	private String content;
	
	/**
	 * When the comment was created
	 */
	@CreationTimestamp
	@Column(name = "timestamp")
	private Date timestamp;

}
