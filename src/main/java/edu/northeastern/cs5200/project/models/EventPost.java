package edu.northeastern.cs5200.project.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a reification of a list of posts within an event
 * @author Nicholas Carugati
 *
 */
@Entity(name = "event_posts")
public class EventPost {

	/**
	 * Retrieves the unique id of the relationship
	 * @return the id of the instance
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to the instance
	 * @param id the new id of the instance
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the event in this relationship
	 * @return the event that is in the relationship
	 */
	public Event getEvent() {
		return event;
	}

	/**
	 * Assigns a new event for this instance
	 * @param event the new event to assign
	 */
	public void setEvent(Event event) {
		this.event = event;
	}

	/**
	 * Retrieves the post of this instance
	 * @return the post of this instance
	 */
	public Post getPost() {
		return post;
	}

	/**
	 * Assigns a new post of this instance
	 * @param post the new post to assign to the instance
	 */
	public void setPost(Post post) {
		this.post = post;
	}

	/**
	 * Dummy constructor
	 */
	public EventPost() {
		
	}
	
	/**
	 * The id of the event post
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonIgnore
	@Column(name = "id")
	private int id;
	
	/**
	 * The event which has the post
	 */
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="event_id")
	private Event event;
	
	/**
	 * The post that is part of the event
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="post_id", nullable = false)
	private Post post;
	
}
