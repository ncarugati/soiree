package edu.northeastern.cs5200.project.models;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Represents a single post
 * @author Nicholas Carugati
 *
 */
@Entity(name = "posts")
public class Post {

	/**
	 * Retrieves the unique id of a post
	 * @return the id of a single post
	 */
	public int getId() {
		return id;
	}

	/**
	 * Assigns a new id to a single post
	 * @param id the new id of the post
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Retrieves the owner of the post
	 * @return the owner of the post
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * Assigns a new owner to a post
	 * @param owner the new owner to assign to
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}
	

	/**
	 * Retrieves when the date was posted
	 * @return the date of the post
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * Assigns a new creation date of the post
	 * @param timestamp the new date instance to assign
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	
	/**
	 * Gets the content of a post
	 * @return the content of a post
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content of a post
	 * @param content the new content to show
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Retrieves the comments for a post
	 * @return the comments for a post
	 */
	public Set<Comment> getComments() {
		return comments;
	}

	/**
	 * Modifies the list of comments for a post
	 * @param comments the new list of comments
	 */
	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Retrieves the post relationship from an event (If applicable)
	 * @return the posting from the event
	 */
	public EventPost getEventPost() {
		return eventPost;
	}

	/**
	 * Sets the post relationship to an event
	 * @param post the new post-event relationship
	 */
	public void setEventPost(EventPost eventPost) {
		this.eventPost = eventPost;
	}

	/**
	 * Retrieves the post relationship given the post is located on a profile
	 * @return the post instance
	 */
	public ProfilePost getProfilePost() {
		return profilePost;
	}

	/**
	 * Assigns a new profile post relationship
	 * @param profilePost the profile post instance to assign
	 */
	public void setProfilePost(ProfilePost profilePost) {
		this.profilePost = profilePost;
	}

	/**
	 * Retrieves the ratings of a post
	 * @return the ratings of a post
	 */
	public Set<Rating> getRatings() {
		return ratings;
	}

	/**
	 * Assigns new ratings to a post
	 * @param ratings the new list of ratings to assign
	 */
	public void setRatings(Set<Rating> ratings) {
		this.ratings = ratings;
	}

	/**
	 * Retrieves the attachment metadata for this post
	 * @return
	 */
	public String getAttachment() {
		return attachment;
	}

	/**
	 * Assigns new attachment metadata for this post 
	 * @param attachment the new attachment to assign to the post
	 */
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	/**
	 * Determines if a post is locked
	 * @return the lock status on a post
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * Sets the lock state on a post
	 * @param locked the new lock state assignment
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	/**
	 * Dummy constructor
	 */
	public Post() {
		
	}
	
	/**
	 * The unique ID of the post
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	/**
	 * The owner of the post
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="owner_id")
	private User owner;
	
	/**
	 * The time stamp for when the date was posted
	 */
	@CreationTimestamp
	@Column(name = "timestamp")
	private Date timestamp;
	
	/**
	 * The content of the post
	 */
	@Column(name = "content")
	private String content;
	
	/**
	 * The attachment that is associated with this post.
	 */
	@Column(name="attachment")
	private String attachment;
	
	/**
	 * A predicate which determines if other users can post additional comments
	 */
	@Column(name="locked")
	private boolean locked;
	
	/**
	 * The list of comments on the post
	 */
	@OrderBy("timestamp ASC")
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "post")
	private Set<Comment> comments = new HashSet<>();
	
	/**
	 * The post instance in relation to an event
	 */
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL, orphanRemoval = true, mappedBy = "post")
	private EventPost eventPost;
	
	/**
	 * The post instance in relation to a profile
	 */
	@JsonIgnore
	@OneToOne(fetch = FetchType.LAZY, cascade =  CascadeType.ALL, orphanRemoval = true, mappedBy = "post")
	private ProfilePost profilePost;
	
	/**
	 * The list of ratings for the post
	 */
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "post")
	private Set<Rating> ratings = new HashSet<>();
	

}
