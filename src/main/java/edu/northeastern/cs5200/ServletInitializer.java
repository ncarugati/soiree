package edu.northeastern.cs5200;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Initializes the Servlet Service for production services
 * @author Nicholas Carugati
 *
 */
public class ServletInitializer extends SpringBootServletInitializer {

	/**
	 * Finds and loads the configuration files for the servlet
	 * @param application the application builder
	 * @return the sources initialized for the application
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Cs5200Spring2018CarugatiApplication.class);
	}

}
