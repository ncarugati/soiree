package edu.northeastern.cs5200;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * The main indexer which initializes the search engine
 * @see http://hibernate.org/search/documentation/getting-started/
 * @author Nicholas Carugati
 *
 */
@Component
public class SearchIndexer implements ApplicationListener<ApplicationReadyEvent> {

	/**
	 * The entity manager which persists full text queries
	 */
	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Initializes and indexes entities related to the database on start up
	 * @param event the application event which was invoked.
	 */
	@Override
	public void onApplicationEvent(final ApplicationReadyEvent event) {
		try {
			FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
			fullTextEntityManager.createIndexer().startAndWait();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		return;
	}

}