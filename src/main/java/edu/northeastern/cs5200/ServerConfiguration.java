package edu.northeastern.cs5200;

import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.http.HttpStatus;

/**
 * Server configuration which handles special case views
 * @author Nicholas Carugati
 *
 */
public class ServerConfiguration extends ServerProperties {

	/**
	 * Customizes special view pages for the servlet
	 * @param container the configurable container instance
	 */
	@Override
	public void customize(ConfigurableEmbeddedServletContainer container) {
		super.customize(container);
		container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/notfound.html"));
	}

}