(function () {
	angular
	.module('SoireeApp')
	.directive('eventThumb', ['$routeParams', eventThumb]);

	function eventThumb() {

		function linkFunc(scope, element, attributes) {
			if(attributes.eData) {
				attributes.$observe('eventThumb', function() {
					var data = JSON.parse(attributes.eData);
					scope.thumb.initialize(data);
				});				
			}
		}

		return {
			scope: {},
			link: linkFunc,
			controller: ThumbnailController,
			controllerAs: "thumb",
			templateUrl: 'views/thumbnails/event-thumbnail.html'
		};
	}


	function ThumbnailController($scope, $rootScope, $routeParams, $route, $location, $window, $mdDialog) {
		var vm = this;
		vm.initialize = initialize;
		vm.toReadableDate = toReadableDate;
		vm.eData = null;
		
		function initialize(eData) {
			if(eData.timeEnd || eData.timeStart || !eData.link) {
				eData.start = eData.timeStart;
				eData.end = eData.timeEnd;
				eData.link = "/events/" + eData.id;
			}
			vm.eData = eData;
		}
		
		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}
		
	}



})();