(function () {
	angular
	.module('SoireeApp')
	.directive('menuBar', ['$routeParams', menuBar]);

	function menuBar() {

		function linkFunc(scope, element, attributes) {
			
		}

		return {
			scope: {},
			link: linkFunc,
			controller: MenubarController,
			controllerAs: "menubar",
			templateUrl: "views/menu.html"
		};
	}


	function MenubarController($scope, $rootScope, $routeParams, $route, $location, $window, $mdDialog, $mdSidenav, UserService, MessageService) {
		var vm = this;
		vm.openMainMenu = openMainMenu;
		vm.logout = logout;
		vm.createEventDialog = createEventDialog;
		vm.search = search;
		vm.isAdmin = false;
		vm.toggleMobileNav = toggleMobileNav;
		
		function openMainMenu($mdOpenMenu, ev) {
			originatorEv = ev;
			$mdOpenMenu(ev);
		}
		
		function toggleMobileNav() {
			$mdSidenav('mobileToggle').toggle();
		}
	
		function logout() {
			UserService.logout().then(function() {
				$location.url("/login");
				$rootScope.sessionId = undefined;
				$rootScope.userData = undefined;
			}, function(error) {
				console.error(error);
			})
		}

		function createEventDialog(evt) {
			$mdDialog.show({
				controller: "CreateEventDialogController",
				controllerAs: "evtdialog",
				templateUrl: 'views/dialogs/create-event.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
			}).then(function(result) {
				if(result) {
					$rootScope.$broadcast('eventCreation', result);
				}
			}, function(error) {});
		}
		
		function search(keyword) {
			if(keyword == undefined) {
				return;
			}
			$location.url("/search?query=" + keyword);
		}
	}



})();