(function() {
	angular
	.module("SoireeApp")
	.controller("MessageController", MessageController)
	
	function MessageController($routeParams, $rootScope, $scope, $location, $mdDialog, MessageService) {
		var vm = this;
		vm.conversationTuple = [];
		vm.unreadMessages = [];
		vm.composeMessageDialog = composeMessageDialog;
		vm.getRecipientFocus = getRecipientFocus;
		vm.toReadableDate = toReadableDate;
		vm.archiveConversation = archiveConversation;
		vm.hideArchivedEntry = hideArchivedEntry;
		vm.findConversationsForUser = findConversationsForUser;
		
		function composeMessageDialog(evt) {
			$mdDialog.show({
				controller: "MessageDialogController",
				controllerAs: "messd",
				templateUrl: 'views/dialogs/create-message.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
			}).then(function(result) {
				
			}, function(error) {});
		}
		

		
		function init() {
			findConversationsForUser();
		}
		init();
		
		function getRecipientFocus(convo) {
			if(convo.sender.id == $rootScope.sessionId) {
				return "recipient"
			}
			return "sender"
		}
		
		
		function hideArchivedEntry(message) {
			var convo = message.conversation;
			if(convo.participantOne.id == $rootScope.sessionId) {
				return convo.partOneArchived;
			} else {
				return convo.partTwoArchived;
			}
		}
		function archiveConversation(entry) {
			var prom = MessageService.archiveConversation(entry.conversation.id);
			prom.then(findConversationsForUser);
		}
		
		function findConversationsForUser() {
			var prom = MessageService.getConversations();
			prom.then(function(result) {
				vm.conversationTuple = result.data;
				for(var i = 0; i < vm.conversationTuple.length; i++) {
					var entry = vm.conversationTuple[i];
					if(entry.recipient.id == $rootScope.sessionId && !entry.read) {
						vm.unreadMessages.push(entry);
						vm.conversationTuple.splice(i, 1);
					}
				}
			}, function(error) {
				console.error(error);
			});
		}
		
		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}
		
		
	}
	
})();