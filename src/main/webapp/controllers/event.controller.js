(function() {
	angular
	.module("SoireeApp")
	.controller("EventController", EventController)
	.controller("EventSettingsController", EventSettingsController)
	.controller("EventInfoController", EventInfoController);

	function EventController($q, $rootScope, $scope, $routeParams, $location, $sce, $mdDialog,
			EventService, PostService, RatingService, CommentService) {
		var vm = this;
		vm.condenseDate = condenseDate;
		vm.submitPost = submitPost;
		vm.submitComment = submitComment;
		vm.toReadableDate = toReadableDate;
		vm.getRateCount = getRateCount; 
		vm.ratePost = ratePost;
		vm.attendEvent = attendEvent;
		vm.expressInterest = expressInterest;
		vm.postInit = postInit;
		vm.editPost = editPost;
		vm.editComment = editComment;
		vm.deletePost = deletePost;
		vm.deleteComment = deleteComment;
		vm.isEntryOwner = isEntryOwner;
		vm.hasControls = hasControls;
		
		vm.shareEventDialog = shareEventDialog;
		vm.hasEnded = hasEnded;
		vm.lockPost = lockPost;
		
		vm.currUserRights = null;
		vm.eventData = null;
		vm.eventPosts = null;
		vm.loaded = false;
		vm.isEventOwner = false;


		vm.isCurrentUserInterested = false;
		vm.isCurrentUserAttending = false;
		vm.currentUserRating = 0;


		function init() {
			retrieveAllEventInformation();
		}
		init();


		function retrieveAllEventInformation() {
			var eventId = $routeParams["eid"];
			var promises = [];
			if(eventId) {
				promises.push(getEvent(eventId));
				promises.push(isInterested());
				promises.push(isAttending());
				promises.push(findRightsForEvent());
			}
			$q.all(promises).then(function(result) {
				vm.loaded = true;
			});
		}

		function getEvent(eventId) {
			prom = EventService.findEventById(eventId);
			prom.then(function(result) {
				vm.eventData = result.data;
				if(result.data.image == 'null') {
					result.data.image = null;
				}
				if($rootScope.sessionId == vm.eventData.owner.id) {
					vm.isEventOwner = true;
				}
				getEventPosts(eventId);
			});
		}

		function getEventPosts(eventId) {
			prom = EventService.getEventPosts(eventId);
			prom.then(function(result) {
				vm.eventPosts = result.data;
			});
		}
		
		function hasControls(entry) {
			var userId = $rootScope.sessionId;
			if(userId == entry.owner.id) {
				return true;
			}
			if(vm.currUserRights != null && vm.currUserRights.id == 3) {
				return true;
			}
			if(vm.isEventOwner) {
				return true;
			}
			return false;
		}

		function isEntryOwner(entry) {
			var userId = $rootScope.sessionId;
			if(userId == entry.owner.id) {
				return true;
			}
			return false;
		}
		
		function hasEnded() {
			if(vm.eventData == null) {
				return true;
			}
			var now = new Date().getTime();
			return vm.eventData.timeEnd < now;
		}

		function isInterested() {
			if(!$rootScope.sessionId) {
				return;
			}
			var eventId = $routeParams["eid"];
			var prom = EventService.getInterestStatusForUser(eventId);
			prom.then(function(result) {
				vm.isCurrentUserInterested = result.data;
			});
		}

		function isAttending() {
			if(!$rootScope.sessionId) {
				return;
			}
			var eventId = $routeParams["eid"];
			var prom = EventService.getAttendStatusForUser(eventId);
			prom.then(function(result) {
				vm.isCurrentUserAttending = result.data;
			});
		}

		function condenseDate(startDate, endDate) {
			var sDate = new Date(startDate);
			var eDate = new Date(endDate);
			if(sDate.getDate() == eDate.getDate()) {
				return " " + sDate.toLocaleString() + " to " + eDate.toLocaleTimeString();
			}
			return " " + sDate.toLocaleString() + " to " + eDate.toLocaleString();
		}

		function attendEvent() {
			var eventId = $routeParams["eid"];
			var prom = null;
			if(vm.isCurrentUserAttending) {
				prom = EventService.rescindAttendance(eventId);
				prom.then(function(result) {
					isCurrentUserAttending = false;
					retrieveAllEventInformation();
				}, function(error) {
					isCurrentUserAttending = true;
				});
			} else {
				prom = EventService.attendEvent(eventId);
				prom.then(function(result) {
					isCurrentUserAttending = true;
					retrieveAllEventInformation();
				}, function(error) {
					isCurrentUserAttending = false;
				});
			}
		}

		function expressInterest() {
			var eventId = $routeParams["eid"];
			var prom = null;
			if(vm.isCurrentUserInterested) {
				prom = EventService.rescindEventInterest(eventId);
				prom.then(function(result) {
					isCurrentUserInterested = false;
					retrieveAllEventInformation();
				}, function(error) {
					isCurrentUserInterested = true;
				});
			} else {
				prom = EventService.invokeEventInterest(eventId);
				prom.then(function(result) {
					isCurrentUserInterested = true;
					retrieveAllEventInformation();
				}, function(error) {
					isCurrentUserInterested = false;
				});
			}
		}
		

		//Posting and Comment Routines

		function postInit(post) {
			post.rating = getRateCount(post.ratings);
			post.isEditing = false;
			var userRating = getUserRatingForPost(post);
			userRating.then(function(data) {
				post.userRating = data;
			});
		}

		function getUserRatingForPost(post) {
			var deferred = $q.defer();
			var prom = RatingService.findRatingForUser(post.id);
			prom.then(function(result) {
				deferred.resolve(result.data.rating);
			}, function(error) {
				deferred.reject();
			});
			return deferred.promise;
		}
		
		function findRightsForEvent() {
			if(vm.isEventOwner || !$rootScope.sessionId) {
				return;
			}
			var eventId = $routeParams["eid"];
			var prom = EventService.getRightsForEvent($rootScope.sessionId, eventId);
			prom.then(function(result) {
				if(result.data) {
					vm.currUserRights = result.data.role;
				}
			});
		}

		function ratePost(postId, value) { //A little bit of callback hell for you all
			var prom = RatingService.findRatingForUser(postId);
			var eventId = $routeParams["eid"];
			prom.then(function(result) {
				var _prom = null;
				if(result.data) {
					if(value == result.data.rating) {
						_prom = RatingService.removeRating(postId);
						_prom.then(function(resultant) {
							getEventPosts(eventId);
						});
					} else {
						_prom = RatingService.updateRating(postId, {"rating": value});
						_prom.then(function(resultant) {
							getEventPosts(eventId);
						});
					}
				} else {
					_prom = RatingService.ratePost(postId, {"rating": value});
					_prom.then(function(reusltant) {
						getEventPosts(eventId);
					});
				}
			}, function(error) {
				console.error(error);
			});
		}

		function submitPost(content) {
			var post = { "content": content };
			var eventId = $routeParams["eid"];
			var prom = PostService.createPostForEvent(eventId, post);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function lockPost(postId) {
			var eventId = $routeParams["eid"];
			var prom = PostService.togglePostLock(postId);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			});
		}

		function editPost(postId, content) {
			var post = {"content": content };
			var eventId = $routeParams["eid"];
			var prom = PostService.updatePost(postId, post);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function editComment(commentId, content) {
			var comment = {"content": content };
			var eventId = $routeParams["eid"];
			var prom = CommentService.updateComment(commentId, comment);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function deletePost(postId) {
			var eventId = $routeParams["eid"];
			var prom = PostService.deletePost(postId);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function deleteComment(commentId) {
			var eventId = $routeParams["eid"];
			var prom = CommentService.deleteComment(commentId);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function submitComment(postId, content) {
			var comment = { "content": content };
			var eventId = $routeParams["eid"];
			var prom = CommentService.createComment(postId, comment);
			prom.then(function(result) {
				if(result) {
					getEventPosts(eventId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function getRateCount(tuple) {
			var total = 0;
			for(var i = 0; i < tuple.length; i++) {
				total += tuple[0].rating;
			}
			return total;
		}

		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}
		
		
		function shareEventDialog(evt, eventEntity) {
			$mdDialog.show({
				controller: "ShareController",
				controllerAs: "share",
				templateUrl: 'views/dialogs/share-post.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "attachedContent": eventEntity }
			}).then(function(result) {
				
			}, function(error) {});
		}
	}
	
	function EventInfoController($q, $rootScope, $scope, $routeParams, $location, $compile, EventService, UserService, FollowService) {
		var vm = this;
		vm.eventData = null;
		vm.sessionFollowingList = {};
		vm.loaded = false;
		vm.isSessionFollowing = isSessionFollowing;
		vm.followUser = followUser;
		vm.unfollowUser = unfollowUser;
		
		function getEvent(eventId) {
			prom = EventService.findEventById(eventId);
			prom.then(function(result) {
				vm.eventData = result.data;
				if(result.data.image == 'null') {
					result.data.image = undefined;
				}
			});
		}
		
		function init() {
			var eventId = $routeParams["eid"];
			var promises = [];
			promises.push(getEvent(eventId));
			promises.push(prepareSessionFollowing());
			$q.all(promises).then(function(result) {
				vm.loaded = true;
			});
		}
		init();
		
		function prepareSessionFollowing() {
			var prom = FollowService.getFollowingData($rootScope.sessionId);
			prom.then(function(result) {
				for(var i = 0; i < result.data.length; i++) {
					var entry = result.data[i].follower.id;
					if($rootScope.sessionId != entry) {
						vm.sessionFollowingList[entry] = true;
					}
				}
			});
		}
		
		function followUser(userId) {
			if(userId == $rootScope.sessionId) {
				return;
			}
			var prom = FollowService.follow(userId);
			prom.then(function(result) {
				vm.sessionFollowingList[userId] = true;
			});
		}
		
		function unfollowUser(userId) {
			var prom = FollowService.unfollow(userId);
			prom.then(function(result) {
				vm.sessionFollowingList[userId] = undefined;
			});
		}
		
		function isSessionFollowing(userId) {
			if(vm.sessionFollowingList[userId] == undefined) {
				return false;
			}
			return vm.sessionFollowingList[userId];
		}

	}

	function EventSettingsController($q, $rootScope, $scope, $routeParams, $location, $compile, EventService, UserService) {
		var vm = this;
		vm.modifyEvent = modifyEvent;
		vm.deleteEvent = deleteEvent;
		vm.getAllUsers = getAllUsers;
		vm.querySearch = querySearch;
		vm.grantRightsToUser = grantRightsToUser;
		vm.alterRightsToUser = alterRightsToUser;
		vm.revokeRightsToUser = revokeRightsToUser;
		vm.uplaod = upload;
		vm.deviseStatusMessage = deviseStatusMessage;
		
		vm.rightsCatalog = [];
		vm.userList = [];
		vm.rightsList = [];
		vm.userSelect = null;
		vm.userSearchText = "";
		vm.eventData = null;
		vm.selectedRole = null;
		vm.isOwner = false;
		vm.currUserRights = null;
		vm.message = null;
		
		vm.allDay = false;
		isEditingRights = false;
		
		function modifyEvent(event) {
			if(vm.allDay) {
				if(vm.eventData.startDate == undefined) {
					vm.message = {"message": "You must specify a start date", "type": "info"};
					return;
				}
				var startDate = vm.eventData.startDate;
				event.timeStart = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0);
				event.timeEnd = new Date(event.timeStart.getFullYear(), event.timeStart.getMonth(), event.timeStart.getDate(), 23, 59, 59);
				var now = new Date().getTime();
				if(event.timeStart.getTime() < now) {
					vm.message = {"message": "You cannot pick a date that has passed.", "type": "error"};
					return;
				}
			} else {
				if(event.startDate == undefined) {
					vm.message = {"message": "You must specify a start date", "type": "info"};
					return;
				}
				if(event.startTime == undefined) {
					vm.message = {"message": "You must specify a start time", "type": "info"};
					return;
				}
				var startDateMs = vm.eventData.startDate.getTime();
				var startTimeMs = vm.eventData.startTime.getTime();
				startDateMs += startTimeMs;
				event.timeStart = new Date(startDateMs);
				
				
				if(event.endDate == undefined) {
					vm.message = {"message": "You must specify a end date", "type": "info"};
					return;
				}
				if(event.endTime == undefined) {
					vm.message = {"message": "You must specify a end time", "type": "info"};
					return;
				}
				var endDateMs = vm.eventData.endDate.getTime();
				var endTimeMs = vm.eventData.endTime.getTime();
				endDateMs += endTimeMs;
				event.timeEnd = new Date(endDateMs);

				var diffCheck = event.timeEnd.getTime() - event.timeStart.getTime();
				if(diffCheck < 0) {
					vm.message = {"message": "End time is before the start time.", "type": "error"};
					return;
				}
				if(event.timeStart.getTime() < Date.now()) {
					vm.message = {"message": "You cannot pick a date that has passed.", "type": "error"};
					return;
				}
			}
			var prom = EventService.updateEvent(event.id, event);
			prom.then(function(result) {
				if(result) {
					vm.message = {"message": "Event successfully edited", "type": "success"};
				}
			}, function(error) {
				vm.message = {"message": "Unable to modify event.", "type": "error"};
			});
		}
		
		function upload() {
			var headers = {	transformRequest: angular.identity,
							transformResponse: undefined,
							headers: {"Content-Type": undefined}};
			$http.post("/api/upload", new FormData(document.getElementById("eventSettingsForm")), headers)
					.then(function(result) {
					vm.eventData.image = result.data;
			});	
		}

		function deleteEvent(eventId) {
			var prom = EventService.deleteEvent(eventId);
			prom.then(function(result) {
				if(result) {
					$location.url("/main/" + $rootScope.sessionId);
				}
			}, function(error) {
				vm.message = {"message": "Unable to delete event", "type": "error"};
			});
		}

		function populateEditTime(eventEdit) {
			var startDate = new Date(eventEdit.timeStart);
			var endDate = new Date(eventEdit.timeEnd);
			var startDateSplit = new Date(startDate.toLocaleDateString()).getTime();
			var endDateSplit =  new Date(endDate.toLocaleDateString()).getTime();
			if(startDateSplit == endDateSplit) {
				vm.allDay = true;
			}
			eventEdit.startDate = startDate;
			eventEdit.startTime = new Date(startDate.getTime() - startDateSplit);
			eventEdit.endDate = endDate;
			eventEdit.endTime = new Date(endDate.getTime() - endDateSplit);
		}

		function getAllUsers() {
			var prom = UserService.getAllUsers(false);
			prom.then(function(result) {
				vm.userList = result.data;
			}, function(error) {
				vm.message = {"message": "Unable to retrieve users", "type": "error"};
			});
		}
		
		/**
		 * Search filtering system
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function querySearch(query) {
			var results = query ? vm.userList.filter(createFilterFor(query)) : [];
			return results;
		}

		/**
		 * Create filter function for a query string
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function createFilterFor(query) {
			var lowercaseQuery = angular.lowercase(query);
			return function filterFn(user) {
				return (user.username.toLowerCase().indexOf(lowercaseQuery) === 0);
			};
		}
	
		
		function getEvent(eventId) {
			prom = EventService.findEventById(eventId);
			prom.then(function(result) {
				vm.eventData = result.data;
				if($rootScope.sessionId == vm.eventData.owner.id) {
					vm.isOwner = true;
				}
				vm.eventData.admission = parseFloat(vm.eventData.admission);
				populateEditTime(vm.eventData);
				vm.rightsList = vm.eventData.eventRights;
			});
		}
		
		function findRightsForEvent() {
			if(vm.isOwner) {
				return;
			}
			var eventId = $routeParams["eid"];
			var prom = EventService.getRightsForEvent($rootScope.sessionId, eventId);
			prom.then(function(result) {
				vm.currUserRights = result.data.role;
			});
		}
		

		function getRightsCatalog() {
			var prom = EventService.getRightsCatalog();
			prom.then(function(result) {
				vm.rightsCatalog = result.data;
			}, function(error) {
				vm.message = {"message": "Unable to get rights catalog", "type": "error"};
			})
		}
		
		function grantRightsToUser(user, right) {
			var eventId = $routeParams["eid"];
			var data = {"user": user, "role": right}
			var prom = EventService.grantRightsToUser(eventId, data);
			prom.then(function(result) {
				if(result) {
					getEvent(eventId);
				}
			});
		}
		
		function alterRightsToUser(rights) {
			var eventId = $routeParams["eid"];
			var data = { "role": rights.alteredSelection }
			var prom = EventService.alterRights(eventId, rights.id, data);
			prom.then(function(result) {
				rights.isEditing = false;
				getEvent(eventId);
			});
		}
		
		function revokeRightsToUser(rightId) {
			var eventId = $routeParams["eid"];
			var prom = EventService.revokeRights(eventId, rightId);
			prom.then(function(result) {
				if(result) {
					getEvent(eventId);
				}
			});
		}
		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}
		
		function init() {
			var eventId = $routeParams["eid"];
			var promises = [];
			promises.push(getEvent(eventId));
			promises.push(getRightsCatalog());
			promises.push(getAllUsers());
			promises.push(findRightsForEvent());
			$q.all(promises);
		}
		init();
	}

})();