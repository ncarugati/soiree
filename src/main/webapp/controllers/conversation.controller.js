(function() {
	angular
	.module("SoireeApp")
	.controller("ConversationController", ConversationController)
	
	function ConversationController($q, $routeParams, $rootScope, $scope, $location, $mdDialog, MessageService) {
		var vm = this;
		vm.conversationTuple = [];
		vm.getRecipientFocus = getRecipientFocus;
		vm.toReadableDate = toReadableDate;
		vm.isSender = isSender;
		vm.sendMessage = sendMessage;
		vm.composedMessage = "";
		vm.conversationData = null;
		vm.findParticipant = findParticipant;
		vm.hideArchivedEntry = hideArchivedEntry;
		vm.archiveMessage = archiveMessage;
		vm.findConversationForUser = findConversationForUser;
		
		function init() {
			var promises = [];
			promises.push(findConversationInformation());
			promises.push(findConversationForUser());
			promises.push(readAllMessages());
			$q.all(promises);
		}
		init();
		
		function getRecipientFocus(convo) {
			if(convo.sender.id == $rootScope.sessionId) {
				return "recipient"
			}
			return "sender"
		}
		
		function isSender(entry) {
			return $rootScope.sessionId == entry.sender.id;
		}
		
		function findConversationInformation() {
			var conversationId = $routeParams["cid"];
			var prom = MessageService.findConversationById(conversationId);
			prom.then(function(result) {
				vm.conversationData = result.data;
			}, function(error) {
				console.error(error);
			});
		}
		
		function findConversationForUser() {
			var conversationId = $routeParams["cid"];
			var prom = MessageService.getConversation(conversationId);
			prom.then(function(result) {
				vm.conversationTuple = result.data;
			}, function(error) {
				console.error(error);
			});
		}
		
		function readAllMessages() {
			var conversationId = $routeParams["cid"];
			var prom = MessageService.readAllMessages(conversationId);
		}
		
		function sendMessage(userId) {
			var message = { "message": vm.composedMessage };
			var prom = MessageService.sendPersonalMessage(userId, message);
			prom.then(function(result) {
				findConversationForUser();
			});
		}
		
		function hideArchivedEntry(message) {
			if(isSender(message)) {
				return message.senderArchived;
			} else {
				return message.recipientArchived;
			}
		}
		
		function archiveMessage(message) {
			var prom = MessageService.archiveMessage(message.id);
			prom.then(function(result) {
				findConversationForUser();
			});
		}
		
		function findParticipant() {
			if(vm.conversationData != null) {
				if(vm.conversationData.participantOne.id == $rootScope.sessionId) {
					return vm.conversationData.participantTwo;
				} else {
					return vm.conversationData.participantOne;
				}
			}
			return null;
		}
		
		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}
		
		
	}
	
})();