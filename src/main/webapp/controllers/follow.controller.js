(function() {
	angular
	.module("SoireeApp")
	.controller("FollowerController", FollowerController)
	.controller("FollowingController", FollowingController)
	.controller("UserEventsController", UserEventsController);

	function FollowerController($q, $routeParams, $rootScope, $location, $sce, $scope, FollowService) {
		var vm = this;
		vm.followerList = [];
		vm.sessionFollowingList = {};
		vm.loaded = false;
		vm.isOwner = isOwner;
		vm.isSessionFollowing = isSessionFollowing;
		vm.followUser = followUser;
		vm.unfollowUser = unfollowUser;
		
		function isOwner() {
			var profileUserId = $routeParams["uid"];
			if(!$rootScope.sessionId) {
				return false;
			}
			return $rootScope.sessionId == profileUserId;
		}
		
		function init() {
			vm.loaded = false;
			var promises = [];
			promises.push(getFollowerData());
			promises.push(prepareSessionFollowing());
			$q.all(promises).then(function(result) {
				vm.loaded = true;
			});
		}
		init();
		
		function prepareSessionFollowing() {
			var prom = FollowService.getFollowingData($rootScope.sessionId);
			prom.then(function(result) {
				for(var i = 0; i < result.data.length; i++) {
					var entry = result.data[i].follower.id;
					vm.sessionFollowingList[entry] = true;
				}
			});
		}
		
		function getFollowerData() {
			
			var profileUserId = $routeParams["uid"];
			var prom = FollowService.getFollowerData(profileUserId);
			prom.then(function(result) {
				vm.followerList = result.data;
				vm.loaded = true;
			});
		}
		
		function isSessionFollowing(userId) {
			if(vm.sessionFollowingList[userId] == undefined) {
				return false;
			}
			return vm.sessionFollowingList[userId];
		}
		
		function followUser(userId) {
			if(userId == $rootScope.sessionId) {
				return;
			}
			var prom = FollowService.follow(userId);
			prom.then(function(result) {
				vm.sessionFollowingList[userId] = true;
			});
		}
		
		function unfollowUser(userId) {
			var prom = FollowService.unfollow(userId);
			prom.then(function(result) {
				vm.sessionFollowingList[userId] = false;
			});
		}
		
	}
	
	function FollowingController($q, $routeParams, $rootScope, $location, $sce, $scope, FollowService) {
		var vm = this;
		vm.followingList = [];
		vm.sessionFollowingList = {};
		vm.loaded = false;
		vm.isSessionFollowing = isSessionFollowing;
		vm.isOwner = isOwner;
		vm.followUser = followUser;
		vm.unfollowUser = unfollowUser;
		
		function prepareSessionFollowing() {
			var prom = FollowService.getFollowingData($rootScope.sessionId);
			prom.then(function(result) {
				for(var i = 0; i < result.data.length; i++) {
					var entry = result.data[i].follower.id;
					vm.sessionFollowingList[entry] = true;
				}
			});
		}
		
		function getFollowingData() {
			var profileUserId = $routeParams["uid"];
			var prom = FollowService.getFollowingData(profileUserId);
			prom.then(function(result) {
				vm.followingList = result.data;
			});
		}
		
		function isOwner() {
			var profileUserId = $routeParams["uid"];
			if(!$rootScope.sessionId) {
				return false;
			}
			return $rootScope.sessionId == profileUserId;
		}
		
		function isSessionFollowing(userId) {
			if(vm.sessionFollowingList[userId] == undefined) {
				return false;
			}
			return vm.sessionFollowingList[userId];
		}
		
		function followUser(userId) {
			if(userId == $rootScope.sessionId) {
				return;
			}
			var prom = FollowService.follow(userId);
			prom.then(function(result) {
				vm.sessionFollowingList[userId] = true;
			});
		}
		
		function unfollowUser(userId) {
			var prom = FollowService.unfollow(userId);
			prom.then(function(result) {
				vm.sessionFollowingList[userId] = false;
			});
		}
		
		function init() {
			vm.loaded = false;
			var promises = [];
			promises.push(getFollowingData());
			promises.push(prepareSessionFollowing());
			$q.all(promises).then(function(result) {
				vm.loaded = true;
			});
		}
		init();
		

	}
	
	function UserEventsController($routeParams, $rootScope, $mdDialog, $location, $sce, $scope, EventService) {
		var vm = this;
		vm.eventList = [];
		vm.loaded = false;
		
		vm.isOwner = isOwner;
		vm.toReadableDate = toReadableDate;
		vm.condenseDate = condenseDate;
		vm.createEventEditDialog = createEventEditDialog;
		vm.shareEventDialog = shareEventDialog;

		
		function isOwner() {
			var profileUserId = $routeParams["uid"];
			if(!$rootScope.sessionId) {
				return false;
			}
			return $rootScope.sessionId == profileUserId;
		}
		
		function init() {
			var profileUserId = $routeParams["uid"];
			var prom = EventService.getEventsByUser(profileUserId);
			prom.then(function(result) {
				vm.eventList = result.data;
				vm.loaded = true;
			});
		}
		init();
		
		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}
		
		function condenseDate(startDate, endDate) {
			var start = new Date(startDate);
			var end = new Date(endDate);
			if(start.getDate() == end.getDate()) {
				return new Date(startDate).toLocaleString() + " to " + new Date(endDate).toLocaleTimeString();
			}
			return new Date(startDate).toLocaleString() + " to " + new Date(endDate).toLocaleString();
		}
		
		function createEventEditDialog(evt, eventEntity) {
			$mdDialog.show({
				controller: "EditEventDialogController",
				controllerAs: "evtdialog",
				templateUrl: 'views/dialogs/edit-event.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "eventEditEntity": eventEntity }
			}).then(function(result) {
				$rootScope.$broadcast('eventEdit', result);
			}, function(error) {});
		}
		
		function shareEventDialog(evt, eventEntity) {
			$mdDialog.show({
				controller: "ShareController",
				controllerAs: "share",
				templateUrl: 'views/dialogs/share-post.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "attachedContent": eventEntity }
			}).then(function(result) {
				
			}, function(error) {});
		}
		
	}
	
	
})();