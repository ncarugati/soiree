(function() {
	angular
	.module("SoireeApp")
	.controller("MessageDialogController", MessageDialogController)
	.controller("CreateEventDialogController", CreateEventDialogController)
	.controller("EditEventDialogController", EditEventDialogController)
	.controller("ShareController", ShareController)
	.controller("PostEditDialogController", PostEditDialogController)
	.controller("MessageEditDialogController", MessageEditDialogController)
	.controller("CreateUserDialogController", CreateUserDialogController)
	.controller("CreatePostDialogController", CreatePostDialogController);
	
	
	function CreateEventDialogController($q, $http, $rootScope, $scope, $routeParams, $location, $mdDialog, UserService, EventService) {
		var vm = this;
		vm.eventTemplate = {};
		vm.addEvent = addEvent;
		vm.closeDialog = closeDialog;
		vm.allDay = false;
		vm.message = null;
		vm.deviseStatusMessage = deviseStatusMessage;
		vm.upload = upload;
		vm.querySearch = querySearch;
		vm.owner = null;
		vm.userSearchText = "";
		vm.userList = [];
		
		
		function init() {
			getAllUsers();
		}
		init();
		
		function getAllUsers() {
			var prom = UserService.getAllUsers(true);
			prom.then(function(result) {
				vm.userList = result.data;
			});
		}
		
		function addEvent(ev) {
			var template = vm.eventTemplate;
			template.priority = getPriorityValue();
			var event = {
					"name": template.name, "location": template.location, 
					"admission": template.admission, "description": template.description, 
					"image": template.image, "priority": template.priority
			};
			if($rootScope.userData.type == 'ADMIN') {
				event.owner = template.owner;
			}
			if(vm.allDay) {
				if(template.startDate == undefined) {
					vm.message = {"message": "You must specify a start date", "type": "info"};
					return;
				}
				var startDate = template.startDate;
				event.timeStart = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0);
				event.timeEnd = new Date(event.timeStart.getFullYear(), event.timeStart.getMonth(), event.timeStart.getDate(), 23, 59, 59);
				var now = new Date().getTime();
				if(event.timeStart.getTime() < now) {
					vm.message = {"message": "You cannot pick a date that has passed.", "type": "error"};
					return;
				}
			} else {
				if(template.startDate == undefined) {
					vm.message = {"message": "You must specify a start date", "type": "info"};
					return;
				}
				if(template.startTime == undefined) {
					vm.message = {"message": "You must specify a start time", "type": "info"};
					return;
				}
				
				var startDateMs = template.startDate.getTime();
				var startTimeMs = template.startTime.getTime();
				startDateMs += startTimeMs;
				event.timeStart = new Date(startDateMs);
				
				if(template.endDate == undefined) {
					vm.message = {"message": "You must specify a end date", "type": "info"};
					return;
				}
				if(template.endTime == undefined) {
					vm.message = {"message": "You must specify a end time", "type": "info"};
					return;
				}
				
				var endDateMs = template.endDate.getTime();
				var endTimeMs = template.endTime.getTime();
				endDateMs += endTimeMs;
				event.timeEnd = new Date(endDateMs);

				var diffCheck = event.timeEnd.getTime() - event.timeStart.getTime();
				if(diffCheck < 0) {
					vm.message = {"message": "End time is before the start time.", "type": "error"};
					return;
				}
				var now = new Date().getTime();
				if(event.timeStart.getTime() < now) {
					vm.message = {"message": "You cannot pick a date that has passed.", "type": "error"};
					return;
				}
			}
			var prom = EventService.addEvent(event);
			prom.then(function(result) {
				if(result) {
					$mdDialog.hide(result);
				}
			});
		}
		
		function getPriorityValue() {
			if($rootScope.userData.type == "TALENT" || $rootScope.userData.type == "VENUE") {
				return 2;
			} else if($rootScope.userData.member) {
				return 1;
			} else {
				return 0;
			}
		}
		
		function upload() {
			var headers = {	transformRequest: angular.identity,
							transformResponse: undefined,
							headers: {"Content-Type": undefined}};
			$http.post("/api/upload", new FormData(document.getElementById("createForm")), headers)
					.then(function(result) {
					vm.eventTemplate.image = result.data;
			});	
		}
		
		/**
		 * Search filtering system
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function querySearch(query) {
			var results = query ? vm.userList.filter(createFilterFor(query)) : [];
			return results;
		}

		/**
		 * Create filter function for a query string
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function createFilterFor(query) {

			var lowercaseQuery = angular.lowercase(query);
			
			return function filterFn(user) {
				return (user.username.toLowerCase().indexOf(lowercaseQuery) === 0);
			};

		}
	
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}
		
		function closeDialog() {
			$mdDialog.hide();
		}

	}
	
	
	function EditEventDialogController($q, $rootScope, $scope, $routeParams, $location, $http, $mdDialog, $window, eventEditEntity,
			UserService, EventService) {
		var vm = this;
		vm.closeDialog = closeDialog;
		vm.modifyEvent = modifyEvent;
		vm.deleteEvent = deleteEvent;
		vm.allDay = false;
		vm.upload = upload;
		
		vm.message = null;
		vm.deviseStatusMessage = deviseStatusMessage;
		
		function init() {
			if(eventEditEntity) {
				vm.eventEditEntity = eventEditEntity;
				vm.eventEditEntity.admission = parseFloat(eventEditEntity.admission);
				populateEditTime(vm.eventEditEntity);
			}
		}
		init();
		
		function modifyEvent(event) {
			if(vm.allDay) {
				if(vm.eventEditEntity.startDate == undefined) {
					vm.message = {"message": "You must specify a start date", "type": "info"};
					return;
				}
				var startDate = vm.eventEditEntity.startDate;
				event.timeStart = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate(), 0, 0, 0);
				event.timeEnd = new Date(event.timeStart.getFullYear(), event.timeStart.getMonth(), event.timeStart.getDate(), 23, 59, 59);
				var now = new Date().getTime();
				if(event.timeStart.getTime() < now) {
					vm.message = {"message": "You cannot pick a date that has passed.", "type": "error"};
					return;
				}
			} else {
				if(vm.eventEditEntity.startDate == undefined) {
					vm.message = {"message": "You must specify a start date", "type": "info"};
					return;
				}
				if(vm.eventEditEntity.startTime == undefined) {
					vm.message = {"message": "You must specify a start time", "type": "info"};
					return;
				}
				var startDateMs = vm.eventEditEntity.startDate.getTime();
				var startTimeMs = vm.eventEditEntity.startTime.getTime();
				startDateMs += startTimeMs;
				event.timeStart = new Date(startDateMs);
				
				
				if(vm.eventEditEntity.endDate == undefined) {
					vm.message = {"message": "You must specify a end date", "type": "info"};
					return;
				}
				if(vm.eventEditEntity.endTime == undefined) {
					vm.message = {"message": "You must specify a end time", "type": "info"};
					return;
				}
				var endDateMs = vm.eventEditEntity.endDate.getTime();
				var endTimeMs = vm.eventEditEntity.endTime.getTime();
				endDateMs += endTimeMs;
				event.timeEnd = new Date(endDateMs);

				var diffCheck = event.timeEnd.getTime() - event.timeStart.getTime();
				if(diffCheck < 0) {
					vm.message = {"message": "End time is before the start time.", "type": "error"};
					return;
				}
				if(event.timeStart.getTime() < Date.now()) {
					vm.message = {"message": "You cannot pick a date that has passed.", "type": "error"};
					return;
				}
			}
			var prom = EventService.updateEvent(event.id, event);
			prom.then(function(result) {
				if(result) {
					$rootScope.$broadcast('eventEdit', result);
					$mdDialog.hide(result);
				}
			}, function(error) {
				console.error(error);
			});
		}
		
		function upload() {
			var headers = {	transformRequest: angular.identity,
							transformResponse: undefined,
							headers: {"Content-Type": undefined}};
			$http.post("/api/upload", new FormData(document.getElementById("editForm")), headers)
					.then(function(result) {
					vm.eventEditEntity.image = result.data;
			});	
		}

		function deleteEvent(eventId) {
			var prom = EventService.deleteEvent(eventId);
			prom.then(function(result) {
				if(result) {
					$rootScope.$broadcast('eventDelete', result);
					$mdDialog.hide(result);
				}
			});
		}

		function populateEditTime(eventEdit) {
			var startDate = new Date(eventEdit.timeStart);
			var endDate = new Date(eventEdit.timeEnd);
			var startDateSplit = new Date(startDate.toLocaleDateString()).getTime();
			var endDateSplit =  new Date(endDate.toLocaleDateString()).getTime();
			if(startDateSplit == endDateSplit) {
				vm.allDay = true;
			}
			eventEdit.startDate = startDate;
			eventEdit.startTime = new Date(startDate.getTime() - startDateSplit);
			eventEdit.endDate = endDate;
			eventEdit.endTime = new Date(endDate.getTime() - endDateSplit);
		}
		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}

		function closeDialog() {
			$mdDialog.hide();
		}
		
	}

	function MessageDialogController($mdDialog, UserService, MessageService) {
		var vm = this;
		
		//Message vm
		vm.recipient = null;
		vm.userSearchText = "";
		vm.userList = [];
		vm.querySearch = querySearch;
		vm.sendMessage = sendMessage;
		vm.messageContent = "";
		vm.closeDialog = closeDialog;
		
		//Form State
		vm.message = null;
		vm.deviseStatusMessage = deviseStatusMessage;

		function init() {
			getAllUsers();
		}
		init();

		function getAllUsers() {
			var prom = UserService.getAllUsers(false);
			prom.then(function(result) {
				vm.userList = result.data;
			});
		}


		/**
		 * Search filtering system
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function querySearch(query) {
			var results = query ? vm.userList.filter(createFilterFor(query)) : [];
			return results;
		}

		/**
		 * Create filter function for a query string
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function createFilterFor(query) {

			var lowercaseQuery = angular.lowercase(query);
			
			return function filterFn(user) {
				return (user.username.toLowerCase().indexOf(lowercaseQuery) === 0);
			};

		}
		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}

		function sendMessage(userId, ev) {
			var message = { "message": vm.messageContent };
			var prom = MessageService.sendPersonalMessage(userId, message);
			prom.then(function(result) {
				if(result) {
					$mdDialog.show($mdDialog.alert()
							.parent(angular.element(document.body))
							.clickOutsideToClose(true)
							.title('Successful')
							.textContent('Message sent')
							.ariaLabel('Message sent')
							.ok('Ok')
							.targetEvent(ev)
					);
				}
			})
		}

		function closeDialog() {
			$mdDialog.hide();
		}

	}
	
	function ShareController($q, $rootScope, $scope, $routeParams, $location, $sce, $mdDialog, attachedContent,
			UserService, PostService) {
			var vm = this;
			vm.recipient = null;
			vm.userSearchText = "";
			vm.userList = [];
			vm.querySearch = querySearch;
			vm.caption = "";
			vm.attachment = attachedContent;
			vm.closeDialog = closeDialog;
			vm.sendEventPost = sendEventPost;
			
			function init() {
				getAllUsers();
			}
			init();
			
			function getAllUsers() {
				var prom = UserService.getAllUsers(true);
				prom.then(function(result) {
					vm.userList = result.data;
				}, function(error) {
					console.error(error);
				});
			}

			/**
			 * Search filtering system
			 * @see https://material.angularjs.org/latest/demo/chips
			 */
			function querySearch(query) {
				var results = query ? vm.userList.filter(createFilterFor(query)) : [];
				return results;
			}

			/**
			 * Create filter function for a query string
			 * @see https://material.angularjs.org/latest/demo/chips
			 */
			function createFilterFor(query) {

				var lowercaseQuery = angular.lowercase(query);
				
				return function filterFn(user) {
					return (user.username.toLowerCase().indexOf(lowercaseQuery) === 0);
				};
			}
			
			function sendEventPost(user, ev) {
				var content = { "content": vm.caption,
								"attachment": JSON.stringify(vm.attachment) };
				var prom = PostService.createPostForProfile(user.id, content);
				prom.then(function(result) {
					if(result) {
						$mdDialog.show($mdDialog.alert()
								.parent(angular.element(document.body))
								.clickOutsideToClose(true)
								.title('Success')
								.textContent('Post sent to: ' + user.username)
								.ariaLabel('Post sent')
								.ok('Ok')
								.targetEvent(ev)
						);
					}
				})
			}
			
			function closeDialog() {
				$mdDialog.hide();
			}

	}
	
	function PostEditDialogController($q, $rootScope, $scope, $routeParams, $location, $sce, $mdDialog, postEntity, PostService, CommentService) {
		var vm = this;
		vm.closeDialog = closeDialog;
		vm.post = postEntity;
		vm.updatePost = updatePost;
		vm.deletePost = deletePost;
		vm.editComment = editComment;
		vm.deleteComment = deleteComment;
		
		function updatePost() {
			var data = {"content": vm.post.content, "attachment": vm.post.attachment}
			var prom = PostService.updatePost(vm.post.id, data);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function deletePost() {
			var prom = PostService.deletePost(vm.post.id);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function deleteComment(commentId) {
			var prom = CommentService.deleteComment(commentId);
			prom.then(function(result) {
				refreshComments(vm.post);
			});
		}
		
		function editComment(comment) {
			var data = { "content": comment.editContent };
			var prom = CommentService.updateComment(comment.id, data);
			prom.then(function(result) {
				comment.isEditing = false;
				refreshComments(vm.post);
			});
		}
		
		function refreshComments(post) {
			var prom = CommentService.getCommentsForPost(post.id);
			prom.then(function(result) {
				post.comments = result.data;
			});
		}
		
		function closeDialog() {
			$mdDialog.hide();
		}
	}
	
	function MessageEditDialogController($q, $rootScope, $scope, $routeParams, $location, $sce, $mdDialog, messageEntity, MessageService) {
		var vm = this;
		vm.closeDialog = closeDialog;
		vm.message = messageEntity;
		vm.deleteMessage = deleteMessage;
		vm.modifyMessage = modifyMessage;
		
		function deleteMessage(messageId) {
			var prom = MessageService.deleteMessage(messageId);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function modifyMessage(messageId) {
			var message = {"message": vm.message.message }
			var prom = MessageService.editMessage(messageId, message);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function closeDialog() {
			$mdDialog.hide();
		}
	}
	
	function CreateUserDialogController($q, $http, $rootScope, $scope, $routeParams, $location, $mdDialog, UserService, EventService) {
		var vm = this;
		vm.user = {};
		vm.user.settings = {};
		vm.user.interests = [];
		vm.catalog = [];
		vm.closeDialog = closeDialog;
		vm.upload = upload;
		vm.createUser = createUser;
		vm.querySearch = querySearch;
		vm.catSearchText = "";
		vm.verify = "";

		
		function upload(elementName) {
			var headers = {	transformRequest: angular.identity,
							transformResponse: undefined,
							headers: {"Content-Type": undefined}};
			$http.post("/api/upload", new FormData(document.getElementById(elementName)), headers)
					.then(function(result) {
					vm.user.settings.avatar = result.data;
			});	
		}
		
		/**
		 * Search filtering system
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function querySearch(query, field, resultSet) {
			var results = query ? resultSet.filter(createFilterFor(query, field)) : [];
			return results;
		}

		/**
		 * Create filter function for a query string
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function createFilterFor(query, field) {
			
			var lowercaseQuery = angular.lowercase(query);

			return function filterFn(res) {
				return (res[field].toLowerCase().indexOf(lowercaseQuery) === 0);
			};
		}
		
		function createUser() {
			var prom = UserService.createUserAdmin(vm.user);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function closeDialog() {
			$mdDialog.hide();
		}
	}
	
	function CreatePostDialogController($q, $http, $rootScope, $scope, $routeParams, $location, $mdDialog, UserService, EventService, PostService) {
		var vm = this;
		vm.post = {};
		vm.location = null;
		vm.eventSearchText = "";
		vm.userProfileText = "";
		vm.userSearchText = "";
		vm.userList = [];
		vm.eventList = [];
		vm.querySearch = querySearch;
		vm.closeDialog = closeDialog;
		vm.submitPost = submitPost;
		
		function init() {
			var promises = [];
			promises.push(getAllUsers());
			promises.push(getAllEvents());
			$q.all(promises);
		}
		init();
		
		/**
		 * Search filtering system
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function querySearch(query, field, resultSet) {
			var results = query ? resultSet.filter(createFilterFor(query, field)) : [];
			return results;
		}

		/**
		 * Create filter function for a query string
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function createFilterFor(query, field) {
			
			var lowercaseQuery = angular.lowercase(query);

			return function filterFn(res) {
				return (res[field].toLowerCase().indexOf(lowercaseQuery) === 0);
			};
		}
		
		function getAllUsers() {
			var prom = UserService.getAllUsers(true);
			prom.then(function(result) {
				vm.userList = result.data;
			});
		}
		
		function getAllEvents() {
			var prom = EventService.getAllEvents();
			prom.then(function(result) {
				vm.eventList = result.data;
			});
		}
		
		function submitPost() {
			var data = { "content": vm.post.content, "owner": vm.post.owner }
			if(vm.post.location == 'EVENT') {
				var prom = PostService.createPostForEvent(vm.post.event.id, vm.post);
				prom.then(function(result) {
					$mdDialog.hide(result);
				});
			} else {
				var prom = PostService.createPostForProfile(vm.post.profile.id, vm.post);
				prom.then(function(result) {
					$mdDialog.hide(result);
				});
			}
		}
		
		function closeDialog() {
			$mdDialog.hide();
		}
	}

})();