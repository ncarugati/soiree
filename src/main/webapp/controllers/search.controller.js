(function() {
	angular
	.module("SoireeApp")
	.controller("SearchController", SearchController)

	function SearchController($q, $http, $routeParams, $rootScope, $location, $sce, $scope, FollowService, EventService) {
		var vm = this;
		vm.searchComposite = null;
		vm.userFollowData = null;
		vm.sessionFollowingList = {};
		vm.condenseDate = condenseDate;
		vm.getEventStats = getEventStats;
		vm.initUserResults = initUserResults;
		vm.followUser = followUser;
		vm.unfollowUser = unfollowUser;
		vm.isSessionFollowing = isSessionFollowing;
		vm.canShowResults = canShowResults;
		vm.userProgress = 0;
		vm.eventProgress = 0;
		
		function commenceSearch() {
			var query = $routeParams.query;
			var prom = $http.get("/api/search/?query=" + query);
			prom.then(function(result) {
				vm.searchComposite = result.data;
			});
		}
		
		function prepareSessionFollowing() {
			var prom = FollowService.getFollowingData($rootScope.sessionId);
			prom.then(function(result) {
				for(var i = 0; i < result.data.length; i++) {
					var entry = result.data[i].follower.id;
					vm.sessionFollowingList[entry] = true;
				}
			});
		}
		
		function condenseDate(startDate, endDate) {
			var sDate = new Date(startDate);
			var eDate = new Date(endDate);
			if(sDate.getDate() == eDate.getDate()) {
				return " " + sDate.toLocaleString() + " to " + eDate.toLocaleTimeString();
			}
			return " " + sDate.toLocaleString() + " to " + eDate.toLocaleString();
		}
		
		function init() {
			var promises = [];
			promises.push(commenceSearch());
			promises.push(prepareSessionFollowing());
			$q.all(promises);
		}
		init();
		
		
		function getUserFollowNumbers(entry) {
			var prom = FollowService.getFollowDataCount(entry.id);
			prom.then(function(result) {
				entry.followers = result.data[1];
				entry.following = result.data[0];
				if(!canShowResults(vm.userProgress, vm.searchComposite[0])) {
					vm.userProgress += 1;
				}
			});
		}
		
		function initUserResults(entry) {
			var promises = [];
			getUserFollowNumbers(entry);
		}
		
		function getEventStats(entry) {
			var eventValues = EventService.getEventStats(entry.id);
			eventValues.then(function(result) {
				entry.attendCount = result.data[0];
				entry.interestCount = result.data[1];
				if(!canShowResults(vm.eventProgress, vm.searchComposite[1])) {
					vm.eventProgress += 1;
				}
			});
		}
		
		function isSessionFollowing(userId) {
			if(vm.sessionFollowingList[userId] == undefined) {
				return false;
			}
			return vm.sessionFollowingList[userId];
		}
		
		function followUser(user) {
			var prom = FollowService.follow(user.id);
			prom.then(function(result) {
				vm.sessionFollowingList[user.id] = true;
				initUserResults(user)
			});
		}
		
		function unfollowUser(user) {
			var prom = FollowService.unfollow(user.id);
			prom.then(function(result) {
				vm.sessionFollowingList[user.id] = undefined;
				initUserResults(user);
			});
		}
		
		function canShowResults(val, tuple) {
			if(tuple == undefined) {
				return false;
			} else if(tuple.length == 0) {
				return true;
			}
			return val / tuple.length >= 1;
		}
		
	}

})();