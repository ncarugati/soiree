(function() {
	angular
	.module("SoireeApp")
	.controller("SettingsController", SettingsController)
	.controller("ProfileController", ProfileController);

	function SettingsController($q, $routeParams, $location, UserService, EventService, $rootScope, $mdDialog, $http) {
		var vm = this;
		var selectedRoleVal = "TALENT";
		
		vm.user = null;
		vm.save = save;
		vm.catalog = [];
		vm.userList = [];
		vm.selectedItem = null;
		vm.userSelectedItem = null;
		vm.searchText = null;
		vm.catSearchText = null;
		vm.userSearchText = null;
		vm.upload = upload;
		vm.isNonUser = false;
		vm.message = null;
		
		vm.transformChip = transformChip;
		vm.querySearch = querySearch;
		vm.selectedRoleVal = selectedRoleVal;
		vm.closeDialog = closeDialog;
		vm.saveWithDialog = saveWithDialog;
		vm.deleteUser = deleteUser;
		vm.deviseStatusMessage = deviseStatusMessage;
		
		
		function init() {
			if($rootScope.userEditData != undefined) {
				vm.user = $rootScope.userEditData;
			} else {
				vm.user = $rootScope.userData;
			}
			var promises = [];
			promises.push(loadCategoryIndex());
			promises.push(getAvailableMembers());
			promises.push(loadUserMembers());
			$q.all(promises).then(function(result) {
				if(vm.user.type == "TALENT" || vm.user.type == "VENUE") {
					vm.isNonUser = true;
					if(vm.user.type == "TALENT") {
						vm.selectedRoleVal = "Talent";
					} else {
						vm.selectedRoleVal = "Venue";
					}
				}
			});
		}
		init();

		function save() {
			var userId = $routeParams["uid"];
			if(vm.user.type != "ADMIN") {
				vm.user.type = vm.isNonUser ? vm.selectedRoleVal.toUpperCase() : "USER";
			}
			vm.user.members = vm.isNonUser ? vm.user.members : [];
			var prom = UserService.modifyUser(userId, vm.user);
			prom.then(function(result) {
				vm.message = {"message": "The changes have been successfully made.", "type": "success"};
			}, function(error) {
				if(error.status == 400) {
					vm.message = {"message": "Passwords do not match!", "type": "error"};
				} else if(error.status == 401) {
					vm.message = {"message": "The current password entered did not match", "type": "error"};
				}
			});
		}
		
		function loadUserMembers() {
			if(vm.user.type == "VENUE" || vm.user.type == "TALENT") {
				var prom = UserService.getUserMembers($rootScope.sessionId);
				prom.then(function(result) {
					vm.user.members = result.data;
				});
			} else {
				vm.user.members = [];
			}
		}

		function loadCategoryIndex() {
			var prom = EventService.allCategories();
			prom.then(function(result) {
				vm.catalog = result.data;
			});
		}
		
		function getAvailableMembers() {
			var prom = UserService.getAvailableMembers();
			prom.then(function(result) {
				vm.userList = result.data;
			});
		}

		/**
		 * Chip transformer
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function transformChip(chip) {
			if (angular.isObject(chip)) {
				return chip;
			}
			return { id: 0, name: chip };
		}

		/**
		 * Search filtering system
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function querySearch(query, field, resultSet) {
			var results = query ? resultSet.filter(createFilterFor(query, field)) : [];
			return results;
		}

		/**
		 * Create filter function for a query string
		 * @see https://material.angularjs.org/latest/demo/chips
		 */
		function createFilterFor(query, field) {
			
			var lowercaseQuery = angular.lowercase(query);

			return function filterFn(res) {
				return (res[field].toLowerCase().indexOf(lowercaseQuery) === 0);
			};
		}
		
		function saveWithDialog(userId) {
			if(vm.user.type != 'USER') {
				vm.user.member = null;
			}
			var prom = UserService.modifyUser(userId, vm.user);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function upload(elementName) {
			var headers = {	transformRequest: angular.identity,
							transformResponse: undefined,
							headers: {"Content-Type": undefined}};
			$http.post("/api/upload", new FormData(document.getElementById(elementName)), headers)
					.then(function(result) {
					vm.user.settings.avatar = result.data;
			});	
		}
		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}
		
		function deleteUser(userId) {
			var prom = UserService.deleteUser(userId);
			prom.then(function(result) {
				$mdDialog.hide(result);
			});
		}
		
		function closeDialog() {
			$mdDialog.hide();
		}
	}

	function ProfileController($q, $sce, $routeParams, $mdToast, $location, $rootScope, UserService, PostService, CommentService, RatingService, FollowService) {
		var vm = this;
		sessionId = $rootScope.sessionId;
		vm.user = null;
		vm.isOwner = isOwner;
		vm.isFollowing = false;
		vm.profileLoaded = false;
		vm.userInterestDisplay = [];
		vm.profilePosts = null;
		vm.postContent = "";
		
		vm.submitPost = submitPost;
		vm.submitComment = submitComment;
		vm.postInit = postInit;
		vm.editPost = editPost;
		vm.editComment = editComment;
		vm.deletePost = deletePost;
		vm.deleteComment = deleteComment;
		vm.isEntryOwner = isEntryOwner;
		vm.getRateCount = getRateCount; 
		vm.ratePost = ratePost;
		vm.isUserFollowing = isUserFollowing;
		vm.followUser = followUser;
		vm.unfollowUser = unfollowUser;
		
		$rootScope.$on('eventCreation', function(event, data) {
			$mdToast.show(
					$mdToast.simple()
					.textContent('Event Successfully Added')
					.position("top right")
					.hideDelay(3500));
			var profileUserId = $routeParams["uid"];
			if($rootScope.sessionId == profileUserId) {
				getUserStats(profileUserId);
			}
		});
		
		function isOwner() {
			var profileUserId = $routeParams["uid"];
			if(!sessionId) {
				return false;
			}
			return sessionId == profileUserId;
		}
		
		function isUserFollowing(userId) {
			if(!$rootScope.sessionId) {
				return;
			}
			var prom = FollowService.isFollowing(userId);
			prom.then(function(result) {
				vm.isFollowing = result.data;
			});
		}
		
		function followUser() {
			if(!$rootScope.sessionId) {
				return;
			}
			var profileUserId = $routeParams["uid"];
			var prom = FollowService.follow(profileUserId);
			prom.then(function(result) {
				vm.isFollowing = true;
				getUserStats(profileUserId);
			});
		}
		
		function unfollowUser() {
			var profileUserId = $routeParams["uid"];
			var prom = FollowService.unfollow(profileUserId);
			prom.then(function(result) {
				vm.isFollowing = false;
				getUserStats(profileUserId);
			});
		}
		
		function init() {
			var promises = [];
			var profileUserId = $routeParams["uid"];
			promises.push(getUserData(profileUserId));
			promises.push(getUserStats(profileUserId));
			promises.push(getProfilePosts(profileUserId));
			promises.push(isUserFollowing(profileUserId));
			$q.all(promises).then(function(resultant) {
				if(resultant) {
					vm.profileLoaded = true;
				}
			});
		}
		init();
		
		function loadUserMembers(userId) {
			if(vm.user.type == "VENUE" || vm.user.type == "TALENT") {
				var prom = UserService.getUserMembers(vm.user.id);
				prom.then(function(result) {
					if(result.data) {
						vm.user.members = result.data;
					}
				});
			} else {
				vm.user.members = [];
			}
		}
		
		
		function getUserStats(userId) {
			var prom = UserService.getProfileStatistics(userId);
			prom.then(function(resultant) {
				if(resultant) {
					vm.user.stats = resultant.data;
				}
			}, function(error) {
				vm.profileLoaded = true;
			});
		}
		
		function getUserData(userId) {
			var prom = UserService.findUserById(userId);
			prom.then(function(result) {
				if(result) {
					vm.user = result.data;
					if(result.data.settings.birthday != null) {
						vm.user.settings.birthday = new Date(result.data.settings.birthday);
					}
					for(var i = 0; i < vm.user.interests.length; i++) {
						vm.userInterestDisplay.push(vm.user.interests[i].category.name);
					}
					loadUserMembers(userId);
				}
			}, function(error) {
				vm.profileLoaded = true;
			});
		}
		
		function getProfilePosts(userId) {
			prom = UserService.getProfilePosts(userId);
			prom.then(function(result) {
				vm.profilePosts = result.data;
			});
		}
		
		function postInit(post) {
			post.rating = getRateCount(post.ratings);
			post.isEditing = false;
			if(post.attachment != null) {
				post.attachmentPresent = true;
			}
			if(!$rootScope.sessionId) {
				return;
			}
			var userRating = getUserRatingForPost(post);
			userRating.then(function(data) {
				post.userRating = data;
			});
		}

		function getUserRatingForPost(post) {
			var deferred = $q.defer();
			var prom = RatingService.findRatingForUser(post.id);
			prom.then(function(result) {
				deferred.resolve(result.data.rating);
			}, function(error) {
				deferred.reject();
			});
			return deferred.promise;
		}

		function ratePost(postId, value) {
			var prom = RatingService.findRatingForUser(postId);
			var userId = $routeParams["uid"];
			prom.then(function(result) {
				var _prom = null;
				if(result.data) {
					if(value == result.data.rating) {
						_prom = RatingService.removeRating(postId);
						_prom.then(function(resultant) {
							getProfilePosts(userId);
						});
					} else {
						_prom = RatingService.updateRating(postId, {"rating": value});
						_prom.then(function(resultant) {
							getProfilePosts(userId);
						});
					}
				} else {
					_prom = RatingService.ratePost(postId, {"rating": value});
					_prom.then(function(reusltant) {
						getProfilePosts(userId);
					});
				}
			}, function(error) {
				console.error(error);
			});
		}

		function submitPost(content) {
			var post = { "content": content };
			vm.postContent = "";
			var userId = $routeParams["uid"];
			var prom = PostService.createPostForProfile(userId, post);
			prom.then(function(result) {
				if(result) {
					getProfilePosts(userId);
				}
			}, function(error) {
				console.error(error);
			});
		}


		function editPost(post, content) {
			var data = {"content": content };
			if(!post.attachmentPresent) {
				data.attachment = null;
			}
			var userId = $routeParams["uid"];
			var prom = PostService.updatePost(post.id, data);
			prom.then(function(result) {
				if(result) {
					getProfilePosts(userId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function editComment(commentId, content) {
			var comment = {"content": content };
			var userId = $routeParams["uid"];
			var prom = CommentService.updateComment(commentId, comment);
			prom.then(function(result) {
				if(result) {
					getProfilePosts(userId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function deletePost(postId) {
			var userId = $routeParams["uid"];
			var prom = PostService.deletePost(postId);
			prom.then(function(result) {
				if(result) {
					getProfilePosts(userId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function deleteComment(commentId) {
			var userId = $routeParams["uid"];
			var prom = CommentService.deleteComment(commentId);
			prom.then(function(result) {
				if(result) {
					getProfilePosts(userId);
				}
			}, function(error) {
				console.error(error);
			});
		}

		function submitComment(postId, content) {
			var comment = { "content": content };
			var userId = $routeParams["uid"];
			var prom = CommentService.createComment(postId, comment);
			prom.then(function(result) {
				if(result) {
					getProfilePosts(userId);
				}
			}, function(error) {
				console.error(error);
			});
		}
		
		function isEntryOwner(entry) {
			var userId = $rootScope.sessionId;
			return userId == entry.owner.id;
		}

		function getRateCount(tuple) {
			var total = 0;
			for(var i = 0; i < tuple.length; i++) {
				total += tuple[0].rating;
			}
			return total;
		}
	}

})();