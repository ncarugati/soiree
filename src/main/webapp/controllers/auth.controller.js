(function() {
	angular
	.module("SoireeApp")
	.controller("LaunchController", LaunchController)
	.controller("RegisterController", RegisterController)
	.controller("LoginController", LoginController)
	.controller("AdminController", AdminController)

	function LaunchController($routeParams, $location) {
		var vm = this;
		
		function init() {
			particlesJS.load('effectPanel', 'js/particles.json', function() {});
		}
		init();
	}

	function RegisterController($routeParams, $location, $window, UserService) {
		var vm = this;
		vm.register = register;
		vm.message = null;
		vm.deviseStatusMessage = deviseStatusMessage;

		function register(username, password, pwdVer, email) {
			if(!username) {
				vm.message = {"message": "No Username Entered.", "type": "info"};
				return;
			}
			if(!password) {
				vm.message = {"message": "No Password Entered.", "type": "info"};
				return;
			}
			if(!email) {
				vm.message = {"message": "No Email Entered.", "type": "info"};
				return;
			}
			if(password === pwdVer) {
				var newUser = {username: username, password: password, email: email };
				var useCreate = UserService.createUser(newUser);
				useCreate.then(function(response) {
					if(response) {
						var data = {"username": newUser.username, "password": newUser.password};
						var login = UserService.login(data);
						login.then(function(result) {
							$location.url("/main/" + result.data);
							$window.location.reload();
						}, function(error) {
							vm.message = {"message": "An internal server error occurred. Please try again later.", "type": "error"};
						});
					}
				}, function(error) {
					if(error == 400) {
						vm.message = {"message": "Username already exists", "type": "error"};
					} else {
						vm.message = {"message": "Internal server error. Please try again later.", "type": "error"};
					}
				});
			} else {
				vm.message = {"message": "Passwords do not match.", "type": "error"};
			}

		}
		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}
	}

	function LoginController($routeParams, $location, $window, UserService) {
		var vm = this;
		vm.login = login;
		vm.status = null;
		vm.message = null;
		vm.deviseStatusMessage = deviseStatusMessage;

		function login(username, password) {
			var data = {"username": username, "password": password};
			if(!username) {
				vm.message = "Please enter a username.";
			}
			UserService.login(data).then(function(result) {
				$location.url("/main/" + result.data);
				$window.location.reload();
			}, function(error) {
				if(error.status == 401) {
					vm.message = {"message": "Invalid username or password.", "type": "error"};
				} else {
					vm.message = {"message": "Internal server error please try again later.", "type": "error"};
				}
			});
		}
		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}
	}

	function AdminController($q, $scope, $rootScope, $routeParams, $location, $window, $mdDialog, UserService, EventService, PostService, MessageService) {
		var vm = this;
		vm.userList = [];
		vm.eventList = [];
		vm.postList = [];
		vm.messageList = [];
		vm.loaded = false;
		vm.message = null;

		vm.deviseStatusMessage = deviseStatusMessage;
		vm.toReadableDate = toReadableDate;
		vm.createEventEditDialog = createEventEditDialog;
		vm.editUserDialog = editUserDialog;
		vm.editPostDialog = editPostDialog;
		vm.editMessageDialog = editMessageDialog;
		vm.createUserDialog = createUserDialog;
		vm.createPostDialog = createPostDialog;

		function init() {
			var promises = [];
			promises.push(getAllUsers());
			promises.push(getAllEvents());
			promises.push(getAllPosts());
			promises.push(getAllMessages());
			$q.all(promises).then(function(result) {
				vm.loaded = true;
			});
		}
		init();
		
		$rootScope.$on('eventCreation', function(event, data) {
			vm.message = {"message": "Event successfully added.", "type": "success"};
		});

		function getAllUsers() {
			var prom = UserService.getAllUsers(true);
			prom.then(function(result) {
				vm.userList = result.data;
			});
		}

		function getAllEvents() {
			var prom = EventService.getAllEvents();
			prom.then(function(result) {
				vm.eventList = result.data;
			});
		}

		function getAllPosts() {
			var prom = PostService.getAllPosts();
			prom.then(function(result) {
				vm.postList = result.data;
			});
		}

		function getAllMessages() {
			var prom = MessageService.getAllMessages();
			prom.then(function(result) {
				vm.messageList = result.data;
			});
		}

		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}

		function createEventEditDialog(evt, eventEntity) {
			$mdDialog.show({
				controller: "EditEventDialogController",
				controllerAs: "evtdialog",
				templateUrl: 'views/dialogs/edit-event.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "eventEditEntity": eventEntity }
			}).then(function(result) {
				if(result) {
					vm.message = {"message": "Event successfully edited.", "type": "success"};
					getAllEvents();
				}
			}, function(error) {});
		}
		
		function createUserDialog(evt) {
			$mdDialog.show({
				controller: "CreateUserDialogController",
				controllerAs: "model",
				templateUrl: 'views/dialogs/create-user.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
			}).then(function(result) {
				if(result) {
					vm.message = {"message": "User successfully created.", "type": "success"};
					getAllUsers();
				}
			}, function(error) {});
		}

		function editUserDialog(evt, userEntity) {
			$rootScope.userEditData = userEntity;
			if($rootScope.userEditData.settings.birthday != null) {
				$rootScope.userEditData.settings.birthday = 
					new Date($rootScope.userEditData.settings.birthday);
			}
			var inters = userEntity.interests;
			$rootScope.userEditData.interests = [];
			for (i = 0; i < inters.length; i++) {
				var entry = inters[i]["category"];
				$rootScope.userEditData.interests.push(entry);
			}
			$mdDialog.show({
				controller: "SettingsController",
				controllerAs: "model",
				templateUrl: 'views/dialogs/edit-user.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "editUserData": userEntity }
			}).then(function(result) {
				if(result) {
					vm.message = {"message": "User " + userEntity.username + " successfully edited.", "type": "success"};
					$rootScope.userEditData = undefined;
					getAllUsers();
				}
			}, function(error) {});
		}
		
		function createPostDialog(evt) {
			$mdDialog.show({
				controller: "CreatePostDialogController",
				controllerAs: "model",
				templateUrl: 'views/dialogs/create-post.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
			}).then(function(result) {
				if(result) {
					vm.message = {"message": "Post successfully created.", "type": "success"};
					getAllPosts();
				}
			}, function(error) {});
		}

		function editPostDialog(evt, postEntity) {
			$mdDialog.show({
				controller: "PostEditDialogController",
				controllerAs: "model",
				templateUrl: 'views/dialogs/edit-post.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "postEntity": postEntity }
			}).then(function(result) {
				if(result) {
					vm.message = {"message": "Post successfully edited.", "type": "success"};
					getAllPosts();
				}
			}, function(error) {});
		}

		function editMessageDialog(evt, messageEntity) {
			$mdDialog.show({
				controller: "MessageEditDialogController",
				controllerAs: "model",
				templateUrl: 'views/dialogs/edit-message.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "messageEntity": messageEntity }
			}).then(function(result) {
				if(result) {
					vm.message = {"message": "Message successfully edited.", "type": "success"};
					getAllMessages();
				}
			}, function(error) {});
		}

		
		function deviseStatusMessage() {
			if(vm.message == null) {
				return "";
			}
			switch(vm.message.type) {
				case "warn":
					return "md-warn-message";
				case "success":
					return "md-success";
				case "error":
					return "md-error";
				default:
					return "md-primary-message";
			}
		}

	}

})();