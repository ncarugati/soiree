(function() {
	angular
	.module("SoireeApp")
	.controller("FeedController", FeedController);

	function FeedController($q, $routeParams, $rootScope, $location, $sce, $scope, $mdDialog, $mdToast, EventService) {
		var vm = this;
		var exploreTuple = [];
		var eventsFeedTuple = [];
		var followingTuple = [];
		var eventsTuple = [];
		
		vm.trustHTML = trustHTML;
		
		vm.eventsTuple = eventsTuple;
		vm.eventsFeedTuple = eventsFeedTuple;
		vm.followingTuple = followingTuple;
		vm.exploreTuple = exploreTuple;
		
		
		
		vm.toReadableDate = toReadableDate;
		vm.createEventEditDialog = createEventEditDialog;
		vm.shareEventBriteDialog = shareEventBriteDialog;
		vm.condenseDate = condenseDate;
		vm.loadExploreModule = loadExploreModule;
		
		vm.interestSearch = false;
		vm.exploreLoaded = false;
		vm.nearbySearch = false;
		
		vm.exploreError = false;
		vm.exploreErrorMessage = null;
		vm.exploreLocationName = "";
		vm.exploreQuery = "";
		
		$rootScope.$on('eventCreation', function(event, data) {
			$mdToast.show(
					$mdToast.simple()
					.textContent('Event Successfully Added')
					.position("top right")
					.hideDelay(3500));
			loadMyEventsModule();
		});
		
		$rootScope.$on('eventEdit', function(event, data) {
			$mdToast.show(
					$mdToast.simple()
					.textContent('Event Edited')
					.position("top right")
					.hideDelay(3500));
			loadMyEventsModule();
		});
		
		$rootScope.$on('eventDelete', function(event, data) {
			$mdToast.show(
					$mdToast.simple()
					.textContent('Event Deleted')
					.position("top right")
					.hideDelay(3500));
			loadMyEventsModule();
		});
		
		function init() {
			vm.interestSearch = true;
			vm.nearbySearch = false;
			var promises = [];
			loadExploreModule();
			loadEventFeedModule();
			loadFollowerEventsModule();
			loadMyEventsModule();
		}
		init();
		
		
		
		function loadExploreModule() {
			vm.exploreError = false;
			if(vm.exploreLoaded) {
				vm.exploreLoaded = false;
			}
			if($rootScope.userData.interests.length == 0) {
				vm.interestSearch = false;
			}
			if(!vm.nearbySearch) {
				vm.exploreLocationName = "";
			}
			var query = { 
				"locationName": vm.exploreLocationName,
				"query": vm.exploreQuery,
				"searchingInterests": vm.interestSearch 
			};
			var prom = EventService.getExploreEvents(query);
			prom.then(function(result) {
				if(result.data.events) {
					exploreTuple = result.data;
					vm.exploreTuple = exploreTuple;
					vm.exploreLoaded = true;
				} else {
					vm.exploreTuple = [];
					vm.exploreError = true;
					vm.exploreLoaded = true;
					if(result.data.error_description.includes("location.address")) {
						vm.exploreErrorMessage = "Invalid Address Specified. Please try a valid address."
					} else if(result.data.error = "INTERNAL_ERROR") {
						vm.exploreErrorMessage = "EventBrite has encountered an internal error. If you have no interests or any filters consider applying them.";
					} else {
						vm.exploreErrorMessage = result.data.error_description;
					}
				}
			}, function(error) {
				vm.exploreErrorMessage = "Internal Error on Soiree's end occurred."
				vm.exploreTuple = [];
				vm.exploreError = true;
				vm.exploreLoaded = true;
			});
		}
		
		function loadEventFeedModule() {
			var prom = EventService.getEventFeed();
			prom.then(function(result) {
				vm.eventsFeedTuple = result.data;
			});
		}
		
		function loadFollowerEventsModule() {
			var prom = EventService.getUserFollowingEvents();
			prom.then(function(result) {
				vm.followingTuple = result.data;
			});
		}
		
		function loadMyEventsModule() {
			vm.exploreError = false;
			var userId = $rootScope.sessionId;
			var prom = EventService.getEventsByUser(userId);
			prom.then(function(result) {
				eventsTuple = result.data;
				vm.eventsTuple = eventsTuple;
			});
		}

		function createEventEditDialog(evt, eventEntity) {
			$mdDialog.show({
				controller: "EditEventDialogController",
				controllerAs: "evtdialog",
				templateUrl: 'views/dialogs/edit-event.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "eventEditEntity": eventEntity }
			});
		}
		
		function shareEventBriteDialog(evt, eventEntity) {
			$mdDialog.show({
				controller: "ShareController",
				controllerAs: "share",
				templateUrl: 'views/dialogs/share-post.html',
				parent: angular.element(document.body),
				targetEvent: evt,
				clickOutsideToClose: true,
				fullscreen: $scope.customFullscreen,
				locals: { "attachedContent": eventEntity }
			});
		}
		
		
		function trustHTML(html) {
			return $sce.trustAsHtml(html);
		}
		
		function toReadableDate(date) {
			return new Date(date).toLocaleString();
		}
		
		function condenseDate(startDate, endDate) {
			var start = new Date(startDate);
			var end = new Date(endDate);
			if(start.getDate() == end.getDate()) {
				return new Date(startDate).toLocaleString() + " to " + new Date(endDate).toLocaleTimeString();
			}
			return new Date(startDate).toLocaleString() + " to " + new Date(endDate).toLocaleString();
		}

	}

})();