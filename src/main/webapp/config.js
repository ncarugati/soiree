(function () {
	angular
	.module("SoireeApp")
	.config(Config);

	var getSession = function($q, $route, UserService, $rootScope) {
		var deferred = $q.defer();
		var prom = UserService.getSession();
		prom.then(function(result) {
			$rootScope.userData = result.data;
			if($rootScope.userData.settings == undefined) {
				deferred.resolve();
				$rootScope.userData = null;
				return;
			}
			if($rootScope.userData.settings.birthday != null) {
				$rootScope.userData.settings.birthday = 
					new Date($rootScope.userData.settings.birthday);
			}
			var inters = result.data.interests;
			$rootScope.userData.interests = [];
			for (i = 0; i < inters.length; i++) {
				var entry = inters[i]["category"];
				$rootScope.userData.interests.push(entry);
			}
			$rootScope.sessionId = result.data.id;
			deferred.resolve();
		}, function(error) {
			console.error(error);
			deferred.reject();
		});
		return deferred.promise;
	}

	function Config($routeProvider, $stateProvider, $locationProvider) {
		$locationProvider.html5Mode(true);

		$routeProvider
		.when("/", {
			templateUrl: "views/launch.html",
			controller: "LaunchController",
			controllerAs: "model"
		})
		.when("/login", {
			templateUrl: "views/login.html",
			controller: "LoginController",
			controllerAs: "model"
		})
		.when("/register", {
			templateUrl: "views/register.html",
			controller: "RegisterController",
			controllerAs: "model"
		})
		.when("/main/:uid", {
			templateUrl: "views/feed.html",
			controller: "FeedController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/main/:uid/settings", {
			templateUrl: "views/settings.html",
			controller: "SettingsController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/main/:uid/inbox", {
			templateUrl: "views/inbox.html",
			controller: "MessageController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/main/:uid/inbox/:cid", {
			templateUrl: "views/chat.html",
			controller: "ConversationController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/profile/:uid", {
			templateUrl: "views/profile.html",
			controller: "ProfileController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/profile/:uid/followers", {
			templateUrl: "views/followers.html",
			controller: "FollowerController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/profile/:uid/following", {
			templateUrl: "views/following.html",
			controller: "FollowingController",
			controllerAs: "model",
			resolve: {  userSession: getSession }
		})
		.when("/profile/:uid/events", {
			templateUrl: "views/profile-events.html",
			controller: "UserEventsController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/events/:eid", {
			templateUrl: "views/event.html",
			controller: "EventController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/events/:eid/settings", {
			templateUrl: "views/event-settings.html",
			controller: "EventSettingsController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/events/:eid/attending", {
			templateUrl: "views/attending.html",
			controller: "EventInfoController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/events/:eid/interested", {
			templateUrl: "views/interested.html",
			controller: "EventInfoController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/search", {
			templateUrl: "views/search.html",
			controller: "SearchController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.when("/admin", {
			templateUrl: "views/admin.html",
			controller: "AdminController",
			controllerAs: "model",
			resolve: { userSession: getSession }
		})
		.otherwise({
			templateUrl : "notfound.html"
		});
	}

})();