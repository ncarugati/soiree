(function() {
	angular
	.module("SoireeApp")
	.factory("EventService", EventService);

	function EventService($http) {

		var api = {
				"allCategories": allCategories,
				"getExploreEvents": getExploreEvents,
				"addEvent": addEvent,
				"getEventsByUser": getEventsByUser,
				"findEventById": findEventById,
				"updateEvent": updateEvent,
				"deleteEvent": deleteEvent,
				"getEventPosts": getEventPosts,
				"getAttendStatusForUser": getAttendStatusForUser,
				"attendEvent": attendEvent,
				"rescindAttendance": rescindAttendance,
				"getInterestStatusForUser": getInterestStatusForUser,
				"invokeEventInterest": invokeEventInterest,
				"rescindEventInterest": rescindEventInterest,
				"getEventStats": getEventStats,
				"getUserFollowingEvents": getUserFollowingEvents,
				"getRightsCatalog": getRightsCatalog,
				"grantRightsToUser": grantRightsToUser,
				"getRightsForEvent": getRightsForEvent,
				"alterRights": alterRights,
				"revokeRights": revokeRights,
				"getAllEvents": getAllEvents,
				"getEventFeed": getEventFeed,
		};
		return api;
		
		function allCategories() {
			return $http.get("/api/event/categories");
		}
		
		function getExploreEvents(query) {
			return $http.post("/api/event/explore", query);
		}
		
		function addEvent(event) {
			return $http.post("/api/event", event);
		}
		
		function getEventsByUser(userId) {
			return $http.get("/api/event/user/" + userId);
		}
		
		function findEventById(eventId) {
			return $http.get("/api/event/" + eventId);
		}
		
		function updateEvent(eventId, event) {
			return $http.put("/api/event/" + eventId, event);
		}
		
		function deleteEvent(eventId) {
			return $http.delete("/api/event/" + eventId);
		}
		
		function getEventPosts(eventId) {
			return $http.get("/api/event/" + eventId + "/posts");
		}
		
		function getAttendStatusForUser(eventId) {
			return $http.get("/api/event/" + eventId + "/attend");
		}
		
		function attendEvent(eventId) {
			return $http.post("/api/event/" + eventId + "/attend");
		}
		
		function rescindAttendance(eventId) {
			return $http.delete("/api/event/" + eventId + "/attend");
		}
		
		function getInterestStatusForUser(eventId) {
			return $http.get("/api/event/" + eventId + "/interest");
		}
		
		function invokeEventInterest(eventId) {
			return $http.post("/api/event/" + eventId + "/interest");
		}
		
		function rescindEventInterest(eventId) {
			return $http.delete("/api/event/" + eventId + "/interest");
		}
		
		function getEventStats(eventId) {
			return $http.get("/api/event/" + eventId + "/stats");
		}
		
		function getUserFollowingEvents() {
			return $http.get("/api/event/following");
		}
		
		function getRightsCatalog() {
			return $http.get("/api/event/roles");
		}
		
		function grantRightsToUser(eventId, rights) {
			return $http.post("/api/event/" + eventId + "/rights", rights);
		}
		
		function alterRights(eventId, rightsId, rights) {
			return $http.put("/api/event/" + eventId + "/rights/" + rightsId, rights);
		}
		
		function revokeRights(eventId, rightsId) {
			return $http.delete("/api/event/" + eventId + "/rights/" + rightsId);
		}
		
		function getAllEvents() {
			return $http.get("/api/event");
		}
		
		function getEventFeed() {
			return $http.get("/api/event/feed");
		}
		
		function getRightsForEvent(userId, eventId) {
			return $http.get("/api/event/" + eventId + "/rights/" + userId);
		}
		
	}
})();