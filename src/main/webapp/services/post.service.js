(function() {
	angular
	.module("SoireeApp")
	.factory("PostService", PostService);

	function PostService($http) {

		var api = {
			"createPostForProfile": createPostForProfile,
			"createPostForEvent": createPostForEvent,
			"findPostById": findPostById,
			"updatePost": updatePost,
			"deletePost": deletePost,
			"getPostComments": getPostComments,
			"getPostRating": getPostRating,
			"getAllPosts": getAllPosts,
			"togglePostLock": togglePostLock
		};
		return api;
		
		function createPostForProfile(userId, post) {
			return $http.post("/api/post/profile/" + userId, post);
		}
		
		function createPostForEvent(eventId, post) {
			return $http.post("/api/post/event/" + eventId, post);
		}
		
		function findPostById(postId) {
			return $http.get("/api/post/" + postId);
		}
		
		function updatePost(postId, post) {
			return $http.put("/api/post/" + postId, post);
		}
		
		function deletePost(postId) {
			return $http.delete("/api/post/" + postId);
		}
		
		function getPostComments(postId) {
			return $http.get("/api/post/" + postId + "/comments");
		}
		
		function getPostRating(postId) {
			return $http.get("/api/post/" + postId + "/rating")
		}
		
		function getAllPosts() {
			return $http.get("/api/post");
		}
		
		function togglePostLock(postId) {
			return $http.put("/api/post/" + postId + "/lock");
		}
	}
})();