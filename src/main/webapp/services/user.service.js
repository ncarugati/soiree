(function() {
	angular
	.module("SoireeApp")
	.factory("UserService", UserService);

	function UserService($http) {

		var api = {
				"login": login,
				"logout": logout,
				"isLoggedIn": isLoggedIn,
				"getSession": getSession,
				"createUser": createUser,
				"modifyUser": modifyUser,
				"findUserById": findUserById,
				"getAllUsers": getAllUsers,
				"getProfileStatistics": getProfileStatistics,
				"getProfilePosts": getProfilePosts,
				"getUserMembers": getUserMembers,
				"getAvailableMembers": getAvailableMembers,
				"deleteUser": deleteUser,
				"createUserAdmin": createUserAdmin
		};
		return api;

		function login(data) {
			return $http.post("/login", "username=" + data.username + "&password=" + data.password, 
					{ headers: {"Content-Type": "application/x-www-form-urlencoded"} });
		}
		
		function logout() {
			return $http.post("/logout");
		}
		
		function isLoggedIn() {
			return $http.get("/api/loggedin");
		}
		
		function getSession() {
			return $http.get("/api/session");
		}

		function createUser(user) {
			return $http.post("/api/user", user);
		}
		
		function modifyUser(userId, user) {
			return $http.put("/api/user/" + userId, user);
		}
		
		function findUserById(userId) {
			return $http.get("/api/user/" + userId);
		}
		
		function getAllUsers(sessionUser) {
			return $http.get("/api/user?session=" + sessionUser);
		}
		
		function getProfileStatistics(userId) {
			return $http.get("/api/user/" + userId + "/stats");
		}
		
		function getProfilePosts(userId) {
			return $http.get("/api/user/" + userId + "/posts");
		}
		
		function getUserMembers(userId) {
			return $http.get("/api/user/members/" + userId);
		}
		
		function getAvailableMembers() {
			return $http.get("/api/user/members");
		}
		
		function deleteUser(userId) {
			return $http.delete("/api/user/" + userId);
		}
		
		function createUserAdmin(user) {
			return $http.post("/api/admin/user", user);
		}

	}
})();