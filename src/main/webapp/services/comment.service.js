(function() {
	angular
	.module("SoireeApp")
	.factory("CommentService", CommentService);

	function CommentService($http) {

		var api = {
			"createComment": createComment,
			"getCommentsForPost": getCommentsForPost,
			"updateComment": updateComment,
			"deleteComment": deleteComment
		};
		return api;

		function createComment(postId, comment) {
			return $http.post("/api/post/" + postId + "/comment" , comment);
		}
		
		function getCommentsForPost(postId) {
			return $http.get("/api/post/" + postId + "/comment");
		}
		
		function updateComment(commentId, comment) {
			return $http.put("/api/comment/" + commentId, comment);
		}
		
		function deleteComment(commentId) {
			return $http.delete("/api/comment/" + commentId);
		}
	}
})();