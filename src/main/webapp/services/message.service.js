(function() {
	angular
	.module("SoireeApp")
	.factory("MessageService", MessageService);

	function MessageService($http) {

		var api = {
			"sendPersonalMessage": sendPersonalMessage,
			"getConversations": getConversations,
			"readAllMessages": readAllMessages,
			"archiveMessage": archiveMessage,
			"archiveConversation": archiveConversation,
			"getConversation": getConversation,
			"findConversationById": findConversationById,
			"getAllMessages": getAllMessages,
			"editMessage": editMessage,
			"deleteMessage": deleteMessage
		};
		return api;

		function sendPersonalMessage(userId, message) {
			return $http.post("/api/message/" + userId, message);
		}
		
		function getConversations() {
			return $http.get("/api/message");
		}
		
		function readAllMessages(conversationId) {
			return $http.put("/api/message/conversation/" + conversationId + "/read");
		}
		
		function archiveMessage(messageId) {
			return $http.put("/api/message/" + messageId + "/archive");
		}
		
		function archiveConversation(conversationId) {
			return $http.put("/api/conversation/" + conversationId);
		}

		function getConversation(conversationId) {
			return $http.get("/api/message/conversation/" + conversationId);
		}
		
		function findConversationById(conversationId) {
			return $http.get("/api/conversation/" + conversationId);
		}
		
		function getAllMessages() {
			return $http.get("/api/message/all");
		}
		
		function editMessage(messageId, message) {
			return $http.put("/api/message/admin/" + messageId, message);
		}
		
		function deleteMessage(messageId) {
			return $http.delete("/api/message/admin/" + messageId);
		}
		

	}
})();