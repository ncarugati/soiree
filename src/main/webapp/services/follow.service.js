(function() {
	angular
	.module("SoireeApp")
	.factory("FollowService", FollowService);

	function FollowService($http) {

		var api = {
			"follow": follow,
			"unfollow": unfollow,
			"getFollowerData": getFollowerData,
			"getFollowingData": getFollowingData,
			"getFollowData": getFollowData,
			"getFollowDataCount": getFollowDataCount,
			"isFollowing": isFollowing
		};
		return api;
		
		function follow(userId) {
			return $http.post("/api/user/follow/" + userId);
		}
		
		function unfollow(userId) {
			return $http.delete("/api/user/follow/" + userId);
		}
		
		function getFollowData(userId) {
			return $http.get("/api/user/follow/" + userId);
		}
		
		function getFollowDataCount(userId) {
			return $http.get("/api/user/follow/" + userId + "/count");
		}
		
		function getFollowerData(userId) {
			return $http.get("/api/user/follow/" + userId + "/follower");
		}
		
		function getFollowingData(userId) {
			return $http.get("/api/user/follow/" + userId + "/following");
		}
		
		function isFollowing(userId) {
			return $http.get("/api/user/follow/" + userId + "/isfollow");
		}
		
	}
})();