(function() {
	angular
	.module("SoireeApp")
	.factory("RatingService", RatingService);

	function RatingService($http) {

		var api = {
			"ratePost": ratePost,
			"findRatingForUser": findRatingForUser,
			"updateRating": updateRating,
			"removeRating": removeRating
		};
		return api;
		
		
		function ratePost(postId, rating) {
			return $http.post("/api/post/" + postId + "/rate", rating);
		}
		
		function findRatingForUser(postId) {
			return $http.get("/api/post/" + postId + "/rate");
		}
		
		function updateRating(postId, rating) {
			return $http.put("/api/post/" + postId + "/rate", rating);
		}
		
		function removeRating(postId) {
			return $http.delete("/api/post/" + postId + "/rate");
		}
		
	}
})();